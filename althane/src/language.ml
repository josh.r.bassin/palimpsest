open! Core
open! Async

type t =
  | Common
  | Abyssal
  | Celestial
  | Deep_speech
  | Draconic
  | Dwarvish
  | Elvish
  | Giant
  | Gnomish
  | Goblin
  | Halfling
  | Infernal
  | Orc
  | Primordial
  | Sylvan
  | Any of int
[@@deriving compare, sexp_of, variants]

let to_string = function
  | Any 1 -> "any one language of your choosing"
  | Any 2 -> "any two languages of your choosing"
  | Any 3 -> "any three languages of your choosing"
  | Any c -> [%string "any %{c#Int} languages of your choosing"]
  | t -> Variants.to_name t |> String.substr_replace_all ~pattern:"_" ~with_:" "
;;

let to_assoc = function
  | Any c -> "any", `Int c
  | t -> to_string t |> String.lowercase, `Bool true
;;

let to_json ts = `List [ `Assoc (List.map ts ~f:to_assoc) ]

let to_entry ts =
  let rec format_hum ?acc con =
    match acc, con with
    | None, [ l ] -> l
    | None, [ l; r ] -> [%string "%{l} and %{r}"]
    | Some acc, [ l ] -> [%string "%{acc}, and %{l}"]
    | Some acc, l :: r -> format_hum ~acc:[%string "%{acc}, %{l}"] r
    | None, l :: r -> format_hum ~acc:l r
    | _ ->
      raise_s
        [%message
          "Bad language list" ~descent:(acc : string option) ~languages:(ts : t list)]
  in
  let languages = List.sort ts ~compare |> List.map ~f:to_string |> format_hum in
  Entry.("Languages" --> [%string "You can speak, read, and write %{languages}."])
;;
