open! Core
open! Async

type t =
  { walk : int
  ; swim : int option
  ; climb : int option
  ; fly : int option
  }
[@@deriving fields]

let create walk ?swim ?climb ?fly () = Fields.create ~walk ~swim ~climb ~fly

let to_json t : Yojson.t =
  let walk acc f = (Fieldslib.Field.name f, `Int (Fieldslib.Field.get f t)) :: acc in
  let conv acc f =
    match Fieldslib.Field.get f t with
    | None -> acc
    | Some v -> (Fieldslib.Field.name f, `Int v) :: acc
  in
  `Assoc (Fields.fold ~init:[] ~walk ~swim:conv ~climb:conv ~fly:conv)
;;
