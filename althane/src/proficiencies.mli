open! Core
open! Async

module Armor : sig
  type t

  val light : t
  val medium : t
  val heavy : t
  val shield : t
end

module Weapon : sig
  type t

  val simple : t
  val martial : t
  val item : string -> t
  val custom : string -> t
end

module Tool : sig
  type t

  val thieves_tools : t
  val tinkers_tools : t
  val musical_instrument : t
  val gaming_set : t
  val artisans_tools : int -> t
end

type t

val create
  :  armor:Armor.t list
  -> weapons:Weapon.t list
  -> tools:Tool.t list
  -> skills:Skill.t list
  -> t

val to_json : t -> Yojson.t