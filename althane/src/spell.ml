open! Core
open! Async

type t =
  { name : string
  ; level : int
  ; school : School.t
  ; aspect : Aspect.t
  ; time : Casting_time.t
  ; range : Range.t
  ; components : Component.t
  ; duration : Duration.t
  ; entries : Entry.t list
  ; entries_at_higher_level : Entry.t list option
  ; classes : (string * string) list
  }
[@@deriving fields]

let compare l r = [%compare: String.t] l.name r.name

let create
    ?entries_at_higher_level
    ~name
    ~level
    ~school
    ~aspect
    ~time
    ~range
    ~components
    ~duration
    ~entries
    ~classes
    ()
  =
  Fields.create
    ~name
    ~level
    ~school
    ~aspect
    ~time
    ~range
    ~components
    ~duration
    ~entries
    ~entries_at_higher_level
    ~classes
;;

let to_json
    { name
    ; level
    ; school
    ; aspect
    ; time
    ; range
    ; components
    ; duration
    ; entries
    ; entries_at_higher_level
    ; classes
    }
    : Yojson.t
  =
  `Assoc
    ([ "name", `String name
     ; "source", `String "ALT"
     ; "level", `Int level
     ; "school", School.to_json school
     ; "realm", Aspect.to_json aspect
     ; "time", Casting_time.to_json time
     ; "range", Range.to_json range
     ; "components", Component.to_json components
     ; "duration", Duration.to_json duration
     ; "entries", `List (List.map entries ~f:Entry.to_json)
     ; ( "classes"
       , `Assoc
           [ ( "fromClassList"
             , `List
                 (List.map classes ~f:(fun (name, source) ->
                      `Assoc [ "name", `String name; "source", `String source ])) )
           ] )
     ]
    @
    match entries_at_higher_level with
    | None -> []
    | Some entries_at_higher_level ->
      [ ( "entriesHigherLevel"
        , `List Entry.[ to_json (ne "At Higher Levels" entries_at_higher_level) ] )
      ])
;;
