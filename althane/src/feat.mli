open! Core
open! Async

type t [@@deriving compare]

val create : name:string -> entries:Entry.t list -> t
val to_json : t -> Yojson.t