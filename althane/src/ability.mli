open! Core
open! Async

type t

val create
  :  ?str:int
  -> ?dex:int
  -> ?con:int
  -> ?wis:int
  -> ?int_:int
  -> ?cha:int
  -> unit
  -> t

val to_json_assoc : t -> Yojson.t
val to_json : t -> Yojson.t
