open! Core
open! Async

type t =
  { str : int
  ; dex : int
  ; con : int
  ; wis : int
  ; int_ : int
  ; cha : int
  }
[@@deriving fields]

let create ?(str = 0) ?(dex = 0) ?(con = 0) ?(wis = 0) ?(int_ = 0) ?(cha = 0) () =
  Fields.create ~str ~dex ~con ~wis ~int_ ~cha
;;

let to_json_assoc t : Yojson.t =
  let conv acc f =
    match Fieldslib.Field.get f t with
    | 0 -> acc
    | v ->
      ( (match Fieldslib.Field.name f with
        | "int_" -> "int"
        | name -> name)
      , `Int v )
      :: acc
  in
  `Assoc
    (Fields.fold ~init:[] ~str:conv ~dex:conv ~con:conv ~wis:conv ~int_:conv ~cha:conv)
;;

let to_json t : Yojson.t = `List [ to_json_assoc t ]