open! Core
open! Async

type t [@@deriving compare]

val two_handed : t
val light : t
val heavy : t
val reload : int -> t
val range : int * int -> t

val create
  :  ?metadata:(string * Yojson.t) list
  -> ?abbreviation:string
  -> name:string
  -> entries:Entry.t list
  -> unit
  -> t

val abbreviation : t -> string
val metadata : t -> (string * Yojson.t) list
val to_json : t -> Yojson.t
