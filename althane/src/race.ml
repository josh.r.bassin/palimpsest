open! Core
open! Async

type t =
  { name : string
  ; size : Size.t
  ; speed : Speed.t
  ; ability : Ability.t
  ; creature_type : Creature_type.t
  ; height_and_weight : Height_and_weight.t
  ; languages : Language.t list
  ; info : Entry.t
  ; traits : Description.t list
  }
[@@deriving fields]

let create = Fields.create

let to_json
    { name
    ; size
    ; speed
    ; ability
    ; creature_type
    ; height_and_weight
    ; languages
    ; info
    ; traits
    }
    : Yojson.t
  =
  let entries =
    (traits
    @ Description.[ create ~rank:Rank.language ~entry:(Language.to_entry languages) ])
    |> Description.to_entries
  in
  `Assoc
    [ "name", `String name
    ; "source", `String "ALT"
    ; "size", Size.to_json size
    ; "speed", Speed.to_json speed
    ; "ability", Ability.to_json ability
    ; "creatureTypes", Creature_type.to_json creature_type
    ; "heightAndWeight", Height_and_weight.to_json height_and_weight
    ; "languageProficiencies", Language.to_json languages
    ; ( "entries"
      , `List ([ info; Entry.(us [ ue [ ue entries ] ]) ] |> List.map ~f:Entry.to_json) )
    ]
;;
