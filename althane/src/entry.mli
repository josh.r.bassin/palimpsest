open! Core
open! Async

type t

val s : string -> t

val tbl
  :  title:string
  -> labels:string list
  -> style:string list
  -> rows:string list list
  -> t

val us : t list -> t
val ns : string -> t list -> t
val ue : t list -> t
val ne : string -> t list -> t
val ln : t list -> t
val lh : t list -> t
val sd : string -> Ability_name.t -> t
val sa : string -> Ability_name.t -> t
val op : string list -> t

(** string to t *)
val ( ! ) : string -> t

(** section, many *)
val ( <~> ) : string -> string list -> t

(** entry, many *)
val ( <-> ) : string -> string list -> t

(** section, one *)
val ( ~~> ) : string -> string -> t

(** entry, one *)
val ( --> ) : string -> string -> t

(** string to item t *)
val ( !! ) : string -> t

(** named item *)
val ( -!> ) : string -> string -> t

val info
  :  name:string
  -> title:string
  -> intro:string
  -> physical_qualities:t list
  -> playstyle:t list
  -> t

val to_json : t -> Yojson.t

module Vestige : sig
  val ingest_entries : t list -> (string * string list) list
  val ingest_traits : t list -> (string * string list) list
end
