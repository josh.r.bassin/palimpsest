open! Core
open! Async

type t

val ancestry : t
val caprice : t
val creation : t
val decay : t
val expansion : t
val individualism : t
val juncture : t
val progress : t
val providence : t
val stability : t
val weight : t
val wisdom : t
val to_json : t -> Yojson.t