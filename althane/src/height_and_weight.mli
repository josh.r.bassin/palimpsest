open! Core
open! Async

type t

val create : h:int -> mh:string -> w:int -> mw:string -> t
val to_json : t -> Yojson.t
