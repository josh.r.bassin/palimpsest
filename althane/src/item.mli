open! Core
open! Async

type t [@@deriving compare]

val create
  :  name:string
  -> type_:Item_type.t
  -> rarity:Rarity.t
  -> properties:Item_property.t list
  -> entries:Entry.t list
  -> t

val to_json : t -> Yojson.t
