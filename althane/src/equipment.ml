open! Core
open! Async

type t = string list

let of_string_list ts = ts

let to_json ts : Yojson.t =
  `Assoc
    [ "additionalFromBackground", `Bool true
    ; "default", `List (List.map ts ~f:(fun t -> `String t))
    ]
;;
