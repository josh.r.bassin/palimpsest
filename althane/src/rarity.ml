open! Core
open! Async

type t =
  | Mundane
  | Uncommon
  | Rare
  | Very_rare
  | Legendary
  | Artifact
[@@deriving variants]

let to_json : t -> Yojson.t = function
  | Mundane -> `String "none"
  | Uncommon -> `String "uncommon"
  | Rare -> `String "rare"
  | Very_rare -> `String "very rare"
  | Legendary -> `String "legendary"
  | Artifact -> `String "artifact";;
