open! Core
open! Async

type t

val self : t
val touch : t
val sight : t
val unlimited : t
val special : t
val miles : int -> t
val point : int -> t
val line : int -> t
val cone : int -> t
val radius : int -> t
val cube : int -> t
val sphere : int -> t
val hemisphere : int -> t
val to_json : t -> Yojson.t
