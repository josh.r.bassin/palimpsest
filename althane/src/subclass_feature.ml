open! Core
open! Async

type t =
  { name : string
  ; class_ : string
  ; class_source : string
  ; subclass : string
  ; level : int
  ; entries : Entry.t list
  }
[@@deriving fields]

let compare l r = [%compare: string] l.name r.name

let create ?(class_source = "ALT") ~name ~class_ ~subclass ~level ~entries () =
  Fields.create ~name ~class_ ~class_source ~subclass ~level ~entries
;;

let to_format_json { name; level; class_; class_source; subclass; _ } : Yojson.t =
  let class_source =
    match class_source with
    | "PHB" -> ""
    | class_source -> class_source
  in
  `String [%string "%{name}|%{class_}|%{class_source}|%{subclass}|ALT|%{level#Int}"]
;;

let to_json { name; class_; class_source; subclass; level; entries } : Yojson.t =
  `Assoc
    [ "name", `String name
    ; "source", `String "ALT"
    ; "className", `String class_
    ; "classSource", `String class_source
    ; "subclassShortName", `String subclass
    ; "subclassSource", `String "ALT"
    ; "level", `Int level
    ; "entries", `List (List.map entries ~f:Entry.to_json)
    ]
;;
