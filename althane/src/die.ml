open! Core
open! Async

type t =
  { value : int
  ; faces : int
  }
[@@deriving fields]

let create = Fields.create

let to_json { value; faces } : Yojson.t =
  `Assoc [ "number", `Int value; "faces", `Int faces ]
;;
