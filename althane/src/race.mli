open! Core
open! Async

type t

val create
  :  name:string
  -> size:Size.t
  -> speed:Speed.t
  -> ability:Ability.t
  -> creature_type:Creature_type.t
  -> height_and_weight:Height_and_weight.t
  -> languages:Language.t list
  -> info:Entry.t
  -> traits:Description.t list
  -> t

val name : t -> string
val to_json : t -> Yojson.t
