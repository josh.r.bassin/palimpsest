open! Core
open! Async

type t

val create : int -> ?swim:int -> ?climb:int -> ?fly:int -> unit -> t
val to_json : t -> Yojson.t
