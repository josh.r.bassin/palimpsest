open! Core
open! Async

type t =
  | Ancestry
  | Caprice
  | Creation
  | Decay
  | Expansion
  | Individualism
  | Juncture
  | Progress
  | Providence
  | Stability
  | Weight
  | Wisdom
[@@deriving variants]

let to_string = function
  | Ancestry -> "a"
  | Caprice -> "ca"
  | Creation -> "cr"
  | Decay -> "d"
  | Expansion -> "e"
  | Individualism -> "i"
  | Juncture -> "j"
  | Progress -> "prog"
  | Providence -> "prov"
  | Stability -> "s"
  | Weight -> "we"
  | Wisdom -> "wi"
;;

let to_json t : Yojson.t = `String (to_string t)
