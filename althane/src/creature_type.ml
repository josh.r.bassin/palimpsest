open! Core
open! Async

type t =
  | Aberration
  | Beast
  | Celestial
  | Construct
  | Dragon
  | Elemental
  | Fey
  | Fiend
  | Giant
  | Humanoid
  | Monstrosity
  | Ooze
  | Plant
  | Undead
[@@deriving variants]

let to_string = Variants.to_name
let to_json t : Yojson.t = `List [ `String (to_string t |> String.lowercase) ]

let to_entry t =
  Entry.(
    to_string t --> [%string "Your creature type is %{String.lowercase (to_string t)}."])
;;
