open! Core
open! Async

type t [@@deriving sexp_of]

val strength : t
val dexterity : t
val constitution : t
val wisdom : t
val intelligence : t
val charisma : t
val to_json : t -> Yojson.t