open! Core
open! Async

type t =
  { name : string
  ; type_ : string
  ; level : int option
  ; prereq : (string * string) option
  ; entries : Entry.t list
  }
[@@deriving fields]

let compare l r = [%compare: string] l.name r.name

let create ?level ?prereq ~name ~type_ ~entries () =
  Fields.create ~name ~type_ ~level ~prereq ~entries
;;

let to_json { name; type_; entries; level; prereq } : Yojson.t =
  let prereq =
    let level =
      match level with
      | None -> []
      | Some level -> [ "level", `Int level ]
    in
    let prereq =
      match prereq with
      | None -> []
      | Some (summary, entry) ->
        [ ( "otherSummary"
          , `Assoc [ "entrySummary", `String summary; "entry", `String entry ] )
        ]
    in
    match List.concat [ level; prereq ] with
    | [] -> []
    | prereq -> [ "prerequisite", `List [ `Assoc prereq ] ]
  in
  `Assoc
    ([ "type", `String "optfeature"
     ; "name", `String name
     ; "source", `String "ALT"
     ; "featureType", `List [ `String type_ ]
     ; "entries", `List (List.map entries ~f:Entry.to_json)
     ]
    @ prereq)
;;
