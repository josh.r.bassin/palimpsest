open! Core
open! Async

module Multiclass = struct
  type t =
    { requirements : Ability.t
    ; proficiencies_gained : Proficiencies.t
    }
  [@@deriving fields]

  let create = Fields.create

  let to_json { requirements; proficiencies_gained } : Yojson.t =
    `Assoc
      [ "requirements", Ability.to_json_assoc requirements
      ; "proficienciesGained", Proficiencies.to_json proficiencies_gained
      ]
  ;;
end

module Table = struct
  type t =
    { title : string option
    ; header : string list
    ; rows : string list list
    }
  [@@deriving fields]

  let create ?title ~header ~rows () = Fields.create ~title ~header ~rows

  let to_json { title; header; rows } : Yojson.t =
    `Assoc
      (List.concat
         [ (match title with
           | None -> []
           | Some title -> [ "title", `String title ])
         ; [ "colLabels", `List (List.map header ~f:(fun header -> `String header))
           ; ( "rows"
             , `List
                 (List.map rows ~f:(fun row ->
                      `List (List.map row ~f:(fun item -> `String item)))) )
           ]
         ])
  ;;
end

module Subclass = struct
  type t =
    { name : string
    ; short_name : string
    ; class_ : string
    ; class_source : string
    ; subclass_tables : Table.t list option
    ; features : Subclass_feature.t list
    }
  [@@deriving fields]

  let compare l r = [%compare: string] l.name r.name

  let create
      ?subclass_tables
      ?(class_source = "ALT")
      ~name
      ~short_name
      ~class_
      ~features
      ()
    =
    Fields.create ~name ~short_name ~class_ ~class_source ~subclass_tables ~features
  ;;

  let to_json { name; short_name; class_; class_source; subclass_tables; features }
      : Yojson.t
    =
    `Assoc
      (List.concat
         [ [ "name", `String name
           ; "shortName", `String short_name
           ; "source", `String "ALT"
           ; "className", `String class_
           ; "classSource", `String class_source
           ; ( "subclassFeatures"
             , `List (List.map features ~f:Subclass_feature.to_format_json) )
           ]
         ; (match subclass_tables with
           | None -> []
           | Some subclass_tables ->
             [ "subclassTableGroups", `List (List.map ~f:Table.to_json subclass_tables) ])
         ])
  ;;
end

type t =
  { name : string
  ; subclass_name : string
  ; hit_die : Die.t
  ; proficiency : Ability_name.t list
  ; starting_proficiencies : Proficiencies.t
  ; starting_equipment : Equipment.t
  ; multiclassing : Multiclass.t
  ; class_table : Table.t
  ; features : Feature.t list
  ; class_spells : [ `Spell of string | `Spell_source of string * string ] list option
  }
[@@deriving fields]

let compare l r = [%compare: string] l.name r.name

let create
    ?class_spells
    ~name
    ~subclass_name
    ~hit_die
    ~proficiency
    ~starting_proficiencies
    ~starting_equipment
    ~multiclassing
    ~class_table
    ~features
    ()
  =
  Fields.create
    ~name
    ~subclass_name
    ~hit_die
    ~proficiency
    ~starting_proficiencies
    ~starting_equipment
    ~multiclassing
    ~class_table
    ~features
    ~class_spells
;;

let to_json
    { name
    ; subclass_name
    ; hit_die
    ; proficiency
    ; starting_proficiencies
    ; starting_equipment
    ; multiclassing
    ; class_table
    ; features
    ; class_spells
    }
    : Yojson.t
  =
  `Assoc
    ([ "name", `String name
     ; "source", `String "ALT"
     ; "hd", Die.to_json hit_die
     ; "proficiency", `List (List.map proficiency ~f:Ability_name.to_json)
     ; "startingProficiencies", Proficiencies.to_json starting_proficiencies
     ; "startingEquipment", Equipment.to_json starting_equipment
     ; "multiclassing", Multiclass.to_json multiclassing
     ; "classTableGroups", `List [ Table.to_json class_table ]
     ; "classFeatures", `List (List.map features ~f:Feature.to_format_json)
     ; "subclassTitle", `String subclass_name
     ]
    @
    match class_spells with
    | None | Some [] -> []
    | Some class_spells ->
      [ ( "classSpells"
        , `List
            (List.map class_spells ~f:(function
                | `Spell spell -> `String spell
                | `Spell_source (spell, source) ->
                  `Assoc [ "name", `String spell; "source", `String source ])) )
      ])
;;
