open! Core
open! Async

type t

val small : t
val medium : t
val large : t
val to_string : t -> string
val to_json : t -> Yojson.t