open! Core
open! Async

module Armor = struct
  type t =
    | Light
    | Medium
    | Heavy
    | Shield
  [@@deriving variants]

  let to_json : t -> Yojson.t = function
    | Light -> `String "light"
    | Medium -> `String "medium"
    | Heavy -> `String "heavy"
    | Shield -> `String "{@item shield|phb|shields}"
  ;;
end

module Weapon = struct
  type t =
    | Simple
    | Martial
    | Item of string
    | Custom of string
  [@@deriving variants]

  let to_json : t -> Yojson.t = function
    | Simple -> `String "simple"
    | Martial -> `String "martial"
    | Item str -> `String [%string "{@item %{str}}"]
    | Custom str -> `String str
  ;;
end

module Tool = struct
  type t = string

  let m t = [%string "{@item %{t}}"]
  let thieves_tools = m "thieves' tools|phb"
  let tinkers_tools = m "tinker's tools|phb"
  let musical_instrument = "one musical instrument of your choice"
  let gaming_set = "one gaming set of your choice"

  let artisans_tools i =
    let i =
      match i with
      | 1 -> "one type"
      | 2 -> "two types"
      | 3 -> "three types"
      | i -> [%string "%{i#Int} types"]
    in
    [%string {|%{i} of %{m "artisan's tools|phb"} of your choice|}]
  ;;

  let to_json t : Yojson.t = `String t
end

type t =
  { armor : Armor.t list
  ; weapons : Weapon.t list
  ; tools : Tool.t list
  ; skills : Skill.t list
  }
[@@deriving fields]

let create = Fields.create

let to_json { armor; weapons; tools; skills } : Yojson.t =
  let h name t f =
    match List.is_empty t with
    | true -> []
    | false -> [ name, `List (List.map t ~f) ]
  in
  `Assoc
    (List.concat
       [ h "armor" armor Armor.to_json
       ; h "weapons" weapons Weapon.to_json
       ; h "tools" tools Tool.to_json
       ; (match skills with
         | [] -> []
         | skills -> [ "skills", `List [ Skill.to_json skills ] ])
       ])
;;
