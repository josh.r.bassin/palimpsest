open! Core
open! Async

type t =
  | Small
  | Medium
  | Large
[@@deriving variants]

let to_string t = Variants.to_name t |> String.lowercase

let to_abbrev = function
  | Small -> "S"
  | Medium -> "M"
  | Large -> "L"
;;

let to_json t : Yojson.t = `List [ `String (to_abbrev t) ]
