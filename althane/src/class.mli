open! Core
open! Async

module Multiclass : sig
  type t

  val create : requirements:Ability.t -> proficiencies_gained:Proficiencies.t -> t
end

module Table : sig
  type t

  val create : ?title:string -> header:string list -> rows:string list list -> unit -> t
end

module Subclass : sig
  type t [@@deriving compare]

  val create
    :  ?subclass_tables:Table.t list
    -> ?class_source:string
    -> name:string
    -> short_name:string
    -> class_:string
    -> features:Subclass_feature.t list
    -> unit
    -> t

  val to_json : t -> Yojson.t
end

type t [@@deriving compare]

val create
  :  ?class_spells:[ `Spell of string | `Spell_source of string * string ] list
  -> name:string
  -> subclass_name:string
  -> hit_die:Die.t
  -> proficiency:Ability_name.t list
  -> starting_proficiencies:Proficiencies.t
  -> starting_equipment:Equipment.t
  -> multiclassing:Multiclass.t
  -> class_table:Table.t
  -> features:Feature.t list
  -> unit
  -> t

val to_json : t -> Yojson.t