open! Core
open! Async

module Custom = struct
  type t =
    { name : string
    ; abbreviation : string
    ; metadata : ((string * Yojson.t) list[@sexp.opaque])
    ; entries : (Entry.t list[@sexp.opaque])
    }
  [@@deriving fields, sexp_of]

  let compare l r = [%compare: String.t] l.name r.name

  let create ?(metadata = []) ?abbreviation ~name ~entries () =
    let abbreviation = Option.value abbreviation ~default:name in
    Fields.create ~name ~abbreviation ~metadata ~entries
  ;;

  let to_json { name; abbreviation; entries; _ } : Yojson.t =
    `Assoc
      [ "abbreviation", `String abbreviation
      ; "source", `String "ALT"
      ; "entries", `List Entry.[ to_json (ne name entries) ]
      ]
  ;;
end

type t =
  | Two_handed
  | Reload of int
  | Range of (int * int)
  | Light
  | Heavy
  | Custom of Custom.t
[@@deriving compare, sexp_of, variants]

let create ?metadata ?abbreviation ~name ~entries () =
  Custom (Custom.create ?metadata ?abbreviation ~name ~entries ())
;;

let abbreviation = function
  | Two_handed -> "2H"
  | Reload _ -> "RLD"
  | Range _ -> "AF"
  | Light -> "L"
  | Heavy -> "H"
  | Custom custom -> Custom.abbreviation custom
;;

let metadata : t -> (string * Yojson.t) list = function
  | Two_handed | Light | Heavy -> []
  | Reload count -> [ "reload", `Int count ]
  | Range (short, long) -> [ "range", `String [%string "%{short#Int}/%{long#Int}"] ]
  | Custom custom -> Custom.metadata custom
;;

let to_json : t -> Yojson.t = function
  | (Two_handed | Reload _ | Range _ | Light | Heavy) as t ->
    raise_s [%message "Attempted to generate json for built-in item property" (t : t)]
  | Custom custom -> Custom.to_json custom
;;
