open! Core
open! Async

type t

val aberration : t
val beast : t
val celestial : t
val construct : t
val dragon : t
val elemental : t
val fey : t
val fiend : t
val giant : t
val humanoid : t
val monstrosity : t
val ooze : t
val plant : t
val undead : t
val to_json : t -> Yojson.t
val to_entry : t -> Entry.t