open! Core
open! Async

type t =
  | Self
  | Touch
  | Sight
  | Unlimited
  | Special
  | Miles of int
  | Point of int
  | Line of int
  | Cone of int
  | Radius of int
  | Cube of int
  | Sphere of int
  | Hemisphere of int
[@@deriving variants]

let to_json : t -> Yojson.t = function
  | Self ->
    `Assoc [ "type", `String "point"; "distance", `Assoc [ "type", `String "self" ] ]
  | Touch ->
    `Assoc [ "type", `String "point"; "distance", `Assoc [ "type", `String "touch" ] ]
  | Sight ->
    `Assoc [ "type", `String "point"; "distance", `Assoc [ "type", `String "sight" ] ]
  | Unlimited ->
    `Assoc [ "type", `String "point"; "distance", `Assoc [ "type", `String "unlimited" ] ]
  | Special -> `Assoc [ "type", `String "special" ]
  | Miles distance ->
    `Assoc
      [ "type", `String "point"
      ; "distance", `Assoc [ "type", `String "miles"; "amount", `Int distance ]
      ]
  | Point distance ->
    `Assoc
      [ "type", `String "point"
      ; "distance", `Assoc [ "type", `String "feet"; "amount", `Int distance ]
      ]
  | Line distance ->
    `Assoc
      [ "type", `String "line"
      ; "distance", `Assoc [ "type", `String "feet"; "amount", `Int distance ]
      ]
  | Cone distance ->
    `Assoc
      [ "type", `String "cone"
      ; "distance", `Assoc [ "type", `String "feet"; "amount", `Int distance ]
      ]
  | Radius distance ->
    `Assoc
      [ "type", `String "radius"
      ; "distance", `Assoc [ "type", `String "feet"; "amount", `Int distance ]
      ]
  | Cube distance ->
    `Assoc
      [ "type", `String "cube"
      ; "distance", `Assoc [ "type", `String "feet"; "amount", `Int distance ]
      ]
  | Sphere distance ->
    `Assoc
      [ "type", `String "sphere"
      ; "distance", `Assoc [ "type", `String "feet"; "amount", `Int distance ]
      ]
  | Hemisphere distance ->
    `Assoc
      [ "type", `String "hemisphere"
      ; "distance", `Assoc [ "type", `String "feet"; "amount", `Int distance ]
      ]
;;
