open! Core
open! Async

type t

val create
  :  ?s:unit
  -> ?v:unit
  -> ?m:
       [ `None
       | `Plain of string
       | `Cost of string * int
       | `Consume of string
       | `Cost_and_consume of string * int
       ]
  -> unit
  -> t

val to_json : t -> Yojson.t
