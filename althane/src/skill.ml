open! Core
open! Async

type t =
  | Athletics
  | Acrobatics
  | Sleight_of_hand
  | Stealth
  | Arcana
  | History
  | Investigation
  | Nature
  | Religion
  | Animal_handling
  | Insight
  | Medicine
  | Perception
  | Survival
  | Deception
  | Intimidation
  | Performance
  | Persuasion
  | Tool of string
  | Choose of (int * t list)
[@@deriving variants]

let to_name t =
  Variants.to_name t
  |> String.substr_replace_all ~pattern:"_" ~with_:" "
  |> String.lowercase
;;

let to_json_assoc : t -> string * Yojson.t = function
  | Choose (count, profs) ->
    ( "choose"
    , `Assoc
        [ ( "from"
          , `List
              (List.map profs ~f:(function
                  | Tool tool -> `Assoc [ "tool", `List [ `String tool ] ]
                  | prof -> `String (to_name prof))) )
        ; "count", `Int count
        ] )
  | t -> to_name t, `Bool true
;;

let to_json ts = `Assoc (List.map ts ~f:to_json_assoc)