open! Core
open! Async

module Type = struct
  type t =
    | Instant
    | Permanent of [ `Dispel | `Trigger ] list
    | Round of int
    | Minute of int
    | Hour of int
    | Day of int
  [@@deriving sexp_of, variants]

  let to_json : t -> Yojson.t = function
    | Instant -> `List [ `Assoc [ "type", `String "instant" ] ]
    | Permanent ends ->
      `List
        [ `Assoc
            [ "type", `String "permanent"
            ; ( "ends"
              , `List
                  (List.map ends ~f:(function
                      | `Dispel -> `String "dispel"
                      | `Trigger -> `String "trigger")) )
            ]
        ]
    | Round amount ->
      `List
        [ `Assoc
            [ "type", `String "timed"
            ; "duration", `Assoc [ "type", `String "round"; "amount", `Int amount ]
            ]
        ]
    | Minute amount ->
      `List
        [ `Assoc
            [ "type", `String "timed"
            ; "duration", `Assoc [ "type", `String "minute"; "amount", `Int amount ]
            ]
        ]
    | Hour amount ->
      `List
        [ `Assoc
            [ "type", `String "timed"
            ; "duration", `Assoc [ "type", `String "hour"; "amount", `Int amount ]
            ]
        ]
    | Day amount ->
      `List
        [ `Assoc
            [ "type", `String "timed"
            ; "duration", `Assoc [ "type", `String "day"; "amount", `Int amount ]
            ]
        ]
  ;;
end

type t =
  { concentration : bool
  ; type_ : Type.t
  }
[@@deriving fields]

let create = Fields.create

let to_json { concentration; type_ } : Yojson.t =
  match concentration with
  | false -> Type.to_json type_
  | true ->
    (match Type.to_json type_ with
    | `List (`Assoc type_ :: _) ->
      `List [ `Assoc (type_ @ [ "concentration", `Bool true ]) ]
    | _ -> raise_s [%message "Impossible state in duration" (type_ : Type.t)])
;;
