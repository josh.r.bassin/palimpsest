open! Core
open! Async

type t =
  | Abjuration
  | Conjuration
  | Divination
  | Enchantment
  | Evocation
  | Illusion
  | Necromancy
  | Transmutation
[@@deriving variants]

let to_string = function
  | Abjuration -> "A"
  | Conjuration -> "C"
  | Divination -> "D"
  | Enchantment -> "E"
  | Evocation -> "V"
  | Illusion -> "I"
  | Necromancy -> "N"
  | Transmutation -> "T"
;;

let to_json t : Yojson.t = `String (to_string t)
