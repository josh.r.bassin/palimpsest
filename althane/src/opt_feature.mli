open! Core
open! Async

type t [@@deriving compare]

val create
  :  ?level:int
  -> ?prereq:string * string
  -> name:string
  -> type_:string
  -> entries:Entry.t list
  -> unit
  -> t

val name : t -> string
val type_ : t -> string
val entries : t -> Entry.t list
val level : t -> int option
val prereq : t -> (string * string) option
val to_json : t -> Yojson.t
