open! Core
open! Async

type t

val any : int -> t
val abyssal : t
val celestial : t
val common : t
val deep_speech : t
val draconic : t
val dwarvish : t
val elvish : t
val giant : t
val gnomish : t
val goblin : t
val halfling : t
val infernal : t
val orc : t
val primordial : t
val sylvan : t
val to_json : t list -> Yojson.t
val to_entry : t list -> Entry.t