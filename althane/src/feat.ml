open! Core
open! Async

type t =
  { name : string
  ; entries : Entry.t list
  }
[@@deriving fields]

let compare l r = [%compare: String.t] l.name r.name
let create = Fields.create

let to_json { name; entries } : Yojson.t =
  `Assoc
    [ "name", `String name
    ; "source", `String "ALT"
    ; "entries", `List (List.map entries ~f:Entry.to_json)
    ]
;;
