open! Core
open! Async

type t

val mundane : t
val uncommon : t
val rare : t
val very_rare : t
val legendary : t
val artifact : t
val to_json : t -> Yojson.t
