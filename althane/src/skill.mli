open! Core
open! Async

type t

val athletics : t
val acrobatics : t
val sleight_of_hand : t
val stealth : t
val arcana : t
val history : t
val investigation : t
val nature : t
val religion : t
val animal_handling : t
val insight : t
val medicine : t
val perception : t
val survival : t
val deception : t
val intimidation : t
val performance : t
val persuasion : t
val tool : string -> t
val choose : int * t list -> t
val to_json : t list -> Yojson.t