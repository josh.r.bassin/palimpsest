open! Core
open! Async

module Rank : sig
  type t

  val age : t
  val creature_type : t
  val size : t
  val language : t
  val level : int -> t
end

type t

val create : rank:Rank.t -> entry:Entry.t -> t
val to_entries : t list -> Entry.t list
val standard : age:string -> creature_type:Creature_type.t -> size:string -> t list
val s : string -> t

val tbl
  :  title:string
  -> labels:string list
  -> style:string list
  -> rows:string list list
  -> t

val us : Entry.t list -> t
val ns : string -> Entry.t list -> t
val ue : Entry.t list -> t
val ne : string -> Entry.t list -> t
val ln : Entry.t list -> t
val lh : Entry.t list -> t

(** string to t *)
val ( ! ) : string -> t

(** section, many *)
val ( <~> ) : string -> string list -> t

(** entry, many *)
val ( <-> ) : string -> string list -> t

(** section, one *)
val ( ~~> ) : string -> string -> t

(** entry, one *)
val ( --> ) : string -> string -> t

(** string to item t *)
val ( !! ) : string -> t

(** named item *)
val ( -!> ) : string -> string -> t