open! Core
open! Async

type t

val abjuration : t
val conjuration : t
val divination : t
val enchantment : t
val evocation : t
val illusion : t
val necromancy : t
val transmutation : t
val to_json : t -> Yojson.t
