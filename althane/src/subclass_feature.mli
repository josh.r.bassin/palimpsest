open! Core
open! Async

type t [@@deriving compare]

val create
  :  ?class_source:string
  -> name:string
  -> class_:string
  -> subclass:string
  -> level:int
  -> entries:Entry.t list
  -> unit
  -> t

val to_format_json : t -> Yojson.t
val to_json : t -> Yojson.t
