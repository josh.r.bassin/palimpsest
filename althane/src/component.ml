open! Core
open! Async

type t =
  { somatic : bool
  ; verbal : bool
  ; material :
      [ `None
      | `Plain of string
      | `Cost of string * int
      | `Consume of string
      | `Cost_and_consume of string * int
      ]
  }
[@@deriving fields]

let create ?s ?v ?(m = `None) () =
  let somatic =
    match s with
    | Some () -> true
    | None -> false
  in
  let verbal =
    match v with
    | Some () -> true
    | None -> false
  in
  let material = m in
  Fields.create ~somatic ~verbal ~material
;;

let to_json { somatic; verbal; material } : Yojson.t =
  `Assoc
    ((match somatic with
     | false -> []
     | true -> [ "s", `Bool true ])
    @ (match verbal with
      | false -> []
      | true -> [ "v", `Bool true ])
    @
    match material with
    | `None -> []
    | `Plain s -> [ "m", `String s ]
    | `Cost (s, c) -> [ "m", `Assoc [ "text", `String s; "cost", `Int c ] ]
    | `Consume s -> [ "m", `Assoc [ "text", `String s; "consume", `Bool true ] ]
    | `Cost_and_consume (s, c) ->
      [ "m", `Assoc [ "text", `String s; "cost", `Int c; "consume", `Bool true ] ])
;;
