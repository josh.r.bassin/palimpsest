open! Core
open! Async

type t =
  { name : string
  ; class_ : string
  ; class_source : string
  ; level : int
  ; gain_subclass : bool
  ; entries : Entry.t list
  }
[@@deriving fields]

let compare l r = [%compare: string] l.name r.name

let create ?(class_source = "ALT") ~name ~class_ ~level ~gain_subclass ~entries () =
  Fields.create ~name ~class_ ~class_source ~level ~gain_subclass ~entries
;;

let to_format_json { name; level; class_; class_source; gain_subclass; _ } : Yojson.t =
  let class_source =
    match class_source with
    | "PHB" -> ""
    | class_source -> class_source
  in
  let format_string =
    `String [%string "%{name}|%{class_}|%{class_source}|%{level#Int}"]
  in
  match gain_subclass with
  | false -> format_string
  | true -> `Assoc [ "classFeature", format_string; "gainSubclassFeature", `Bool true ]
;;

let to_json { name; class_; class_source; level; entries; _ } : Yojson.t =
  `Assoc
    [ "name", `String name
    ; "source", `String "ALT"
    ; "className", `String class_
    ; "classSource", `String class_source
    ; "level", `Int level
    ; "entries", `List (List.map entries ~f:Entry.to_json)
    ]
;;
