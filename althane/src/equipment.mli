open! Core
open! Async

type t

val of_string_list : string list -> t
val to_json : t -> Yojson.t