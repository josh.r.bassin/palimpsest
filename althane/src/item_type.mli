open! Core
open! Async

type t [@@deriving compare]

val abbreviation : t -> string

val create
  :  ?metadata:(string * Yojson.t) list
  -> ?abbreviation:string
  -> name:string
  -> type_:string
  -> entries:Entry.t list
  -> unit
  -> t

val metadata : t -> (string * Yojson.t) list
val to_json : t -> Yojson.t
