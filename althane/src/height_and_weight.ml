open! Core
open! Async

type t =
  { base_height : int
  ; mod_height : string
  ; base_weight : int
  ; mod_weight : string
  }

let create ~h ~mh ~w ~mw =
  { base_height = h; mod_height = mh; base_weight = w; mod_weight = mw }
;;

let to_json { base_height; mod_height; base_weight; mod_weight } : Yojson.t =
  `Assoc
    [ "baseHeight", `Int base_height
    ; "heightMod", `String mod_height
    ; "baseWeight", `Int base_weight
    ; "weightMod", `String mod_weight
    ]
;;
