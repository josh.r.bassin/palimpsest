open! Core
open! Async

type t

val action : t
val bonus : t
val minute : int -> t
val hour : int -> t
val reaction : string -> t
val to_json : t -> Yojson.t
