open! Core
open! Async

type t =
  { name : string
  ; type_ : Item_type.t
  ; rarity : Rarity.t
  ; properties : Item_property.t list
  ; entries : Entry.t list
  }
[@@deriving fields]

let compare l r = [%compare: String.t] l.name r.name
let create = Fields.create

let to_json { name; type_; rarity; properties; entries } : Yojson.t =
  `Assoc
    ([ "name", `String name
     ; "source", `String "ALT"
     ; "type", `String (Item_type.abbreviation type_)
     ; "rarity", Rarity.to_json rarity
     ; ( "property"
       , `List
           (List.map properties ~f:(fun property ->
                `String (Item_property.abbreviation property))) )
     ; "entries", `List (List.map entries ~f:Entry.to_json)
     ]
    @ (Item_type.metadata type_ :: List.map properties ~f:Item_property.metadata
      |> List.concat
      |> String.Map.of_alist_reduce ~f:(fun l _ -> l)
      |> String.Map.to_alist))
;;
