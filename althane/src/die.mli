open! Core
open! Async

type t

val create : value:int -> faces:int -> t
val to_json : t -> Yojson.t