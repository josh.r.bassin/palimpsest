open! Core
open! Async

type t =
  { name : string
  ; type_ : string
  ; abbreviation : string
  ; metadata : (string * Yojson.t) list
  ; entries : Entry.t list
  }
[@@deriving fields]

let compare l r = [%compare: String.t] l.name r.name

let create ?(metadata = []) ?abbreviation ~name ~type_ ~entries () =
  let abbreviation = Option.value abbreviation ~default:name in
  Fields.create ~name ~type_ ~abbreviation ~metadata ~entries
;;

let metadata { metadata; _ } = metadata

let to_json { name; type_; abbreviation; entries; _ } : Yojson.t =
  `Assoc
    [ "abbreviation", `String abbreviation
    ; "source", `String "ALT"
    ; "name", `String type_
    ; "entries", `List Entry.[ to_json (ne name entries) ]
    ]
;;
