open! Core
open! Async

module Table = struct
  type t =
    { title : string
    ; labels : string list
    ; style : string list
    ; rows : string list list
    }
  [@@deriving fields, sexp_of]

  let create = Fields.create

  let to_json { title; labels; style; rows } : Yojson.t =
    `Assoc
      [ "type", `String "table"
      ; "caption", `String title
      ; "colLabels", `List (List.map labels ~f:(fun label -> `String label))
      ; "colStyles", `List (List.map style ~f:(fun style -> `String style))
      ; ( "rows"
        , `List
            (List.map rows ~f:(fun row ->
                 `List (List.map row ~f:(fun entry -> `String entry)))) )
      ]
  ;;
end

type 'a named =
  | Named of string * 'a
  | Unnamed of 'a
[@@deriving sexp_of]

and t =
  | Section of t list named
  | Entries of t list named
  | Item of t named
  | List_nohang of t list
  | List_hang of t list
  | Table of Table.t
  | Save_dc of (string * Ability_name.t)
  | Spell_attack of (string * Ability_name.t)
  | Optional_feature of string list
  | String of string
[@@deriving sexp_of]

let s s = String s
let tbl ~title ~labels ~style ~rows = Table (Table.create ~title ~labels ~style ~rows)
let us e = Section (Unnamed e)
let ns n e = Section (Named (n, e))
let ue e = Entries (Unnamed e)
let ne n e = Entries (Named (n, e))
let ui e = Item (Unnamed e)
let ni n e = Item (Named (n, e))
let ln e = List_nohang e
let lh e = List_hang e
let sd n s = Save_dc (n, s)
let sa n s = Spell_attack (n, s)
let op s = Optional_feature s
let ( ! ) = s
let ( <~> ) n e = ns n (List.map e ~f:s)
let ( <-> ) n e = ne n (List.map e ~f:s)
let ( ~~> ) n e = ns n [ !e ]
let ( --> ) n e = ne n [ !e ]
let ( !! ) s = ui !s
let ( -!> ) n s = ni n !s

let rec to_json t : Yojson.t =
  match t with
  | String s -> `String s
  | Section (Unnamed e) ->
    `Assoc [ "type", `String "section"; "entries", `List (List.map e ~f:to_json) ]
  | Section (Named (n, e)) ->
    `Assoc
      [ "type", `String "section"
      ; "name", `String n
      ; "entries", `List (List.map e ~f:to_json)
      ]
  | Entries (Unnamed e) ->
    `Assoc [ "type", `String "entries"; "entries", `List (List.map e ~f:to_json) ]
  | Entries (Named (n, e)) ->
    `Assoc
      [ "type", `String "entries"
      ; "name", `String n
      ; "entries", `List (List.map e ~f:to_json)
      ]
  | Item (Unnamed e) -> `Assoc [ "type", `String "item"; "entry", to_json e ]
  | Item (Named (n, e)) ->
    `Assoc [ "type", `String "item"; "name", `String n; "entry", to_json e ]
  | List_nohang e ->
    `Assoc
      [ "type", `String "list"
      ; "style", `String "list-hang-notitle"
      ; "items", `List (List.map e ~f:to_json)
      ]
  | List_hang e ->
    `Assoc [ "type", `String "list"; "items", `List (List.map e ~f:to_json) ]
  | Table t -> Table.to_json t
  | Save_dc (n, s) ->
    `Assoc
      [ "type", `String "abilityDc"
      ; "name", `String n
      ; "attributes", `List [ Ability_name.to_json s ]
      ]
  | Spell_attack (n, s) ->
    `Assoc
      [ "type", `String "abilityAttackMod"
      ; "name", `String n
      ; "attributes", `List [ Ability_name.to_json s ]
      ]
  | Optional_feature s ->
    `Assoc
      [ "type", `String "options"
      ; "count", `Int 1
      ; ( "entries"
        , `List
            (List.map s ~f:(fun optional_feature ->
                 `Assoc
                   [ "type", `String "refOptionalfeature"
                   ; "optionalfeature", `String optional_feature
                   ])) )
      ]
;;

let info ~name ~title ~intro ~physical_qualities ~playstyle =
  ns
    [%string "%{name}: %{title}"]
    [ s intro
    ; ne "Physical Qualities" physical_qualities
    ; ne [%string "Playing a %{name}"] playstyle
    ]
;;

module Vestige = struct
  let sanitize =
    let open Re in
    let outer_re =
      seq
        [ str "{@"
        ; alt [ str "spell"; str "condition"; str "dice"; str "item"; str "skill" ]
        ; str " "
        ; group (shortest (rep any))
        ; str "}"
        ]
      |> compile
    in
    let multi_pipe_inner_re =
      let not_pipe = diff any (char '|') in
      seq
        [ shortest (rep not_pipe)
        ; char '|'
        ; shortest (rep not_pipe)
        ; char '|'
        ; group (rep any)
        ]
      |> compile
    in
    let single_pipe_inner_re =
      let not_pipe = diff any (char '|') in
      seq [ group (shortest (rep not_pipe)); char '|'; group (rep any) ] |> compile
    in
    replace ~all:true outer_re ~f:(fun group ->
        Group.get group 1
        |> replace ~all:true multi_pipe_inner_re ~f:(fun group -> Group.get group 1)
        |> replace ~all:true single_pipe_inner_re ~f:(fun group -> Group.get group 1))
  ;;

  let rec inner_to_readable = function
    | String s -> sanitize s
    | Item (Unnamed s) | Item (Named (_, s)) -> [%string "  - %{inner_to_readable s}"]
    | Entries (Named (n, e)) ->
      let e = List.map e ~f:inner_to_readable |> String.concat ~sep:"\n" in
      [%string "%{n} -- %{e}"]
    | List_hang e | List_nohang e | Entries (Unnamed e) ->
      List.map e ~f:inner_to_readable |> String.concat ~sep:"\n"
    | e -> raise_s [%message "Failed to make readable" (e : t)]
  ;;

  let ingest_entries entries =
    List.filter_map entries ~f:(function
        | Entries (Named (n, e)) -> Some (n, List.map e ~f:inner_to_readable)
        | _ -> None)
  ;;

  let ingest_traits entries =
    List.filter_map entries ~f:(function
        | Section (Named (n, e)) -> Some (n, List.map e ~f:inner_to_readable)
        | _ -> None)
  ;;
end