open! Core
open! Async

type t =
  | Action
  | Bonus
  | Minute of int
  | Hour of int
  | Reaction of string
[@@deriving variants]

let to_json : t -> Yojson.t = function
  | Action -> `List [ `Assoc [ "unit", `String "action"; "number", `Int 1 ] ]
  | Bonus -> `List [ `Assoc [ "unit", `String "bonus"; "number", `Int 1 ] ]
  | Minute number -> `List [ `Assoc [ "unit", `String "minute"; "number", `Int number ] ]
  | Hour number -> `List [ `Assoc [ "unit", `String "hour"; "number", `Int number ] ]
  | Reaction condition ->
    `List
      [ `Assoc
          [ "unit", `String "action"; "number", `Int 1; "condition", `String condition ]
      ]
;;
