open! Core
open! Async

type t =
  | Strength
  | Dexterity
  | Constitution
  | Wisdom
  | Intelligence
  | Charisma
[@@deriving sexp_of, variants]

let to_json t : Yojson.t =
  `String (Variants.to_name t |> String.lowercase |> Fn.flip String.prefix 3)
;;
