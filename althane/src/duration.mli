open! Core
open! Async

module Type : sig
  type t

  val instant : t
  val permanent : [ `Dispel | `Trigger ] list -> t
  val round : int -> t
  val minute : int -> t
  val hour : int -> t
  val day : int -> t
end

type t

val create : concentration:bool -> type_:Type.t -> t
val to_json : t -> Yojson.t
