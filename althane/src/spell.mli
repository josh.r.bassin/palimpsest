open! Core
open! Async

type t [@@deriving compare]

val create
  :  ?entries_at_higher_level:Entry.t list
  -> name:string
  -> level:int
  -> school:School.t
  -> aspect:Aspect.t
  -> time:Casting_time.t
  -> range:Range.t
  -> components:Component.t
  -> duration:Duration.t
  -> entries:Entry.t list
  -> classes:(string * string) list
  -> unit
  -> t

val to_json : t -> Yojson.t
