open! Core
open! Async

module Rank = struct
  type t =
    | Age
    | Creature_type
    | Size
    | Language
    | Level of int
  [@@deriving variants]

  let level_rank = 100

  let to_int = function
    | Level n -> n + 100
    | t -> Variants.to_rank t
  ;;

  let compare l r =
    let l = to_int l in
    let r = to_int r in
    Int.compare l r
  ;;
end

type t =
  { rank : Rank.t
  ; entry : Entry.t
  }
[@@deriving fields]

let compare { rank = l_rank; _ } { rank = r_rank; _ } = Rank.compare l_rank r_rank
let create = Fields.create
let to_entries ts = List.sort ts ~compare |> List.map ~f:(fun { entry; _ } -> entry)

let standard ~age ~creature_type ~size =
  Entry.
    [ create ~rank:Rank.age ~entry:("Age" --> age)
    ; create ~rank:Rank.creature_type ~entry:(Creature_type.to_entry creature_type)
    ; create ~rank:Rank.size ~entry:("Size" --> size)
    ]
;;

open Entry

let c entry = create ~rank:(Rank.level Rank.level_rank) ~entry
let s e = c (s e)
let tbl ~title ~labels ~style ~rows = c (tbl ~title ~labels ~style ~rows)
let us e = c (us e)
let ns n e = c (ns n e)
let ue e = c (ue e)
let ne n e = c (ne n e)
let ln e = c (ln e)
let lh e = c (lh e)
let ( ! ) s = c !s
let ( <~> ) n e = c (n <~> e)
let ( <-> ) n e = c (n <-> e)
let ( ~~> ) n e = c (Entry.( ~~> ) n e)
let ( --> ) n e = c (n --> e)
let ( !! ) s = c !!s
let ( -!> ) n s = c (n -!> s)