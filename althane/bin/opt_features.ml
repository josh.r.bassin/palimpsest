open! Core
open! Async
open! Gen_lib
open! Opt_feature

module Binder = struct
  let minor_fragments =
    List.map
      ~f:(fun (name, entries) -> create ~name ~type_:"Minor Fragment" ~entries ())
      Entry.
        [ ( "Blade"
          , [ !"The remnants of an intelligent item's soul, a Blade manifests as a \
                faint, ethereal weapon. As a bonus action, you can make a melee spell \
                attack with it against a target within 5 feet of you, dealing 1d8 \
                slashing damage on a hit."
            ; !"Additionally, you can use your action to transform your Blade into a \
                shield or return it to its normal form. You can’t make a melee spell \
                attack with your Blade while it is transformed."
            ] )
        ; ( "Chill"
          , [ !"A Chill is a minor elemental fragment resembling a multifaceted \
                snowflake. As a bonus action, you can make a ranged spell attack with it \
                against any creature within 30 feet of you, dealing 1d6 cold damage on a \
                hit."
            ; !"Additionally, as an action, you can also use the Chill to freeze a \
                handheld object, create an icicle, or extinguish a torch or small \
                campfire."
            ] )
        ; ( "Glitch"
          , [ !"Abruptly shifting and flashing, the Glitch is a time-lost fragment from \
                a distant land. As a bonus action, you can make a ranged spell attack \
                with it against any creature within 120 feet of you, dealing 1d4 force \
                damage on a hit. The Glitch ignores half cover and three-quarters cover \
                as it clips through solid objects."
            ] )
        ; ( "Grue"
          , [ !"A ravenous fragment that haunts dark places, the Grue is feared for its \
                stealth and acidic saliva. As a bonus action, you can use the Grue to \
                cause a creature within 15 feet to make a Dexterity saving throw or take \
                1d6 acid damage. If the target is in darkness, it has disadvantage on \
                its saving throw."
            ] )
        ; ( "Haunt"
          , [ !"A Haunt is a fragment of regret or woe which persists long its death. As \
                a bonus action, you can use the Haunt to cause a creature within 30 feet \
                to make a Dexterity saving throw or take 1d6 necrotic damage."
            ; !"Additionally, as an action, you can use the Haunt to project faint, \
                ethereal noises or create up to four ghostly lights which move as you \
                direct. These effects must remain within 30 feet of you, and last until \
                the beginning of your next turn."
            ] )
        ; ( "Lantern"
          , [ !"A minor divine fragment of pure goodness, a lantern manifest as a \
                fist-sized ball of light. As a bonus action, you can use the lantern to \
                cause a creature within 30 feet to make a Dexterity saving throw or take \
                1d6 radiant damage."
            ; !"The lantern sheds light as a torch. You can use your action to brighten \
                the lantern such that it sheds bright light in a 40-foot radius and dim \
                light an additional 40 feet until the beginning of your next turn."
            ] )
        ; ( "Stone"
          , [ !"A Stone is a rocky, hovering elemental fragment, the smallest unit of \
                living elemental earth. As a bonus action, you can make a melee spell \
                attack with it against a target within 5 feet of you, dealing 1d8 \
                bludgeoning damage on a hit."
            ; !"Alternatively, you can throw the Stone up to 30 feet as an improvised \
                weapon. After being thrown, the Stone returns to you at the beginning of \
                your turn."
            ] )
        ; ( "Spark"
          , [ !"A Spark is a minor elemental fragment, resembling a small blue bolt of \
                crackling lightning. As a bonus action, you can make a ranged spell \
                attack with it against any creature within 30 feet of you, dealing 1d6 \
                lightning damage on a hit. You can repeat this attack roll against a \
                second target within 5 feet of the first if both targets are wearing \
                metal armor."
            ] )
        ; ( "Strange"
          , [ !"The shifting, incomprehensible form of a Strange must originate in a \
                far-off dimension whose rules differ from our own. As a bonus action, \
                you can use the Strange to cause a creature within 60 feet to make a \
                Wisdom saving throw or take 1d4 psychic damage."
            ; !"As an action, you can use the Strange to cloud the thoughts of a \
                creature within 30 feet with bizarre images, making it impossible for \
                its thoughts to be read or for it to use telepathy until the end of your \
                next turn."
            ] )
        ; ( "Torchling"
          , [ !"A Torchling is a flickering, living flame, a minor elemental fragment of \
                elemental fire. As a bonus action, you can make a ranged spell attack \
                with it against any creature within 60 feet of you, dealing 1d6 fire \
                damage on a hit."
            ; !"Additionally, as an action, you can use the Torchling to start a fire, \
                melt snow or ice, or boil water."
            ] )
        ; ( "Totem"
          , [ !"A Totem is a manifestation of an animal fragment. As a bonus action, you \
                can make a melee spell attack with the Totem bite against a target \
                within 5 feet of you, dealing 1d8 piercing damage on a hit."
            ; !"Additionally, you can use your action to channel your Totem animal \
                instincts, allowing you to make a Wisdom (Perception) check that relies \
                on scent with advantage."
            ] )
        ; ( "Wisp"
          , [ !"This wisp is a faintly-glowing fragment of capricious fey energy which \
                produces poisonous spores. As a bonus action, you can use the wisp to \
                cause a creature within 15 feet to make a Constitution saving throw or \
                take 1d8 poison damage."
            ; !"The wisp shines light as a torch. As an action, you can cause the wisp \
                and its light to be visible only to yourself until the end of your next \
                turn."
            ] )
        ]
  ;;

  let vestiges =
    List.map
      ~f:(fun (name, level, entries) ->
        let prereq =
          ( "—"
          , let level =
              match level with
              | 1 -> "1st-level"
              | 2 -> "2nd-level"
              | 3 -> "3rd-level"
              | x -> [%string "%{x#Int}th-level"]
            in
            [%string "can bind %{level} vestiges"] )
        in
        let level =
          match level with
          | 2 -> Some 3
          | 3 -> Some 5
          | 4 -> Some 7
          | 5 -> Some 9
          | 6 -> Some 11
          | 7 -> Some 13
          | 8 -> Some 15
          | 9 -> Some 17
          | _ -> None
        in
        create ?level ~prereq ~name ~type_:"Vestige" ~entries ())
      Entry.
        [ ( "Trickery"
          , 1
          , [ "Bonus Proficiencies"
              --> "While bound to Trickery, you gain proficiency with Deception and \
                   Persuasion. Additionally, Trickery steals power from other vestiges, \
                   granting you proficiency in one additional skill or tool of your \
                   choice for each other vestige you have bound."
            ; "Deep Pockets"
              --> "While bound to Trickery, a pocket, bag, or other container of your \
                   choice becomes a portal to a personal extradimensional space, which \
                   is 64 cubic feet in volume. The container’s opening stretches to \
                   accommodate items of any size which can fit within the space, and \
                   items within the space are weightless until removed. When you reach \
                   into this space, any item you intend to take is magically on top. A \
                   container loses this property and its contents are expelled when you \
                   are no longer bound to Trickery."
            ; "Persuasive Words"
              --> "You can cast the spell {@spell charm person} once without expending a \
                   spell slot. Once you cast this spell, you can’t cast it again in this \
                   way until you finish a short or long rest."
            ; "Trait: Blue Tongue"
              <~> [ "While bound to Trickery, you can cast the spell {@spell disguise \
                     self} without using a spell slot or spell components. Casting the \
                     spell in this fashion requires 1 minute. No matter what your \
                     appearance, however, whenever you speak, a serpentine blue tongue \
                     can be seen within your mouth."
                  ]
            ] )
        ; ( "Guilt"
          , 1
          , [ "Bonus Proficiencies"
              --> "While bound to Guilt, you gain proficiency with {@item \
                   shield|phb|shields}, as well as with {@item \
                   battleaxe|phb|battleaxes}, {@item longsword|phb|longswords}, {@item \
                   spear|phb|spears}, {@item trident|phb|tridents}, and {@item \
                   warhammer|phb|warhammers}."
            ; "Legion Tactics"
              --> "While you are wielding a shield in one hand and a versatile weapon in \
                   the other, you can use the weapon’s two-handed damage die."
            ; "Coup de Grâce"
              --> "When you take the Attack action on your turn, you can use your bonus \
                   action to make one additional melee weapon attack. On a hit, this \
                   attack deals additional damage equal to your binder level. Once you \
                   use this ability, you can’t use it again until you finish a short or \
                   long rest."
            ; "Trait: Bloodstained"
              <~> [ "You are stained with the blood of saints, which never washes off. \
                     Immediately after you take damage from a melee attack, you can use \
                     your reaction to gain 5 temporary hit points, which last until the \
                     end of your next turn. The amount of the temporary hit points you \
                     gain increases by 5 for each vestige other than Guilt you have \
                     bound."
                  ]
            ] )
        ; ( "Fiction"
          , 1
          , [ "Heretical Lore"
              --> "You have advantage on ability checks you make to recall legends, \
                   myths, or lore. Additionally, the GM can allow you to make such \
                   checks, even when it would be impossible for you to know such \
                   information."
            ; "Strength in Numbers"
              --> "While bound to Fiction, you gain a bonus to your vestige spell save \
                   DC, spell attack modifier, and any attack roll you make which uses \
                   your Charisma, instead of Strength or Dexterity. This bonus equals to \
                   the number of other vestiges you have bound and does not stack with \
                   bonuses provided by magic weapons or items."
            ; "Legendary Vestige"
              --> "If a creature succeeds on a saving throw against a spell or feature \
                   offered to you by one of your vestiges, you can force that creature \
                   to repeat that save. Once you use this ability, you can’t use it \
                   again until you finish a long rest."
            ; "Trait: Hidden"
              <~> [ "While bound to Fiction, you can cloak your vestiges with ease. You \
                     can suppress or reveal any of your vestiges' traits one on each of \
                     your turns as a free action. Moreover, you can instantly tell when \
                     another creature is bound to vestiges, and how many vestiges to \
                     which they are bound. You have advantage on attack rolls against \
                     other creatures bound to vestiges."
                  ]
            ] )
        ; ( "Thievery"
          , 1
          , [ "Bonus Proficiencies"
              --> "While bound to Thievery, you gain proficiency with {@item \
                   scimitar|phb|scimitars}, {@item shortsword|phb|shortswords}, and \
                   {@item thieves' tools|phb}."
            ; "Sneak Attack"
              <-> [ "While bound to Thievery, once per turn, you can deal an extra \
                     {@dice 1d6} damage to one creature you hit with an attack if you \
                     have advantage on the attack roll. The attack must use a finesse or \
                     a ranged weapon. You don’t need advantage on the attack roll if \
                     another enemy of the target is within 5 feet of it, that enemy \
                     isn’t incapacitated, and you don’t have disadvantage on the attack \
                     roll."
                  ; "The amount of the extra damage increases by {@dice 1d6} for each \
                     vestige other than Thievery you have bound. If you already have \
                     Sneak Attack from another class feature, you add this damage to the \
                     Sneak Attack roll."
                  ]
            ; "Thief's Instincts"
              --> "You can take the Dash, Disengage, or Hide action as a bonus action. \
                   You can use this ability twice and regain all expended uses when you \
                   finish a short or long rest."
            ; "Trait: Marked"
              <~> [ "While bound to Thievery, your skin becomes branded with all manner \
                     of ancient runes and symbols, which magically silence your \
                     movements. You don’t gain disadvantage on Dexterity (Stealth) \
                     checks for wearing any type of armor. Additionally, if you make a \
                     Dexterity (Stealth) check, you can treat the result as 10, or your \
                     binder level plus your Charisma modifier, whichever is higher."
                  ]
            ] )
        ; ( "Arcana"
          , 1
          , [ "Words of Power"
              --> "While bound to Arcana, you learn 2 cantrips of your choice from the \
                   bard, sorcerer, or wizard spell list, plus an additional cantrip for \
                   each other vestige you have bound. Charisma is your spellcasting \
                   modifier for these cantrips."
            ; "Pale Arcana"
              --> "While bound to Arcana, whenever you take damage from a spell, you can \
                   use your reaction to gain resistance to the damage taken."
            ; ne
                "Spellcasting: Mystic Utterances"
                [ !"While bound to Arcana, you can cast the following spells without \
                    using spell slots or spell components:"
                ; ue
                    [ !(List.map
                          [ "detect magic"
                          ; "feather fall"
                          ; "tenser's floating disk"
                          ; "fog cloud"
                          ; "mage armor"
                          ; "magic missile"
                          ; "shield"
                          ; "sleep"
                          ; "thunderwave"
                          ; "unseen servant"
                          ]
                          ~f:(fun spell -> [%string "{@spell %{spell}}"])
                       |> String.concat ~sep:", ")
                    ]
                ; !"You can cast a spell from this list twice, plus one additional time \
                    for each vestige other than Arcana you have bound. You regain all \
                    expended uses when you finish a long rest."
                ; !"Additionally, you can cast any spell from this list as a ritual if \
                    it has the ritual tag."
                ]
            ; "Trait: Glossolalia"
              <~> [ "You frequently slip into a language that mixes all known (and \
                     unknown) forms of speech, and your writing at a glance seems to be \
                     gibberish. Despite this, your speech and writing are comprehensible \
                     by any creature that can understand a language. As well, you can \
                     understand and read any language."
                  ]
            ] )
        ; ( "Health"
          , 2
          , [ "Triage"
              --> "While bound to Health, you know whether each creature you see has all \
                   its hit points, more than half of its hit points, less than half of \
                   its hit points, or less than 10 hit points. You also know if a \
                   creature you see is cursed, poisoned, or diseased."
            ; "Bloodletting"
              --> "For all Health's insight, their methods can be quite brutal. Once on \
                   each of your turns when you deal bludgeoning, piercing, or slashing \
                   damage to a creature, you can add a d4 to the damage dice."
            ; "Healing Balm"
              --> "While bound to Health, you can use your action to touch a humanoid, \
                   which regains hit points equal to your binder level plus your \
                   Charisma modifier. You can also end one disease afflicting the \
                   creature or end the blinded, deafened, poisoned condition affecting \
                   it. You can use this ability three times and regain all expended uses \
                   when you finish a long rest."
            ; "Trait: Serpent Staff"
              <~> [ "While bound to Health, their serpent materializes and coils on your \
                     arm, or on a staff, tool, or a weapon you are holding, and whispers \
                     medicinal wisdom in your ear. If you make a Wisdom (Medicine) check \
                     while bound to this vestige, you can treat the result as 10, or \
                     your binder level plus your Charisma modifier, whichever is higher."
                  ]
            ] )
        ; ( "Accuracy"
          , 2
          , [ "Bonus Proficiencies"
              --> "While bound to Accuracy, you gain proficiency with {@item \
                   blowgun|phb|blowguns}, {@item hand crossbow|phb|hand crossbows}, \
                   {@item heavy crossbow|phb|heavy crossbows}, {@item \
                   longbow|phb|longbows}, and {@item net|phb|nets}."
            ; "Fighting Style: Archery"
              --> "You gain a +2 bonus to attack rolls you make with ranged weapons."
            ; "Sunkiller's Quiver"
              <-> [ "Whenever you would draw a weapon, you can summon the antique, but \
                     exquisitely crafted longbow and quiver used by Accuracy. The quiver \
                     contains an unlimited supply of regular arrows and 9 sunkiller \
                     arrows. This equipment lasts until you dismiss it on your turn (no \
                     action required) or you are no longer bound to Accuracy"
                  ; "A sunkiller arrow deals fire damage instead of piercing damage and \
                     deals an additional 1d4 fire damage on a hit. When a sunkiller \
                     arrow hits a target, it explodes in a 5-foot radius sphere and is \
                     destroyed. The arrow can be fired at an unoccupied space within its \
                     range. Each creature other than the target within the blast radius \
                     must succeed on a Dexterity saving throw, taking half the damage \
                     rolled on a failed save or no damage on a successful one."
                  ; "Once a sunkiller arrow is used, it can’t be used again until you \
                     finish a long rest."
                  ]
            ; "Trait: Eagle's Eyes"
              <~> [ "While bound to Accuracy, your eyes are replaced with that of an \
                     eagle’s, bordered by resplendent feathers. Because of this, you can \
                     use your Charisma, instead of your Dexterity modifier, for attacks \
                     and damage rolls with ranged weapon attacks."
                  ; "Additionally, if you make a Wisdom (Perception) check that relies \
                     on sight, you can treat the result as 10, or your binder level plus \
                     your Charisma modifier, whichever is higher."
                  ]
            ] )
        ; ( "Growth"
          , 2
          , [ "Bonus Proficiencies"
              --> "While bound to Growth, you gain proficiency with martial weapons."
            ; "Fighting Style: Great Weapon Fighting"
              --> "When you roll a 1 or 2 on a damage die for an attack you make with a \
                   melee weapon that you are wielding with two hands, you can reroll the \
                   die and must use the new roll, even if the new roll is a 1 or a 2. \
                   The weapon must have the Two-Handed or Versatile property for you to \
                   gain this benefit."
            ; "Gigantic Size"
              --> "You can cast the {@spell enlarge/reduce} spell, targeting yourself \
                   with the \"enlarge\" effect of the spell only, once as a bonus action \
                   without expending a spell slot or spell components. You do not need \
                   to concentrate on this spell. Once you cast this spell, you can’t \
                   cast it again in this way until you finish a long rest."
            ; "Trait: Colossal Strength"
              <~> [ "While bound to Growth, you grow an inch taller and your muscles \
                     have greater definition. You can wield heavy weapons without \
                     penalty, even if you are Small size. Additionally, you can use your \
                     Charisma, instead of your Strength modifier, for attacks and damage \
                     rolls with melee weapon attacks using heavy weapons."
                  ]
            ] )
        ; ( "Passion"
          , 3
          , [ "Inheritance of Flame"
              --> "While bound to Passion, you know the {@spell fire bolt} cantrip. \
                   Additionally, you can add your Charisma modifier to damage rolls you \
                   make with spells that deal fire damage."
            ; "Fire Spin"
              --> "As a bonus action, you can launch yourself in a spiral of flame. When \
                   you do so, you can make a melee spell attack against a creature \
                   within 5 feet of you, dealing fire damage equal to {@dice 1d4} + your \
                   Charisma modifier on a hit. You then move up to 10 feet in any \
                   direction without provoking opportunity attacks."
            ; ne
                "Spellcasting: Pyromancy"
                [ !"While bound to Passion, you can cast the following spells without \
                    using spell slots or spell components:"
                ; ne
                    "2/day each"
                    [ !(List.map [ "burning hands"; "scorching ray" ] ~f:(fun spell ->
                            [%string "{@spell %{spell}}"])
                       |> String.concat ~sep:", ")
                    ]
                ; ne
                    "1/day each"
                    [ !(List.map [ "fireball"; "heat metal" ] ~f:(fun spell ->
                            [%string "{@spell %{spell}}"])
                       |> String.concat ~sep:", ")
                    ]
                ; !"You regain all expended uses when you finish a long rest."
                ]
            ; "Trait: Inferno Within"
              <~> [ "While bound to Passion, your skin is hot to the touch, and \
                     flickering embers can be seen within your mouth, nostrils, and \
                     eyes. You have resistance to fire damage."
                  ]
            ] )
        ; ( "Flair"
          , 3
          , [ "Bonus Proficiencies"
              --> "While bound to Flair, you gain proficiency with hand {@item \
                   crossbow|phb|crossbows}, {@item rapier|phb|rapiers}, {@item \
                   scimitar|phb|scimitars}, {@item shortsword|phb|shortswords}, and \
                   {@item whip|phb|whips}."
            ; "Fighting Style: Dueling"
              --> "When you are wielding a melee weapon in one hand and no other \
                   weapons, you gain a +2 bonus to damage rolls with that weapon."
            ; "Extra Attack"
              --> "You can attack twice, instead of once, whenever you take the attack \
                   action on your turn."
            ; "Trait: After Image"
              <~> [ "While bound to Flair, you move with an unearthly speed that leaves \
                     a lingering trail behind you. You can use your Charisma, instead of \
                     your Strength or Dexterity modifier, for attacks and damage rolls \
                     with finesse weapons."
                  ; "Additionally, as a bonus action, you can move 15 feet in a flash, \
                     without provoking opportunity attacks. You can use this ability a \
                     number of times equal to your Charisma modifier and regain all \
                     expended uses when you finish a long rest."
                  ]
            ] )
        ; ( "Permanence"
          , 3
          , [ "Bonus Proficiencies"
              --> "While bound to Permanence, you gain proficiency in heavy armor, as \
                   well as with {@item flail|phb|flails} and {@item \
                   morningstar|phb|morningstars}."
            ; "Mortal Bargain"
              --> "While bound to Permanence, whenever you drop to 0 hit points, but are \
                   not killed outright, you remain conscious and do not begin making \
                   death saving throws until the end of your next turn. If you take any \
                   damage while at 0 hit points, you instantly fall unconscious and \
                   suffer one death saving throw failure."
            ; "Absolute Resilience"
              <-> [ "Whenever you take bludgeoning, piercing, or slashing damage while \
                     wearing heavy armor, you can use your reaction you reduce the \
                     damage taken by {@dice 1d12}. You can further reduce the damage by \
                     an additional {@dice 1d12} for each vestige other than Permanence \
                     you have bound."
                  ; "Once you use this ability, you can’t use it again until you finish \
                     a short or long rest."
                  ]
            ; "Trait: Shining Armor"
              <~> [ "While bound to Permanence, you use your action to summon shining \
                     metal armor around you, along with any melee weapon with which you \
                     have proficiency. The armor is full plate which cannot be \
                     destroyed, which you can wear without penalty, regardless of your \
                     Strength score. This equipment vanishes when you dismiss them as an \
                     action, or when you are no longer binding Permanence."
                  ; "The armor is blinding and difficult gaze upon. If you make a \
                     Charisma (Intimidation) check while wearing this armor, you can \
                     treat the result as 10, or your binder level plus your Charisma \
                     modifier, whichever is higher."
                  ]
            ] )
        ; ( "Wrath"
          , 4
          , [ "Evil Eye"
              <-> [ "As an action, choose one creature you can see that can see you \
                     within 60 feet to make a Wisdom saving throw. On a failed save, the \
                     creature is frightened of you until the end of your next turn."
                  ; "You can extend the duration that a creature is frightened of you by \
                     using your bonus action to cackle loudly. When you do so, this \
                     effect requires concentration, as a spell, but you can concentrate \
                     on these abilities and a spell at the same time. You make only one \
                     saving throw to maintain concentration on both."
                  ]
            ; "Waking Nightmare"
              --> "Once on each of your turns, when a creature fails an Intelligence, \
                   Wisdom, or Charisma saving throw against one of your spells or \
                   vestige features, you can cause that creature to take {@dice 2d8} \
                   psychic damage."
            ; ne
                "Spellcasting: Spellbind"
                [ !"While bound to Wrath, you can cast the following spells without \
                    using spell slots or spell components:"
                ; ne
                    "2/day each"
                    [ !(List.map [ "bane"; "darkness" ] ~f:(fun spell ->
                            [%string "{@spell %{spell}}"])
                       |> String.concat ~sep:", ")
                    ]
                ; ne
                    "1/day each"
                    [ !(List.map [ "fear"; "phantasmal killer" ] ~f:(fun spell ->
                            [%string "{@spell %{spell}}"])
                       |> String.concat ~sep:", ")
                    ]
                ; !"You regain all expended uses when you finish a long rest."
                ]
            ; "Trait: After Image"
              <~> [ "While bound to Wrath, your legs transform into  cloven hooves, a \
                     mark of anger's enduring curse. By leveraging this curse, you can \
                     use your action to cause one creature that you can see within 60 \
                     feet that is frightened of you to flee. The creature must use its \
                     reaction to move away from you by the safest available route, \
                     unless there is nowhere to move. The creature can then repeat its \
                     saving throw against the spell or effect which caused it to be \
                     frightened, ending the effect on itself on a success."
                  ]
            ] )
        ; ( "Despair"
          , 4
          , [ "Martyr's Path"
              --> "While bound to Despair, your hit point maximum increases by your \
                   binder level plus your Charisma modifier."
            ; "Blood Sacrifice"
              --> "Once on each of your turns, when you hit a creature with a melee \
                   weapon attack, you can spill your own boiling blood to deal \
                   additional damage to the target. When you do so, choose a number of \
                   d8s up to your Charisma modifier of additional radiant damage to add \
                   to the damage roll. You take 3 damage for each additional die added \
                   to the roll."
            ; "Mercy"
              --> "You can use your bonus action to regain hit points equal to your \
                   binder level. Once you use this ability, you can’t use it again until \
                   you finish a short or long rest."
            ; "Trait: Thorny Flesh"
              <~> [ "While bound to Despair, your flesh toughens and sprouts long, sharp \
                     thorns. Whenever a creature within 5 feet of you hits you with a \
                     melee weapon attack, it takes piercing damage equal to your \
                     Charisma modifier."
                  ]
            ] )
        ; ( "Fortune"
          , 4
          , [ "Bonus Proficiencies"
              --> "While bound to Fortune, you gain proficiency with all gaming sets. \
                   Additionally, you can reroll any ability check you make to play \
                   mundane games of skill."
            ; "Lucky Hit"
              --> "Once on each of your turns, when you deal damage, you can reroll a \
                   damage die and must keep the number rolled."
            ; "Stolen Luck"
              --> "When you bind Fortune, roll a d20 and record the number rolled. This \
                   is your stolen luck roll. Once per turn, you can trade your stolen \
                   luck roll with any attack roll, ability check, or saving throw made \
                   by you or a creature that you can see. Your stolen luck roll becomes \
                   the number rolled for the attack roll, ability check, or saving \
                   throw, and your new stolen luck roll becomes the number that was been \
                   rolled on the d20. You can use this ability three times, and regain \
                   all expended uses when you finish a long rest. "
            ; "Trait: Third Chances"
              <~> [ "While bound to Fortune, three small sigils (associated with a game \
                     of chance you are familiar with) float in front of your forehead. \
                     You can extinguish one of these to add {@dice 1d8} to an attack \
                     roll, ability check, or saving throw. However, when you extinguish \
                     all three, you subtract 2 from all subsequent attack rolls, ability \
                     checks, and saving throws you make while bound to Fortune."
                  ]
            ] )
        ; ( "Calm"
          , 5
          , [ "Inheritance of Frost"
              --> "While bound to Calm, you know the {@spell ray of frost} cantrip. \
                   Additionally, you can add your Charisma modifier to damage rolls you \
                   make with spells that deal cold damage."
            ; "Crystalline Arcana"
              --> "When you cast a spell that affects an area and requires your \
                   concentration, you can choose a number of Medium or smaller creatures \
                   equal to your Charisma modifier to be protected from its effects. A \
                   5-foot cube gap in the spell effects opens around each chosen \
                   creature. These creatures do not need to make saving throws against \
                   the spell. Additionally, for the spell’s duration, these creatures \
                   are immune to the effects of the spell within its area and ignore \
                   conditions, such as difficult terrain, created by the spell."
            ; ne
                "Spellcasting: Cryomancy"
                [ !"While bound to Calm, you can cast the following spells without using \
                    spell slots or spell components:"
                ; ne
                    "2/day each"
                    [ !(List.map [ "sleet storm" ] ~f:(fun spell ->
                            [%string "{@spell %{spell}}"])
                       |> String.concat ~sep:", ")
                    ]
                ; ne
                    "1/day each"
                    [ !(List.map [ "ice storm"; "cone of cold" ] ~f:(fun spell ->
                            [%string "{@spell %{spell}}"])
                       |> String.concat ~sep:", ")
                    ]
                ; !"You regain all expended uses when you finish a long rest."
                ]
            ; "Trait: Rime"
              <~> [ "While bound to Calm, your skin, as well as your clothing, weapons, \
                     and armor, are covered with a thick frost, and your breath is \
                     visible, as if on a cold night. Whenever you begin concentrating on \
                     a spell, this frost grows into large ice crystals on your skin. As \
                     long as you maintain concentration, you can subtract the spell’s \
                     level from the damage taken. Fire damage ignores this ability."
                  ]
            ] )
        ; ( "Movement"
          , 5
          , [ "Loose Gravitation"
              --> "While bound to Movement, the distance of your long jump and height of \
                   your high jump are doubled, and you take half damage from falling."
            ; "Fragmentation"
              --> "Whenever you cast a spell which teleports you, you can choose to \
                   teleport into a space occupied by a creature. When you do so, the \
                   creature takes {@dice 1d4} force damage for every 10 feet you \
                   teleported, up to a maximum of {@dice 5d4}, and, if you and the \
                   creature are within two size categories of one another, it moves into \
                   an adjacent unoccupied space of its choice."
            ; ne
                "Spellcasting: Blink"
                [ !"While bound to Movement, you can cast the following spells without \
                    using spell slots or spell components:"
                ; ne
                    "at will"
                    [ !(List.map [ "misty step" ] ~f:(fun spell ->
                            [%string "{@spell %{spell}}"])
                       |> String.concat ~sep:", ")
                    ]
                ; ne
                    "2/day each"
                    [ !(List.map [ "dimension door" ] ~f:(fun spell ->
                            [%string "{@spell %{spell}}"])
                       |> String.concat ~sep:", ")
                    ]
                ; ne
                    "1/day each"
                    [ !(List.map [ "blink"; "teleportation circle" ] ~f:(fun spell ->
                            [%string "{@spell %{spell}}"])
                       |> String.concat ~sep:", ")
                    ]
                ; !"You regain all expended uses when you finish a long rest."
                ]
            ; "Trait: Realmatic Error"
              <~> [ "While bound to Movement, your body becomes wedged between reality \
                     and nonexistence. Your joints, including your neck, shoulders, \
                     elbows, hips, and knees, seem to have vanished into some other \
                     plane, leaving your other body parts loosely floating in their \
                     positions. As a result, whenever you are hit with an attack, roll a \
                     d20. On a 20, the attack misses."
                  ]
            ] )
        ; ( "Transformation"
          , 5
          , [ "Snap Reflexes"
              --> "You can make an opportunity attack without using a reaction. You have \
                   advantage on this attack roll. Once you use this ability, you can’t \
                   use it again until you finish a short or long rest."
            ; "Trait: Spider-Climber"
              <~> [ "When you bind Transformation, your skin becomes pale and strangely \
                     adhesive. You can move up, down, and across vertical surfaces and \
                     upside down along ceilings, while leaving your hands free. You also \
                     gain a climbing speed equal to your walking speed."
                  ]
            ; "Trait: Abominable Limbs"
              <~> [ "When you bind Transformation, your hands sprout sickening claws, \
                     which you can use the make unarmed strikes. The claws deal {@dice \
                     1d4} slashing damage, and you can use Charisma, instead of \
                     Strength, for their attack and damage rolls. The claws count as \
                     magical for the purpose of overcoming resistance and immunity."
                  ; "When you take the Attack action, you can make one unarmed strike \
                     with the claws as a bonus action. Additionally, when use your claws \
                     to hit a creature that has already been hit with them during that \
                     turn, you deal an additional 2d6 slashing damage. "
                  ]
            ; "Trait: Extraneous Joint"
              <~> [ "When you bind Transformation, your arms and legs deform, \
                     lengthening and cracking until they each contain an additional \
                     joint. The reach of all of your melee attacks, as well as your \
                     reach for opportunity attacks, extends out to 10 feet. \
                     Additionally, you can make an opportunity attack against any \
                     creature that moves while within your reach."
                  ]
            ] )
        ; ( "Death"
          , 6
          , [ "Grave Empathy"
              --> "The undead can innately sense your closeness to their kind. Whenever \
                   an undead tries to attack you, it must make a Wisdom saving throw. On \
                   a failed save, its attack misses. On a successful save, the undead is \
                   immune to this ability for the next 24 hours."
            ; "Corpse Shepherd"
              --> "You can perform a 10-minute ritual to summon a Medium humanoid corpse \
                   or pile of bones (your choice), which is magically teleported to your \
                   location from a random cemetery. "
            ; ne
                "Spellcasting: Dead Alive"
                [ !"While bound to Death, you can cast the following spells without \
                    using spell slots or spell components:"
                ; ne
                    "at will"
                    [ !(List.map [ "false life" ] ~f:(fun spell ->
                            [%string "{@spell %{spell}} (self only)"])
                       |> String.concat ~sep:", ")
                    ]
                ; ne
                    "3/day each"
                    [ !(List.map [ "animate dead" ] ~f:(fun spell ->
                            [%string "{@spell %{spell}}"])
                       |> String.concat ~sep:", ")
                    ]
                ; ne
                    "1/day each"
                    [ !(List.map [ "create undead" ] ~f:(fun spell ->
                            [%string "{@spell %{spell}}"])
                       |> String.concat ~sep:", ")
                    ]
                ; !"You regain all expended uses when you finish a long rest."
                ]
            ; "Trait: Venerable"
              <~> [ "While bound to Death, you appear dramatically aged. You voice \
                     descends into a hoarse rasp, your hair grows white, deep wrinkles \
                     appear in your skin. Additionally, you can siphon the life and \
                     youth from others. As an action, make a melee spell attack against \
                     a hostile creature within your reach. On a hit, the target takes \
                     {@dice 4d6} necrotic damage and you regain hit points equal to half \
                     the necrotic damage dealt."
                  ]
            ] )
        ; ( "Control"
          , 6
          , [ "No Strings On Me"
              --> "While bound to Control, you are immune to being charmed or possessed."
            ; "Puppeteering"
              --> "Whenever you cast the spell {@spell dominate beast}, {@spell dominate \
                   person}, or {@spell dominate monster}, you can take total and precise \
                   control of the target as a bonus action, rather than an action. \
                   Additionally, you can concentrate on two of these spells at once, \
                   taking control of both targets using one bonus action, and making \
                   only one saving throw to maintain concentration on both spells."
            ; ne
                "Spellcasting: Soul Strings"
                [ !"While bound to Control, you can cast the following spells without \
                    using spell slots or spell components:"
                ; ne
                    "at will"
                    [ !(List.map [ "command" ] ~f:(fun spell ->
                            [%string "{@spell %{spell}}"])
                       |> String.concat ~sep:", ")
                    ]
                ; ne
                    "2/day each"
                    [ !(List.map [ "dominate beast"; "dominate person" ] ~f:(fun spell ->
                            [%string "{@spell %{spell}}"])
                       |> String.concat ~sep:", ")
                    ]
                ; ne
                    "1/day each"
                    [ !(List.map
                          [ "compulsion"; "otto's irresistible dance" ]
                          ~f:(fun spell -> [%string "{@spell %{spell}}"])
                       |> String.concat ~sep:", ")
                    ]
                ; !"You regain all expended uses when you finish a long rest."
                ]
            ; "Trait: Dummy"
              <~> [ "While bound to Control, your skin appears wooden and lacquered and \
                     your joints seem to be wooden hinges. While you remain motionless, \
                     you are indistinguishable from a puppet. Moreover, you can throw \
                     your voice, causing it to originate from any point you choose \
                     within 60 feet."
                  ]
            ] )
        ; ( "Ferocity"
          , 6
          , [ "Bonus Proficiencies"
              --> "While bound to Ferocity, you gain proficiency with {@item \
                   battleaxe|phb|battleaxes}, {@item greataxe|phb|greataxes}, {@item \
                   maul|phb|mauls}, and {@item warhammer|phb|warhammers}."
            ; "Extra Attack"
              --> "You can attack twice, instead of once, whenever you take the Attack \
                   action on your turn."
            ; ne
                "Fury"
                [ !"On your turn, you can use your bonus action to summon up Ferocity's \
                    bottomless rage. For the next minute, you gain the following \
                    benefits:"
                ; lh
                    [ !!"You can add your Charisma modifier to Strength checks and \
                         Strength saving throws."
                    ; !!"You have advantage on all melee weapon attacks which use heavy \
                         weapons, versatile weapons, or unarmed strikes. However, melee \
                         weapon attacks against you are rolled with advantage."
                    ; !!"You have resistance to bludgeoning, piercing, and slashing \
                         damage."
                    ; !!"When you reduce a hostile creature to 0 hit points with a melee \
                         weapon attack, you can move up to 10 feet and make an \
                         additional weapon attack."
                    ]
                ; !"You can end this effect early as a bonus action. Once you use this \
                    ability, you can’t use it again until you take a short or long rest."
                ]
            ; "Trait: Bloodthirst"
              <~> [ "While bound to Ferocity, you assume the savage guise and violent \
                     aspect of a lycanthrope: coarse hair covers your body, your nose \
                     lengthens, your fingernails lengthen into claws, and your teeth \
                     sharpen. When you take damage from a creature that is within 5 feet \
                     of you, you can use your reaction to make a melee weapon attack \
                     against that creature."
                  ]
            ] )
        ; ( "Recompense"
          , 7
          , [ "Enduring Punishment"
              --> "Whenever you deal damage to a creature, the creature can’t regain hit \
                   points until the start of your next turn."
            ; "Eye for an Eye"
              --> "When a creature you can see hits you with a melee weapon attack, you \
                   have advantage on the first attack you make against it on your next \
                   turn."
            ; "Forbid"
              --> "If a hostile creature you can see uses its action to attack or takes \
                   a legendary action or a lair action, you can use your reaction to \
                   intercede, preventing the action from happening. Once you use this \
                   ability, you can’t use it again until you finish a long rest."
            ; "Trait: Maimed"
              <~> [ "While bound to Recompense, you are missing multiple fingers, \
                     several teeth, and an eye—all gouged out as recompense for past \
                     crimes. You are immune to the following conditions: {@condition \
                     blinded}, {@condition deafened}, {@condition exhaustion}, \
                     {@condition paralyzed}, {@condition poisoned}, and {@condition \
                     stunned}. If a creature attempts to impose one of these conditions \
                     on you, you can use your reaction to instead impose the same \
                     condition upon the creature."
                  ]
            ] )
        ; ( "Piety"
          , 7
          , [ "Halo"
              --> "Whenever you fail a saving throw, roll a d6. On a 6, you succeed the \
                   saving throw instead."
            ; "Flyby Attack"
              --> "When you make a melee attack against a creature, while flying you \
                   don’t provoke opportunity attacks from that creature for the rest of \
                   the turn, whether you hit or not."
            ; "Judgement"
              --> "When you hit a creature with a melee weapon attack, you can deal an \
                   additional {@dice 6d8} necrotic or radiant damage (your choice) to \
                   the target. Once you use this ability, you can’t use it again until \
                   you finish a short or long rest."
            ; "Trait: Angelic Wings"
              <~> [ "While bound to Piety, you sprout broad, feathered wings from your \
                     back, granting you a flying speed of 60 feet."
                  ]
            ] )
        ; ( "Mediocrity"
          , 8
          , [ "Inquisition"
              --> "You can cast the spell {@spell magic weapon} at will as a 4th-level \
                   spell without using spell slots or spell components. Your \
                   concentration on the spell breaks if the weapon ever leaves your \
                   hand."
            ; ne
                "Spellcasting: Mage-Killer"
                [ !"While bound to Death, you can cast the following spells without \
                    using spell slots or spell components:"
                ; ne
                    "3/day each"
                    [ !(List.map [ "counterspell"; "dispel magic" ] ~f:(fun spell ->
                            [%string "{@spell %{spell}}"])
                       |> String.concat ~sep:", ")
                    ]
                ; ne
                    "1/day each"
                    [ !(List.map [ "antimagic field"; "true seeing" ] ~f:(fun spell ->
                            [%string "{@spell %{spell}}"])
                       |> String.concat ~sep:", ")
                    ]
                ; !"Casting antimagic field in this way does not disable any of your \
                    vestige features, except for spellcasting and magic items. You \
                    regain all expended uses when you finish a long rest."
                ]
            ; "Trait: Deadeye"
              <~> [ "While bound to Mediocrity, your eyes become vacant pits with two \
                     bright coals burning within them, granting you vision that can \
                     pierce arcana. You are constantly under the effects of the spell \
                     {@spell detect magic}, which does not require your concentration. \
                     Additionally, you can use your action to focus on a creature you \
                     can see within 30 feet. You can determine if that creature has cast \
                     a spell within the last 24 hours, and the spell’s school of magic, \
                     if any."
                  ]
            ] )
        ; ( "Legend"
          , 8
          , [ "Minor Relic"
              <-> [ "When you bind Legend, a magic item appears in your possession. The \
                     item vanishes when you are no longer bound to Legend."
                  ; "The item is your choice of the following: 2 {@item bead of \
                     force|dmg|beads of force}, a {@item necklace of fireballs} (2 \
                     beads), an {@item oil of etherealness}, a {@item potion of gaseous \
                     form}, or a {@item potion of invisibility}."
                  ]
            ; "Major Relic"
              <-> [ "When you bind Legend, a magic item appears in your possession. You \
                     are automatically attuned to it, if it requires attunement, and it \
                     does not count against the number of items you can have attuned. \
                     Moreover, no other creature can attune to the item or, if the item \
                     is a weapon, use it make an attack. The item vanishes when you are \
                     no longer bound to Legend."
                  ; "The item is your choice of the following: a {@item carpet of \
                     flying}, a {@item cloak of the bat}, a {@item flame tongue}, {@item \
                     gauntlets of ogre power}, an {@item daern's instant \
                     fortress|dmg|instant fortress}, a {@item ring of regeneration}, a \
                     {@item ring of telekinesis}, a {@item sun blade}, or a {@item wand \
                     of wonder}."
                  ]
            ; "Trait: Spectacles"
              <~> [ "As a boon, Legend offers its binders magic spectacles. While \
                     wearing the spectacles, if you make an Intelligence (Arcana), \
                     Intelligence (History), Intelligence (Nature), or Intelligence \
                     (Religion) check, you can treat the result as a 10, or your binder \
                     level plus your Charisma modifier, whichever is higher"
                  ]
            ] )
        ; ( "Destruction"
          , 9
          , [ "Obliviate"
              --> "At your touch, you unmake. As an action, you can touch an object or \
                   creature, which must make a Constitution saving throw. On a failure, \
                   the target takes {@dice 10d10 + 50} necrotic damage, or half as much \
                   on a successful save. If this damage reduces the target to 0 hit \
                   points, it is totally unmade. An unmade creature and everything it is \
                   wearing or carrying, except for magic items, is completely \
                   annihilated, leaving behind nothing, not even dust. The creature can \
                   be restored to life only by means of a true resurrection or a wish \
                   spell. You can use this ability once, and regain the ability to do so \
                   when you finish a long rest."
            ; "Trait: Vestigial"
              <~> [ "While bound to Destruction, you are divorced from reality, much \
                     like vestiges themselves, causing you to appear hazy and \
                     indistinct, as your form is stretched between the realms. You have \
                     resistance to all damage. Additionally, you can move through other \
                     creatures and objects as if they were difficult terrain. You take \
                     {@dice 4d10} force damage if you end your turn inside a creature or \
                     object, as you are ejected into the nearest unoccupied space."
                  ]
            ] )
        ; ( "Desire"
          , 9
          , [ "Wish"
              --> "While bound to Desire, you can cast the spell {@spell wish}, without \
                   expending spell slots or spell components. Once you cast this spell, \
                   Desire is expelled and is replaced by a vestige of your choice of 3rd \
                   level or lower. You can’t rebind Desire until you take a long rest.  \
                   If you lose the ability to wish, you also lose the ability to bind \
                   Desire. "
            ; "Trait: Vestigial"
              <~> [ "When you bind to Desire, your skin tints to a blueish hue, and you \
                     constantly float a few inches off the ground on a layer of smoke. \
                     You ignore the effects of difficult terrain, your speed increases \
                     by 10 feet, and you can walk across fluid surfaces, such as water \
                     and quicksand."
                  ]
            ] )
        ]
  ;;

  let subclass_vestiges =
    List.map
      ~f:(fun (name, level, prereq, entries) ->
        let prereq =
          ( prereq
          , let level =
              match level with
              | 1 -> "1st-level"
              | 2 -> "2nd-level"
              | 3 -> "3rd-level"
              | x -> [%string "%{x#Int}th-level"]
            in
            [%string "can bind %{level} vestiges"] )
        in
        let level =
          match level with
          | 2 -> Some 3
          | 3 -> Some 5
          | 4 -> Some 7
          | 5 -> Some 9
          | 6 -> Some 11
          | 7 -> Some 13
          | 8 -> Some 15
          | 9 -> Some 17
          | _ -> None
        in
        create ?level ~prereq ~name ~type_:"Vestige" ~entries ())
      Entry.
        [ ( "Envy"
          , 1
          , "Doctrine: Envy's Faithful"
          , [ "Embrace"
              --> "While bound to Envy, you have advantage on opportunity attacks."
            ; "Envy's Mark"
              --> "As a bonus action, you can force a creature within 30 feet to make a \
                   Wisdom saving throw. A creature immune to being charmed automatically \
                   succeeds on this saving throw. On a failed save, a special mark \
                   representing Envy's favor appears on the creature for up to 1 minute. \
                   While the creature is marked, it can’t willingly move more than 30 \
                   feet away from you. Additionally, you can use your bonus action on \
                   subsequent turns to magically pull the marked creature up to 10 feet \
                   closer to you. You can only mark one creature with this ability at a \
                   time."
            ; "Enamour"
              --> "You can pry open another creature’s heart and force Envy into it. As \
                   an action, choose a humanoid creature you have marked to be charmed \
                   by you for up to 10 minutes. While charmed, the creature regards you \
                   the object of its true desire. When this effect ends, the creature \
                   knows it was charmed by you. This effect ends early if you or your \
                   companions deal any damage to the creature. Once you use this \
                   ability, you can’t use it again until you finish a short or long \
                   rest."
            ; "Trait: Arms of Embrace"
              <~> [ "While bound to Envy, you grow a second set of arms belonging to \
                     Envy, which grasp you in an embrace. These arms are fully \
                     functional and can be used to hold weapons and shields (allowing \
                     you to hold 2 two-handed weapons, or 4 one-handed weapons), perform \
                     somatic components of spells, and perform other actions. Using \
                     these arms, once per round, you can make an opportunity attack \
                     against a creature you have marked without using a reaction."
                  ]
            ] )
        ]
  ;;

  let vestiges = List.concat [ vestiges; subclass_vestiges ]
  let features = List.concat [ minor_fragments; vestiges ]
end

module Hedge_mage = struct
  let arcane_manuevers =
    List.map
      ~f:(fun (name, entries) -> create ~name ~type_:"Arcane Maneuver" ~entries ())
      Entry.
        [ ( "Blitz"
          , [ !"As a bonus action, when you hit a creature with an attack, you can \
                expend one battle die to maneuver one of your comrades into a more \
                advantageous position. Choose a friendly creature who can see or hear \
                you. That creature can use its reaction to move up to half its speed \
                without provoking opportunity attacks from the target of your attack."
            ] )
        ; ( "Bulwark"
          , [ !"When you hit a creature with a melee attack, you can expend a battle die \
                as a bonus action to brace yourself for its counterattack. The next time \
                the creature damages you before the start of your next turn, you can \
                roll the battle die and subtract the result from the damage dealt."
            ] )
        ; ( "Check"
          , [ !"When you hit a creature with an attack, as a bonus action you can spend \
                one battle die to force that creature to flee. The target must make a \
                Charisma saving throw. A creature that is immune to being charmed \
                automatically succeeds this saving throw. On a failed save, the target \
                must immediately use its reaction, if available, to move up to half its \
                speed directly away from you."
            ] )
        ; ( "Gambit"
          , [ !"When you hit a creature with an attack, you can expend one battle die to \
                give your allies an opening. The next creature other than you to make an \
                attack against the target adds the battle die to their attack roll."
            ] )
        ; ( "Rally"
          , [ !"As a bonus action on your turn, you can expend a battle die to choose \
                one allied creature within 60 feet of you who can see or hear you. That \
                creature regains hit points equal to the die rolled + your Intelligence \
                modifier. You cannot use this ability to heal a creature who has 0 hit \
                points."
            ] )
        ; ( "Stalemate"
          , [ !"When you hit a creature with an attack, as a bonus action you can expend \
                one battle die to hold that creature in place. Until the end of its next \
                turn, the target can’t willingly move unless it first takes the \
                Disengage action."
            ] )
        ]
    @ [ create
          ~name:"Checkmate"
          ~type_:"Arcane Maneuver"
          ~level:15
          ~entries:
            Entry.
              [ !"When you hit a creature with a weapon or spell attack, you can use \
                  your bonus action and expend a battle die to direct one of your \
                  companions to strike. When you do so, choose a friendly creature who \
                  can see or hear you that is within reach of the creature you hit. That \
                  creature can immediately use its reaction to make one weapon attack or \
                  cast a cantrip requiring an attack roll, adding the battle die to the \
                  attack’s damage roll."
              ]
          ()
      ]
  ;;

  let fighting_style =
    List.map
      ~f:(fun (name, entries) ->
        create ~name ~type_:"Hedge Mage Fighting Style" ~entries ())
      Entry.
        [ "Blaster", [ !"The spell save DC for your hedge mage cantrips increases by 1." ]
        ; ( "Deflector"
          , [ !"When you have one hand free and a creature hits you with a spell attack \
                or a ranged weapon attack, you can use your reaction to add your \
                proficiency bonus to your Armor Class, potentially causing the attack to \
                miss."
            ] )
        ; ( "Resistive"
          , [ !"While you are wearing light armor or are under the effects of the \
                {@spell mage armor} spell, you gain a +1 bonus to your Armor Class."
            ] )
        ; ( "Sniper"
          , [ !"When making a ranged spell attack, you gain a +1 bonus to the attack \
                roll. Additionally, your ranged cantrips ignore half cover."
            ] )
        ; ( "Striker"
          , [ !"When you hit with a cantrip requiring a melee attack and exceed the \
                target's AC by 5 or more or score a critical hit, you can add your \
                proficiency modifier to the damage roll."
            ] )
        ]
  ;;

  let tricks =
    List.map
      ~f:(fun (name, level, prereq, entries) ->
        create ~name ~level ~prereq:(prereq, prereq) ~type_:"Hedge Mage Trick" ~entries ())
      Entry.
        [ ( "Bishop's Maneuver"
          , 10
          , "House: Bishops"
          , [ !"You can take the {@action Disengage} action as a bonus action, and when \
                you do so, your movement speed increases by 10 feet until the end of \
                your turn."
            ] )
        ; ( "Castle"
          , 10
          , "House: Rooks"
          , [ !"As an action, you can choose a willing Small or Medium creature you can \
                see within 100 feet of you. You both teleport, switching places."
            ; !"Once you use this trick, you can’t use it again until you finish a short \
                or long rest."
            ] )
        ; ( "Directed Momentum"
          , 10
          , "House: Lancers"
          , [ !"Once on each of your turns, when you score a critical hit with a melee \
                attack or reduce a creature to 0 hit points with one, you can make an \
                unarmed strike against a second target. If the target is within range of \
                your Shock Trooper feature, you can lunge toward it. On a hit, this \
                attack deals an additional {@dice 1d8} force damage"
            ] )
        ; ( "Knight's Aegis"
          , 10
          , "House: Knights; force buckler cantrip"
          , [ !"When you cast the {@spell force buckler|ALT} cantrip, you can \
                concentrate on it for up to 1 minute. The spell does not end early if \
                you are hit by an attack."
            ] )
        ; ( "Lieutenant's Demand"
          , 10
          , "House: Kings"
          , [ !"You can cast the spell {@spell command} at will without using a spell \
                slot."
            ] )
        ; ( "Unerring Strike"
          , 10
          , "true strike cantrip"
          , [ !"When you cast the {@spell true strike} cantrip, you can concentrate on \
                it for a number of rounds equal to your Intelligence modifier. You gain \
                advantage on the first attack roll you make against the target each \
                round while maintaining concentration on true strike"
            ] )
        ; ( "Snake Eyes"
          , 10
          , "House: Dice"
          , [ !"If you roll a 1 or 2 on a Die of Fate, you keep the die instead of \
                giving it to the GM."
            ] )
        ]
    @ List.map
        ~f:(fun (name, prereq, entries) ->
          create ~name ~prereq:(prereq, prereq) ~type_:"Hedge Mage Trick" ~entries ())
        Entry.
          [ ( "Blinding Light"
            , "light cantrip"
            , [ !"When you use the {@spell light} cantrip to target an object you are \
                  holding, you can direct a flare at a creature within 10 feet of you, \
                  which must make a Constitution saving throw against your spell save \
                  DC. On a failed save, the creature is blinded until the beginning of \
                  your next turn. After a creature has failed a saving throw against \
                  this ability, it has advantage on all Constitution saving throws \
                  against it for 24 hours."
              ] )
          ; ( "Chivalrous Presence"
            , "House: Knights"
            , [ !"You can add half your proficiency bonus (rounded up) to any Charisma \
                  check you make that doesn’t already use your proficiency bonus. \
                  Additionally, you can tell when a creature you are speaking with tells \
                  a lie, as long as you haven’t told a lie to that creature."
              ] )
          ; ( "Cloak of Shadow"
            , "House: Rooks"
            , [ !"While not wearing armor, under the effects of {@spell mage armor}, or \
                  using a shield, your AC equals 10 + your Dexterity modifier + your \
                  Intelligence modifier."
              ] )
          ; ( "Commander's Steed"
            , "House: Kings"
            , [ !"You learn the {@spell find steed} spell and can cast it without using \
                  a spell slot. Your steed is more resilient than most, and has a number \
                  of additional hit points equal to your hedge mage level."
              ] )
          ; ( "Encryptogram"
            , "cryptogram cantrip"
            , [ !"Your knowledge of ciphers has improved your magically clandestine \
                  communications. When you cast the {@spell cryptogram|ALT} cantrip, its \
                  limit is 20 characters, instead of 8, and only the specified recipient \
                  can read the message."
              ] )
          ; ( "Field Magic"
            , "House: Bishops"
            , [ !"You learn the cantrip {@spell spare the dying}, which does not count \
                  against your maximum number of cantrips known. Additionally, when you \
                  cast spare the dying on a creature which has 0 hit points, the target \
                  regains 1 hit point and gains temporary hit points equal to your \
                  level, which last for 1 minute. Once a creature regains hit points due \
                  to this ability, it can’t do so again until it finishes a long rest. "
              ] )
          ; ( "Gamble"
            , "House: Dice; cheat cantrip"
            , [ !"You are always under the effects of the {@spell cheat|ALT} cantrip."
              ; !"Additionally, you can reroll an attack roll, ability check, or saving \
                  throw. Once you use this ability, you can’t use it again until you \
                  finish a short or long rest."
              ] )
          ; ( "Infinite Variation"
            , "prestidigitation cantrip"
            , [ !"You have become exceptionally skilled at using the {@spell \
                  prestidigitation} cantrip to mimic other spells. When you cast \
                  prestidigitation, you can use it to emulate the effects of any other \
                  cantrip that does not deal damage, even one that is not on the hedge \
                  mage spell list. To do so, you must succeed on a DC 15 Intelligence \
                  (Arcana) check, otherwise the spell fizzles and does nothing. A \
                  cantrip cast using this trick counts as a hedge mage cantrip and uses \
                  your Intelligence modifier as the spellcasting modifier."
              ] )
          ; ( "Leading Edge Tactics"
            , "House: Lancers"
            , [ !"You always have a plan when engaging the enemy. As such, attacks \
                  during the first round of combat have disadvantage against you."
              ] )
          ; ( "Skilled Mage Hand"
            , "mage hand cantrip"
            , [ !"Your skill with the {@spell mage hand} cantrip allows you to use it as \
                  an extension of yourself. When you cast the spell and as a bonus \
                  action on each of your subsequent turns, you can use one of the \
                  following effects with the hand:"
              ; "Press"
                --> "The hand pushes against a Large or smaller creature within 5 feet \
                     of it. Choose a direction away from that creature. Every foot of \
                     movement in that direction while the hand is pressing against it \
                     costs the creature two feet of movement. The hand continues to push \
                     the target until the spell ends or you use your bonus action to use \
                     a different effect using the hand."
              ; "Punch"
                --> "The hand strikes one creature or object within 5 feet of it. Make a \
                     melee spell attack for the hand using your spell attack bonus. On a \
                     hit, the target takes {@dice 1d6} force damage."
              ; "Seize"
                --> "The hand grabs a creature of Tiny size and attempts to grapple it. \
                     The creature must succeed on a Strength ({@skill Athletics}) or \
                     Dexterity ({@skill Acrobatics}) check against your spell save DC or \
                     be grappled by the hand. The hand continues to grapple the target \
                     until the target uses an action to escape on its turn, the spell \
                     ends, or you use your bonus action to use a different effect using \
                     the hand. "
              ] )
          ; ( "Shadowy Minor Illusion"
            , "minor illusion cantrip"
            , [ !"When you create the image of an object in an unoccupied space using \
                  the {@spell minor illusion} cantrip, you can fill it with raw aspect, \
                  causing it to become partially real. No matter what form the semi-real \
                  object takes, it still must be no larger than a 5-foot cube. It has AC \
                  10 and 5 HP, and it weighs 5 pounds. You can only have one semi-real \
                  illusion at a time. While this semi-real object exists, the cantrip \
                  requires your concentration."
              ; !"The illusion can’t replicate a creature, but it can deal damage to a \
                  creature within its 5-foot cube. If the illusion is of an object that \
                  can deal damage, a creature that enters the object’s 5-foot cube or \
                  begins its turn there must make an Intelligence saving throw against \
                  your spell save DC. On a failed save, the creature takes {@dice 1d6} \
                  damage of a type appropriate to the illusion. This damage can’t \
                  trigger Empowered Magic or any hedge mage tricks."
              ; "At Higher Levels"
                --> "This damage increases by 1d6 when you reach 5th level ({@dice \
                     2d6}), 11th level ({@dice 3d6}), and 17th level ({@dice 4d6})."
              ] )
          ; ( "Mystical Athlete"
            , "quickstep or springheel cantrip"
            , [ !"When you cast the {@spell quickstep|ALT} cantrip, your speed increases \
                  by 20 feet instead of 10 feet. When you cast the {@spell \
                  springheel|ALT} cantrip, your jumping distance increases by 20 feet \
                  instead of 10 feet. If you know both of these cantrips, you can cast \
                  both of them as part of the same bonus action."
              ] )
          ; ( "Mystical Weaponsmith"
            , "force weapon or magic daggers cantrip"
            , [ !"Once on each of your turns when you roll a 1 on the d20 for an attack \
                  roll for the {@spell force weapon|ALT} or {@spell magic daggers|ALT} \
                  cantrips, you can reroll the die and must use the new roll."
              ] )
          ; ( "Phantom Hookshot"
            , "phantom grapnel cantrip"
            , [ !"You can cast the {@spell phantom grapnel|ALT} cantrip as a bonus \
                  action. If you do so, its range is reduced to 15 feet. Additionally, \
                  creatures pulled by phantom grapnel are pulled an additional 10 feet."
              ] )
          ; ( "Rapid Fortification"
            , "mending cantrip"
            , [ !"You can cast the {@spell mending} cantrip as a bonus action, or you \
                  can cast it as an action for one of the following effects:"
              ; lh
                  [ !!"You can restore a single object, such as a door, cart, wall, or \
                       window to pristine condition, if at least half of its parts are \
                       present. This object can be no larger than 10 cubic feet, or 1 \
                       cubic foot if it is an exceptionally complex object (such as a \
                       clock)."
                  ; !!"You can create simple fortifications, such as sealing a door \
                       shut, adding wooden planks to a window, or building a short stone \
                       wall (no larger than 10 cubic feet). You must have the materials \
                       present to use this ability."
                  ]
              ] )
          ]
    @ List.map
        ~f:(fun (name, level, entries) ->
          create ~name ~level ~type_:"Hedge Mage Trick" ~entries ())
        Entry.
          [ ( "Signature Focus"
            , 5
            , [ !"When you finish a long rest, you can place a unique sigil on a simple \
                  weapon, which becomes your signature focus until you use this ability \
                  again. This weapon becomes magical, and can be used as an arcane \
                  focus. Your signature focus is bonded to you, and gains a number of \
                  special abilities:"
              ; lh
                  [ !!"As a bonus action, you can call your signature focus to your \
                       hand, as long as you are on the same plane as it."
                  ; !!"You can use your Intelligence modifier, instead of your Strength \
                       or Dexterity modifier, with attack rolls using your signature \
                       focus."
                  ; !!"Your signature focus gains a number of charges equal to your \
                       Intelligence modifier. When you damage a creature with it or a \
                       cantrip cast through it, you can expend one charge to deal an \
                       additional {@dice 1d8} force damage to that creature. Your focus \
                       regains all spent charges after you finish a long rest."
                  ]
              ] )
          ; ( "Split Fire"
            , 5
            , [ !"When you cast a hedge mage cantrip that requires a single spell attack \
                  roll, you can select multiple creatures and make a spell attack roll \
                  against each. You can target a number of creatures equal to the number \
                  of damage dice the cantrip deals, and split your damage dice up \
                  amongst your targets, to a minimum of 1 die of damage per target. Each \
                  attack must target a different creature."
              ; !"For example, fire bolt deals 3d10 damage. You can choose to target \
                  three creatures and deal 1d10 damage to each creature, or you can \
                  target two creatures, dealing 1d10 damage to one creature and 2d10 \
                  damage to the other creature, or you can target one creature for 3d10 \
                  damage."
              ] )
          ]
    @ List.map
        ~f:(fun (name, entries) -> create ~name ~type_:"Hedge Mage Trick" ~entries ())
        Entry.
          [ ( "Blasting Cantrip"
            , [ !"Once on each of your turns, when you deal force damage to a creature \
                  with a hedge mage cantrip, you can push the creature up to 10 feet \
                  away from you in a straight line."
              ] )
          ; ( "Corrosive Cantrip"
            , [ !"Once on each of your turns, when you deal acid damage to a creature \
                  with a hedge mage cantrip, you can cause the acid to erode the \
                  target’s defenses. The next time a creature makes an attack roll \
                  against the target before the beginning of your next turn, roll a d4 \
                  and subtract it from the target’s Armor Class for this attack."
              ] )
          ; ( "Draining Cantrip"
            , [ !"Whenever you deal necrotic or poison damage to a hostile creature \
                  using a hedge mage cantrip, you can siphon some of its life force. You \
                  gain temporary hit points equal to half your hedge mage level, which \
                  last for 1 minute. "
              ] )
          ; ( "Explosive Cantrip"
            , [ !"Once on each of your turns, when you deal fire damage to a creature \
                  with a hedge mage cantrip, each creature within 5 feet of the target, \
                  except yourself and the target, must succeed a Dexterity saving throw \
                  against your spell save DC or take half the fire damage dealt.  "
              ] )
          ; "Extended Range", [ !"The range of your hedge mage cantrips is doubled." ]
          ; ( "Flexible Range"
            , [ !"Being within 5 feet of a hostile creature doesn’t impose disadvantage \
                  on your ranged spell attack rolls. Additionally, the range of any \
                  cantrip you cast that requires a melee spell attack increases to 10 \
                  feet."
              ] )
          ; ( "Icy Cantrip"
            , [ !"Once on each of your turns, when you deal cold damage to a creature \
                  with a hedge mage cantrip, you can numb the target with a frigid \
                  blast. The first time the target makes an attack roll before the end \
                  of its next turn, it must roll a d4 and subtract it from the roll."
              ] )
          ; ( "Mystical Armor"
            , [ !"You can cast the spell {@spell mage armor} on yourself at will, \
                  without expending a spell slot."
              ] )
          ; ( "Mystical Vision"
            , [ !"You can cast the spell {@spell detect magic} at will without expending \
                  a spell slot."
              ] )
          ; ( "Severe Cantrip"
            , [ !"When a creature rolls a 1 on a saving throw against one of your hedge \
                  mage cantrips, it automatically fails the save and takes twice the \
                  number of damage dice dealt by the spell, as if you scored a critical \
                  hit."
              ] )
          ; ( "Silent Cantrip"
            , [ !"You can cast hedge mage cantrips silently, ignoring the verbal \
                  component of the spell if there is any."
              ] )
          ; ( "Static Cantrip"
            , [ !"Whenever you deal lightning damage to a hostile creature using a hedge \
                  mage cantrip, you can sap part of the energy into a charge which \
                  clings to your body until the beginning of your next turn. While \
                  charged, you can use your reaction when you take damage from a \
                  creature you can see within 5 feet of you to deal lightning damage \
                  equal to half your hedge mage level to the creature."
              ] )
          ]
  ;;

  let features = List.concat [ arcane_manuevers; fighting_style; tricks ]
end

module Gunslinger = struct
  let fighting_style =
    List.map
      ~f:(fun (name, entries) ->
        create ~name ~type_:"Gunslinger Fighting Style" ~entries ())
      Entry.
        [ ( "Akimbo"
          , [ !"When you engage in two-weapon fighting with firearms, you do not take a \
                penalty to the damage of the second attack."
            ] )
        ; ( "Bullseye"
          , [ !" You gain a +2 bonus to ranged attack rolls you make using firearms. The \
                weapon must have the Sighted property or have a normal range of 80 feet \
                or longer to gain this effect. This effect does not stack with the \
                Archery fighting style."
            ] )
        ; ( "Precise"
          , [ !"While you are wielding a firearm in one hand and nothing in the other, \
                if you make a ranged weapon attack and exceed the target's AC by 5 or \
                more, you deal an additional die of weapon damage. You can only use this \
                ability once per round."
            ] )
        ; ( "Scattershot"
          , [ !"When you hit with a ranged weapon attack using a firearm that has the \
                Scatter property, you can reroll the lowest damage die, and you must use \
                the new roll, even if the new roll is worse than the original."
            ] )
        ]
  ;;

  let deeds =
    List.map
      ~f:(fun (name, level, prereq, entries) ->
        create ~name ~level ~prereq:(prereq, prereq) ~type_:"Deed" ~entries ())
      []
    @ List.map
        ~f:(fun (name, entries) -> create ~name ~type_:"Deed" ~entries ())
        Entry.
          [ ( "Bite the Bullet"
            , [ !"As a bonus action on your turn, you can expend one risk die to gain \
                  temporary hit points equal the number rolled on the die + your \
                  Constitution modifier."
              ] )
          ; ( "Covering Fire"
            , [ !"When you hit a creature with a ranged weapon attack, you can expend \
                  one risk die as a bonus action to subdue the creature. Roll the risk \
                  die and subtract it from the next attack roll the creature makes \
                  before the start of your next turn."
              ] )
          ; ( "Dodge Roll"
            , [ !"You can expend one risk die as a bonus action to move up to 15 feet \
                  and reload any firearm you are holding. This movement does not provoke \
                  Opportunity Attacks, ignores difficult terrain, and can move you \
                  through hostile creature’s spaces, as long as you do not end your \
                  movement there."
              ] )
          ; ( "Limb Shot"
            , [ !"When you hit a creature with a ranged weapon attack, you can expend \
                  one risk die as a bonus action and aim for one of its limbs, forcing \
                  it to drop one item of your choice that it’s holding. The target must \
                  make a Constitution saving throw. On a failed save, it drops the \
                  object you choose. The object lands at its feet."
              ] )
          ; ( "Skin of Your Teeth"
            , [ !"When an attacker you can see makes an attack roll against you, you can \
                  expend a risk die as a reaction to dodge out of harm’s way at the last \
                  second. You add the risk die to your AC against this attack, \
                  potentially causing it to miss. "
              ] )
          ; ( "Steady Aim"
            , [ !"On your turn, you can use a bonus action and expend one risk die to \
                  double the normal and maximum range for the next ranged weapon attack \
                  you make."
              ] )
          ; ( "Fancy Gunplay"
            , [ !"You can impressively twirl your weapons to impress others. Whenever \
                  you make a Charisma ({@skill Performance}) check or a Dexterity \
                  ({@skill Sleight of Hand}) check using one of your ranged weapons, you \
                  can expend a risk die and add it to the ability check."
              ] )
          ; ( "Maverick Spirit"
            , [ !"As a reaction when you make an Intelligence, Wisdom, or Charisma \
                  saving throw, you can expend one risk die and add it to saving throw."
              ] )
          ; ( "Grazing Shot"
            , [ !"When you make a ranged attack against a target within the weapon's \
                  normal range and miss, you can spend a risk die as a reaction and deal \
                  damage to the target equal to the roll on the die. This ability deals \
                  no damage if the attack roll misses the target's AC by 10 or more."
              ] )
          ; ( "Reposition"
            , [ !"As a reaction when a hostile creature within 30 feet from you moves, \
                  you can expend a risk die to move up to your movement speed before \
                  they continue their turn."
              ] )
          ]
  ;;

  let features = List.concat [ fighting_style; deeds ]
end

module Warlock = struct
  let pact_boon =
    List.map
      ~f:(fun (name, entries) -> create ~name ~type_:"PB" ~entries ())
      Entry.
        [ ( "Pact of the Hourglass"
          , [ !"Your patron has gifted you a mystical timekeeping device. As an action, \
                you can create a pact hourglass in your empty hand. It takes the form of \
                a timekeeping device of your choice each time you summon it, and it can \
                be used as a spellcasting focus by you. Once per short rest, while you \
                have your pact hourglass summoned, you can use your reaction to briefly \
                rewind time and re-roll one attack roll, ability check, or saving throw \
                you make. Once you use this reaction you must finish a short or long \
                rest before you can use it again."
            ; !"Your pact hourglass disappears if it is more than 5 feet away from you \
                for 1 minute, if you use this feature again, if you dismiss your \
                hourglass (no action required), or if you die."
            ] )
        ; ( "Pact of the Shroud"
          , [ !"Your patron has gifted you a mystical cloak to shield you from harm. As \
                an action you can summon a pact shroud to cloak your body. It takes the \
                form of a robe or cloak of your choice each time you summon it. While \
                you have your pact shroud summoned, you gain proficiency in the Stealth \
                skill, and you have advantage on any Dexterity (Stealth) checks you make \
                to hide while obscured by shadows or darkness."
            ; !"Your pact shroud disappears if it is more than 5 feet away from you for \
                1 minute, if you use this feature again, if you dismiss your shroud (no \
                action required), or if you die."
            ; !"You can transform a magic robe or cloak of your choice into your pact \
                shroud by performing a special ritual over the course of a short or long \
                rest. Once complete, you can shunt it to an extradimensional space, or \
                summon it back, as an action. This ability does not effect artifacts or \
                sentient clothing, and you can only have one pact shroud at a time"
            ] )
        ]
  ;;

  let features = List.concat [ pact_boon ]
end

let vestiges = Binder.vestiges

let json =
  `List
    ([ Binder.features; Hedge_mage.features; Gunslinger.features; Warlock.features ]
    |> List.concat
    |> List.sort ~compare:[%compare: t]
    |> List.map ~f:to_json)
;;