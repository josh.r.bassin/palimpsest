open! Core
open! Async
open! Gen_lib

let hit_die faces = Die.create ~value:1 ~faces

let int_to_string = function
  | 0 -> "—"
  | x -> Int.to_string x
;;

let ability_score level =
  Entry.
    [ ![%string
         "When you reach %{level#Int}th level you can increase one ability score of your \
          choice by 2, two ability scores by one, or you may select a feat. You can't \
          increase an ability score above 20 using this feature."]
    ]
;;

module Binder = struct
  let name = "Binder"
  let subclass_name = "Doctrine Specialist"
  let hit_die = hit_die 8
  let proficiency = Ability_name.[ charisma; wisdom ]

  let starting_proficiencies =
    Proficiencies.(
      create
        ~armor:Armor.[ light; medium ]
        ~weapons:Weapon.[ simple ]
        ~tools:[]
        ~skills:
          Skill.
            [ choose
                ( 2
                , [ arcana
                  ; deception
                  ; history
                  ; insight
                  ; investigation
                  ; persuasion
                  ; religion
                  ] )
            ])
  ;;

  let starting_equipment =
    Equipment.of_string_list
      [ "A light crossbow, 20 bolts, and any simple weapon"
      ; "(a) scale mail or (b) leather armor"
      ; "(a) a component pouch or (b) an arcane focus"
      ; "(a) a scholar's pack or (b) a dungeoneer's pack"
      ]
  ;;

  let multiclassing =
    Class.Multiclass.create
      ~requirements:(Ability.create ~cha:13 ())
      ~proficiencies_gained:
        Proficiencies.(
          create ~armor:Armor.[ light; medium ] ~weapons:[] ~tools:[] ~skills:[])
  ;;

  let class_table =
    Class.Table.create
      ~header:
        [ "{@filter Vestiges|optionalfeatures|feature type=vestige} Level"
        ; "{@filter Vestiges|optionalfeatures|feature type=vestige} Bound"
        ]
      ~rows:
        (List.map
           ~f:(fun (l, r) -> [ l; Int.to_string r ])
           [ "1st", 1
           ; "1st", 1
           ; "2nd", 1
           ; "2nd", 1
           ; "3rd", 2
           ; "3rd", 2
           ; "4th", 2
           ; "4th", 2
           ; "5th", 2
           ; "5th", 2
           ; "6th", 3
           ; "6th", 3
           ; "7th", 3
           ; "7th", 3
           ; "8th", 3
           ; "8th", 3
           ; "9th", 4
           ; "9th", 4
           ; "9th", 4
           ; "9th", 4
           ])
      ()
  ;;

  let features =
    let open Feature in
    let c n l b e = create ~name:n ~level:l ~class_:name ~gain_subclass:b ~entries:e () in
    List.map
      ~f:(fun (n, l, b, e) -> c n l b e)
      Entry.
        [ ( "Soul Binding"
          , 1
          , false
          , [ !"In your studies you have uncovered the means to pierce the veil of the \
                realms and call to fragments of their Aspects. You learn how to bind \
                these fragments -- called {@filter Vestiges|optionalfeatures|feature \
                type=vestige} -- to your soul."
            ; "Binding Ritual"
              <-> [ "You can spend 10 minutes conducting a special binding ritual, which \
                     entails drawing the signs of vestiges in chalk, calling each by \
                     aspect or name, and performing other, more esoteric acts. During \
                     this ritual, vestiges manifest tangible signs as they press against \
                     the boundaries of reality and find purchase within your soul."
                  ; "At 1st level you can bind one vestige, and can bind more vestiges \
                     at higher levels, as shown in the Vestiges Bound column of the \
                     binder table. Unless otherwise specified, you can only bind \
                     vestiges who's combined level is no greater than your binder level."
                  ; "Vestiges remain bound until you finish a long rest. Once you \
                     perform a binding ritual, you may not do so again until you finish \
                     a long rest."
                  ]
            ; ne
                "Spellcasting Ability"
                [ !"Charisma is your spellcasting ability for all spells and powers \
                    granted to you by your vestiges, since you command the power of your \
                    vestiges through your very soul. Use your charisma score whenever a \
                    spell refers to your spellcasting ability. In addition, use your \
                    Charisma modifier when setting the saving throw DC for a spell or \
                    ability granted to you by one of your vestiges."
                ; sd "Vestige" Ability_name.charisma
                ; sa "Spell" Ability_name.charisma
                ]
            ] )
        ; ( "Variant: Fathomless Wisdom"
          , 1
          , false
          , [ !"Some binders enthrall fragments not via the power of their spirits, but \
                through a firm, unyielding devotion to those aspects. You use your \
                Wisdom, instead of Charisma, for your binder class abilities and \
                vestiges."
            ] )
        ; ( "Minor Fragments"
          , 2
          , false
          , [ !"Beginning at 2nd level, you can use the runoff energy from your binding \
                ritual to enlist two  minor fragments to your service, selected from the \
                {@filter Minor Fragments|optionalfeatures|feature type=minor fragment} \
                list. These fragments manifest faintly around you, though you can cause \
                them to become invisible or visible as a free action."
            ; !"You can bind additional fragments to your service as you gain additional \
                levels in this class, as shown on the binder table. When you gain a \
                level in this class, you can choose to replace a minor fragment you can \
                bind with another."
            ] )
        ; ( "Rebinding"
          , 2
          , false
          , [ !"Starting at 2nd level, you can use your action to perform a modified \
                version of the binding ritual, allowing you to expel a bound vestige \
                early and bind another vestige of equal or lower level in its place. Any \
                effects created by a dismissed vestige immediately end. Once you use \
                this ability, you can't use it again until you finish a long rest."
            ] )
        ; ( "Doctrine"
          , 3
          , true
          , [ !"Starting at 3rd level, you align yourself with a particular doctrine, a \
                secretive organization of binders bound together by similar motives and \
                shared mystic knowledge. Choose one of the doctrines in the subclass \
                menu. Your choice in doctrine grants you features at 3rd level, and \
                again at 7th, 10th, and 14th level."
            ] )
        ; ( "Suppress Sign"
          , 3
          , false
          , [ !"Also at 3rd level, you can use your action to conceal all Trait features \
                offered by your bound vestiges. All physical signs created by these \
                Traits vanish, but you can't use any Trait features until you use your \
                action to reveal your vestiges' Traits."
            ] )
        ; "Ability Score Improvement", 4, false, ability_score 4
        ; ( "Minor Fragments (3)"
          , 5
          , false
          , [ !"Beginning at 5th level, you can bind three minor fragments to your \
                service instead of two."
            ] )
        ; "Ability Score Improvement", 6, false, ability_score 6
        ; ( "Doctrine Feature"
          , 7
          , true
          , [ !"At 7th level, you gain a feature granted by your Doctrine." ] )
        ; "Ability Score Improvement", 8, false, ability_score 8
        ; ( "Adamant Mind"
          , 9
          , false
          , [ !"At 9th level, your experience in sharing your soul with otherworldly \
                entities has taught you how to guard your thoughts, and punish those \
                that dare to try and influence them. You have advantage on saving throws \
                against being charmed, frightened, or possessed, and on saving throws \
                against any effect that would sense your emotions or read your thoughts."
            ; !"Additionally, when you succeed on a save against such an effect caused \
                by a creature, the creature which created the effect takes psychic \
                damage equal to your binder level + your Charisma modifier."
            ] )
        ; ( "Doctrine Feature"
          , 10
          , true
          , [ !"At 10th level, you gain a feature granted by your Doctrine." ] )
        ; "Ability Score Improvement", 12, false, ability_score 12
        ; ( "Minor Fragments (4)"
          , 13
          , false
          , [ !"Beginning at 13th level, you can bind four minor fragments to your \
                service instead of three."
            ] )
        ; ( "Doctrine Feature"
          , 14
          , true
          , [ !"At 14th level, you gain a feature granted by your Doctrine." ] )
        ; ( "Rebinding Improvement"
          , 15
          , false
          , [ !"At 15th level, you can use rebind two vestiges instead of one when you \
                use your rebind ability."
            ] )
        ; "Ability Score Improvement", 16, false, ability_score 16
        ; ( "Minor Fragments (5)"
          , 18
          , false
          , [ !"Beginning at 18th level, you can bind five minor fragments to your \
                service instead of four."
            ] )
        ; "Ability Score Improvement", 19, false, ability_score 19
        ; ( "Voidsoul"
          , 20
          , false
          , [ !"By 20th level, your soul is so fragmented from its inhabiting vestiges \
                that you can adapt yourself into a vestige for a short time. As a bonus \
                action, you can bind an additional vestige of your choice of 3rd level \
                or lower for 1 minute. This vestige doesn't count against the total \
                number or level of vestiges you can bind."
            ; !"Once you use this ability, you can't use it again until you finish a \
                long rest."
            ] )
        ]
  ;;

  module Avatarist = struct
    let class_ = name
    let name = "Avatarist"
    let short_name = name

    let features =
      let open Subclass_feature in
      let c n l e = create ~name:n ~class_ ~subclass:short_name ~level:l ~entries:e () in
      List.map
        ~f:(fun (n, l, e) -> c n l e)
        Entry.
          [ ( "Avatarists"
            , 3
            , [ !"All binders call to their vestiges from between the realms, but only \
                  the Avatarists bring them into physical form, outside their very \
                  bodies. Doing so is an ancient discipline, harnessing the very magic \
                  which conjures fiends from the underworld—as with all the other great \
                  secrets of binding, conjuring an avatar is heresy of the highest \
                  magnitude. Those that manage this feat may join the ranks of the \
                  avatarists, a selective legion of binders who combat others using only \
                  manifestations of their spirits. Such combat is a battle of the ego as \
                  much as a physical brawl, for a fraction of any injury bestowed upon \
                  an avatar is in turn laid on the binder."
              ] )
          ; ( "Summon Avatar"
            , 3
            , [ !"Beginning when you join this doctrine at 3rd level, you can use your \
                  bonus action to manifest an avatar of your vestiges, a tangible spirit \
                  that appears within 5 feet of you, tethered to you by a ghostly cord. \
                  The avatar is a Medium fey with ability scores equal to your own and \
                  an Armor Class equal to 10 + your Charisma modifier + your Dexterity \
                  modifier. It appears as an embodiment of all your bound vestiges."
              ; !"The avatar does not have hit points, but you take the damage it would \
                  take as if it had resistance to all damage. It vanishes if you fall \
                  unconscious or if you dismiss it as a bonus action. It instantly \
                  appears at your side if you would ever be more than 60 feet from it."
              ; !"On your turn, you can command the avatar to fly up to 30 feet. You can \
                  make one or more of your attacks through the avatar when you take the \
                  Attack action on your turn. The avatar conjures a spectral duplicate \
                  of any weapon you’re holding and uses your attack bonus and the damage \
                  or your weapon. You can use your reaction to make an opportunity \
                  attack through your avatar when a creature moves out of its reach."
              ; !"Your avatar benefits from all your vestige abilities as if it had them \
                  bound. When you cast a spell or use a vestige ability, you can choose \
                  to deliver it through your avatar, as if it was the origin of the \
                  effect."
              ] )
          ; ( "Spirit Transposition"
            , 7
            , [ !"By 7th level, you can use your bonus action to exchange places with \
                  your avatar. Once you use this ability, you can’t use it again until \
                  you finish a short or long rest."
              ] )
          ; ( "Ritual of the Titan"
            , 10
            , [ !"Starting at 10th level, you can perform a special ritual over the \
                  course of 10 minutes, empowering your avatar into a towering spirit. \
                  When you summon your avatar, its size is Large, it can move only 20 \
                  feet on each of your turns, and it can add half your binder level to \
                  its damage rolls. These changes persist until you finish a long rest \
                  or you dismiss them as an action."
              ] )
          ; ( "True Avatar"
            , 14
            , [ !"Beginning at 14th level, as an action, you can trade roles with your \
                  avatar, becoming spirit, while it becomes more tangible in your place. \
                  For the next minute, you fade to the ethereal plane, as per the spell \
                  etherealness, from whence you can command your avatar in your place. \
                  At the end of this duration, or when you end this ability as an \
                  action, you return to the space you left or the nearest unoccupied \
                  space, if that space is occupied."
              ; !"Once you use this ability, you can’t use it again until you finish a \
                  long rest."
              ] )
          ]
    ;;

    let subclass = Class.Subclass.create ~name ~short_name ~class_ ~features ()
  end

  module Ascetic = struct
    let class_ = name
    let name = "Ascetic"
    let short_name = name

    let features =
      let open Subclass_feature in
      let c n l e = create ~name:n ~class_ ~subclass:short_name ~level:l ~entries:e () in
      List.map
        ~f:(fun (n, l, e) -> c n l e)
        Entry.
          [ ( "Ascetics"
            , 3
            , [ !"Quite to the contrary of other binders, Ascetics believe that \
                  self-discipline is the only path to enlightenment. Binding vestiges \
                  isn’t enough: one must have perfect control of their own soul to truly \
                  accept the gifts of wandering spirits. Thus, Ascetics deprive \
                  themselves of worldly pleasures and the very power of their bound \
                  vestiges in order to assert their will perfectly and achieve \
                  transcendental wisdom."
              ] )
          ; ( "Suppress Vestige"
            , 3
            , [ !"Starting when you choose this doctrine at 3rd level, you can use your \
                  action to suppress one of your bound vestiges to focus your body and \
                  mind. You can resume normal use of this vestige as a bonus action on \
                  your turn. While this vestige is suppressed, none of its features \
                  affect you, and you gain the following benefits:"
              ; lh
                  [ !!"While you are wearing no armor and not wielding a shield, your AC \
                       equals 10 + your Dexterity modifier + your Charisma modifier."
                  ; !!"You can use Charisma instead of Strength for the attack and \
                       damage rolls of your unarmed strikes."
                  ; !!"Your unarmed strikes use a {@dice 1d4} for damage. This die \
                       changes as you gain binder levels: {@dice 1d6} at 7th level, \
                       {@dice 1d8} at 10th level, and {@dice 1d10} at 14th level."
                  ; !!"You can use your bonus action to make a ranged spell attack with \
                       a range of 30 feet. You are proficient with this attack, and you \
                       add your Charisma modifier to its attack and damage rolls. It \
                       uses your unarmed strike damage die and deals psychic damage."
                  ; !!"Starting at 7th level, your unarmed strikes count as magical for \
                       the purposes of overcoming damage resistance and immunity."
                  ]
              ] )
          ; ( "Thousand Fists"
            , 7
            , [ !"Starting at 7th level, while you are suppressing a vestige, you can \
                  use your action to manifest a wave of psychic fists to strike your \
                  foes. You can make up to four unarmed strikes against creatures which \
                  are within 5 feet of you. You can’t attack a creature more than twice \
                  using this ability"
              ; !"Once you use this ability, you can’t use it again until you finish a \
                  short or long rest."
              ] )
          ; ( "Ritual of the Third Eye"
            , 10
            , [ !"Starting at 10th level, you can perform a special ritual over the \
                  course of 10 minutes, granting you blindsight with a range of 15 feet. \
                  You maintain concentration on this ability as you would a spell."
              ] )
          ; ( "Transcendent Control"
            , 14
            , [ !"By 14th level, you can achieve total focus by suppressing all your \
                  vestiges as a bonus action. For up to 1 minute, you gain the following \
                  benefits:"
              ; lh
                  [ !!"Your Armor Class is 20, if it would be lower."
                  ; !!"Whenever you make an attack roll and the result is less than 15, \
                       you can treat the result as 15."
                  ; !!"Your unarmed strikes use a {@dice 1d12} for damage."
                  ]
              ; !"Once you use this ability, you can’t use it again until you finish a \
                  long rest."
              ] )
          ]
    ;;

    let subclass = Class.Subclass.create ~name ~short_name ~class_ ~features ()
  end

  module Storyteller = struct
    let class_ = name
    let name = "Storyteller"
    let short_name = name

    let features =
      let open Subclass_feature in
      let c n l e = create ~name:n ~class_ ~subclass:short_name ~level:l ~entries:e () in
      List.map
        ~f:(fun (n, l, e) -> c n l e)
        Entry.
          [ ( "Storytellers"
            , 3
            , [ !"Members of the Doctrine of Storytellers seem almost cleric-like, but \
                  brandish different holy symbols and tell entirely different tales. \
                  Their scriptures are the legends of various vestiges, woven into a \
                  sort of pantheon, with the enigmatic Lacuna at its head."
              ] )
          ; ( "Bonus Proficiency"
            , 3
            , [ !"Starting when you choose this doctrine at 3rd level, you gain \
                  proficiency with the Religion skill, if you don’t have it already. "
              ] )
          ; ( "Heretical Divinity"
            , 3
            , [ !"Also at 3rd level, you can emulate divine magic through your vestigial \
                  heresy. You can cast the spells {@spell command}, {@spell cure \
                  wounds}, {@spell healing word}, and {@spell sanctuary} once each \
                  without expending a spell slot. Charisma is your spellcasting modifier \
                  for these spells. You regain all expended uses when you finish a long \
                  rest."
              ] )
          ; ( "Blessing of the Vestigial Pantheon"
            , 7
            , [ !"By 7th level, you receive the blessings of all the forgotten gods \
                  through your vestiges. As a reaction when you make a saving throw, you \
                  can add half your binder level (rounded down) to the roll."
              ; !"Once you use this ability, you can’t use it again until you finish a \
                  long rest."
              ] )
          ; ( "Ritual of Turning"
            , 10
            , [ !"Starting at 10th level, you can perform a special ritual over the \
                  course of 10 minutes, warding yourself against undead threats. For the \
                  next hour, whenever an undead targets you directly with an attack or \
                  harmful spell, that creature must make a Wisdom saving throw. On a \
                  failed save, that creature is turned for one minute or until it takes \
                  any damage. A turned creature must spend its turns trying to move as \
                  far away from you as it can, and can’t willingly move to a space \
                  within 30 feet of you. It also can’t take reactions. For its action, \
                  it can use only the Dash action or try to escape from an effect that \
                  prevents it from moving. If there’s nowhere to move, the creature can \
                  use the Dodge action. If a creature’s saving throw is successful, it \
                  is immune to this ability for the next 24 hours."
              ] )
          ; ( "Sacrifice to the Realms"
            , 14
            , [ !"At 14th level, you can cast your bound vestiges into the realms to \
                  bring forth miracles. On your turn, you can expel a vestige you have \
                  bound to cast a spell from the cleric spell list without using a spell \
                  slot. This spell must be of a level no greater than the level of the \
                  vestige expelled. Once you use this ability, you can’t do so again \
                  until you finish a long rest."
              ] )
          ]
    ;;

    let subclass = Class.Subclass.create ~name ~short_name ~class_ ~features ()
  end

  module Envy's_faithful = struct
    let class_ = name
    let name = "Envy's Faithful"
    let short_name = name

    let features =
      let open Subclass_feature in
      let c n l e = create ~name:n ~class_ ~subclass:short_name ~level:l ~entries:e () in
      List.map
        ~f:(fun (n, l, e) -> c n l e)
        Entry.
          [ ( "Envy's Faithful"
            , 3
            , [ !"Nearly all binders learn that the vestige of Envy is forbidden, for \
                  once it is summoned, it can never be dismissed. Driven by jealousy and \
                  obsession, the fragment seizes the soul of its binder and embraces it \
                  tightly. Its love is like a grasping weed, and it strangles all it \
                  touches. Yet those who reciprocate its love and placate the envious \
                  vestige might join the ranks of Envy's Faithful, a doctrine of binders \
                  who prize want above all else."
              ] )
          ; ( "Bonus Proficiency"
            , 3
            , [ !"Starting when you choose this doctrine at 3rd level, you gain \
                  proficiency with the Insight and Persuasion skills, if you don’t have \
                  them already."
              ] )
          ; ( "Envy's Embrace"
            , 3
            , [ !"Starting at 3rd level, you are bound to Envy and can’t be unbound from \
                  it. Envy does not count as a bound vestige, and does not count against \
                  your total number or level of vestiges bound. You only gain its \
                  Embrace and Envy's Mark features. At 7th level, you gain the use of \
                  its Enamor feature and at 14th level, you gain the use of its Trait: \
                  Arms of Embrace feature. "
              ] )
          ; ( "Ritual of Idolatry"
            , 10
            , [ !"Starting at 10th level, you can perform a special ritual over the \
                  course of 10 minutes, enrobing you in a 30-foot radius aura that \
                  infects the minds of others. When a creature enters aura for the first \
                  time on a turn or starts its turn there, it must make a Charisma \
                  saving throw against your spell save DC. On a failure, if the creature \
                  regards you as of a species and gender to which it is normally \
                  romantically attracted, it is charmed by you while it remains in the \
                  aura, up to a maximum of 1 minute. Once this effect ends for a \
                  creature, it is immune to the effects of the aura for 24 hours. This \
                  aura lasts for one hour, and its effects end early for a creature that \
                  takes damage or succeeds on an opposed ability check made to socially \
                  interact with you."
              ] )
          ; ( "Heartbreak"
            , 14
            , [ !"By 14th level, Envy's love can drive mad those who you expose to it. \
                  As an action, you can end the charmed condition on each creature \
                  charmed by you that you can see within 120 feet. A creature affected \
                  takes 4d8 psychic damage."
              ] )
          ]
    ;;

    let subclass = Class.Subclass.create ~name ~short_name ~class_ ~features ()
  end

  module Legion's_lodge = struct
    let class_ = name
    let name = "Legion's Lodge"
    let short_name = name

    let features =
      let open Subclass_feature in
      let c n l e = create ~name:n ~class_ ~subclass:short_name ~level:l ~entries:e () in
      List.map
        ~f:(fun (n, l, e) -> c n l e)
        Entry.
          [ ( "Legion's Lodge"
            , 3
            , [ !"The binders of Legion’s Lodge fill their souls with an abundance of \
                  fragments, becoming hives of wandering ghosts and whispered voices \
                  from beyond the pale. With each new fragment they bind, they develop \
                  even greater power, as the whole of their collection is mightier than \
                  the sum of its parts. At the peak of their strength, these binders \
                  speak with the voice of dozens in uncanny unison and sling attacks for \
                  a swarm of minor fragments which linger around them."
              ] )
          ; ( "We are Many"
            , 3
            , [ !"Beginning when you join this doctrine at 3rd level, you gain an \
                  additional minor fragment, which doesn’t count against your total \
                  number of minor fragments. At 10th level, you gain another additional \
                  minor fragment."
              ; !"Additionally, you can add your Charisma modifier to damage you deal \
                  with your minor fragments."
              ] )
          ; ( "Fragment Arcana"
            , 3
            , [ !"Also by 3rd level, you have unlocked the hidden potential of the \
                  myriad fragments residing within you. Each minor fragment you have \
                  bound grants you the ability to cast a cantrip, as shown on the \
                  Fragment Arcana table below."
              ; tbl
                  ~title:"Fragment Arcana"
                  ~labels:[ "Minor Fragments"; "Cantrips" ]
                  ~style:[ ""; "" ]
                  ~rows:
                    (List.map
                       [ "Blade", "True strike"
                       ; "Chill", "Ray of frost"
                       ; "Glitch", "Prestidigitation"
                       ; "Grue", "Acid splash"
                       ; "Haunt", "Chill touch"
                       ; "Lantern", "Guidance"
                       ; "Spark", "Shocking grasp"
                       ; "Stone", "Resistance"
                       ; "Strange", "Message"
                       ; "Torchling", "Produce flame"
                       ; "Totem", "Shillelagh"
                       ; "Wisp", "Minor Illusion"
                       ]
                       ~f:(fun (f, s) -> [ f; [%string "{@spell %{s}}"] ]))
              ] )
          ; ( "Extinguish Soul"
            , 7
            , [ !"Beginning at 7th level, you can burn the totality of a minor \
                  fragment's essence for a flash of great power. When you hit with a \
                  minor fragment's spell attack or a creature fails its saving throw \
                  against your minor fragment's ability, you can choose to deal four \
                  dice of damage, instead of only one. Once you do this with a minor \
                  fragment, the fragment is dismissed until you finish a long rest. \
                  While a minor fragment is dismissed, you can’t use any of its \
                  abilities or the cantrip the minor fragment allows you to use."
              ] )
          ; ( "Ritual of Fellowship"
            , 10
            , [ !"Starting at 10th level, you can perform a special ritual over the \
                  course of 10 minutes, manifesting a minor fragment you have bound in a \
                  physical form. This minor fragment becomes a familiar, as per the find \
                  familiar spell, and remains in this form until you take a long rest. \
                  Additionally, on your turn, you can command your familiar to use any \
                  of its abilities it offers as a minor fragment (using your action or \
                  bonus action, as appropriate), which originate from it."
              ] )
          ; ( "Fragment Horde"
            , 14
            , [ !"Beginning at 14th level you can bring your army of minor fragments to \
                  bear all at once. When you use your bonus action to make a spell \
                  attack with a minor fragment or use a minor fragment to cause a target \
                  to make a saving throw, you can use this ability twice, or use the \
                  bonus action of another minor fragment, targeting the same or \
                  different creatures."
              ] )
          ]
    ;;

    let subclass = Class.Subclass.create ~name ~short_name ~class_ ~features ()
  end

  module Flexile = struct
    let class_ = name
    let name = "Flexile"
    let short_name = name

    let features =
      let open Subclass_feature in
      let c n l e = create ~name:n ~class_ ~subclass:short_name ~level:l ~entries:e () in
      List.map
        ~f:(fun (n, l, e) -> c n l e)
        Entry.
          [ ( "Flexile Binder"
            , 3
            , [ !"Flexile Binders see the nature of the soul as not unlike that of the \
                  Deep itself: unknowable, fractal, and ultimately hollow. With their \
                  specialized ritual implements and ink made of lodestone, they can form \
                  special red seals with which to entrap vestiges deeper into their \
                  souls, capturing more of the vestige’s essence and allowing them \
                  greater control of the binding process."
              ] )
          ; ( "Flexible Rebinding"
            , 3
            , [ !"Starting when you join this doctrine at 3rd level, you can use your \
                  Rebinding feature twice, instead of once, between long rests."
              ] )
          ; ( "Vestigial Skill"
            , 3
            , [ !"At 3rd level, when a vestige’s Trait allows you to replace a skill \
                  roll with a 10 or your binder level plus your Charisma modifier, you \
                  can choose to gain advantage on the roll. You can choose to use this \
                  ability after seeing the result of the check. Once you use this \
                  ability, you must finish a long rest before using it again."
              ] )
          ; ( "Soul Transfer"
            , 7
            , [ !"Beginning at 7th level, you can transfer some of the damage you take \
                  to your vestiges. As a reaction when you take damage, you can halve \
                  the damage taken."
              ; !"Once you use this ability, you can’t use it again until you finish a \
                  short or long rest."
              ] )
          ; ( "Ritual of the Flexile Brand"
            , 10
            , [ !"Starting at 10th level, you can perform a special ritual over the \
                  course of 10 minutes, allowing you to partially bind an additional \
                  vestige. You can only bind one vestige at a time using this ability, \
                  and this vestige doesn’t count against the number of vestiges you can \
                  bind nor toward the number of vestiges you have bound. While it is \
                  bound, you gain only the vestige’s Bonus Proficiencies feature, if it \
                  has one, or one of its Traits, if it offers the ability to replace a \
                  skill roll with a 10 or your binder level plus your Charisma modifier. \
                  You remain partially bound to this vestige until you finish a long \
                  rest."
              ] )
          ; ( "Deep Binding"
            , 14
            , [ !"Starting at 14th level, the total level of vestiges you can have bound \
                  increases by 3."
              ] )
          ]
    ;;

    let subclass = Class.Subclass.create ~name ~short_name ~class_ ~features ()
  end

  module Shadowbinder = struct
    let class_ = name
    let name = "Shadowbinder"
    let short_name = name

    let features =
      let open Subclass_feature in
      let c n l e = create ~name:n ~class_ ~subclass:short_name ~level:l ~entries:e () in
      List.map
        ~f:(fun (n, l, e) -> c n l e)
        Entry.
          [ ( "Shadowbinder"
            , 3
            , [ !"Shadowbinder initiates learn the story of Lacuna's Shadow, a vestige \
                  sourced from the original creator itself. Initiates seek the Shadow's \
                  sigil -- a calling card able to revive Lacuna by pulling their essence \
                  back into their shadow, and by extension, bring about the coming of a \
                  new, more perfect world. By drawing parts of the sigil, Shadowbinders \
                  can pull forth raw aspect and shape it to their whims."
              ] )
          ; ( "Voidsight"
            , 3
            , [ !"At 3rd level, you gain darkvision out to a range of 60 feet. If you \
                  already have darkvision from your race, its range increases by 30 \
                  feet."
              ] )
          ; ( "Tenebrous Initiate"
            , 3
            , [ !"Starting at 3rd level, you can bend the walls of the universe, \
                  creating small rifts through the realms. Once on each of your turns \
                  when you hit a hostile creature with a melee attack, you can teleport \
                  5 feet to a location you can see."
              ] )
          ; ( "Oblivion Exile"
            , 7
            , [ !"At 7th level, you can use your action to cause a creature you can see \
                  within 60 feet to make a Wisdom saving throw against your Vestige save \
                  DC. On a failed save, the creature is banished into an endless stable \
                  demiplane. While there, the target is incapacitated. At the beginning \
                  of your next turn, the target reappears in the space it left or in the \
                  nearest unoccupied space if that space is occupied. You can also \
                  target yourself with this ability, requiring no saving throw."
              ; !"Once you use this ability, you can’t use it again until you finish a \
                  short or long rest."
              ] )
          ; ( "Ritual of the Aegis"
            , 10
            , [ !"Starting at 10th level, you can perform a special ritual over the \
                  course of 10 minutes, shrouding yourself in plates of unshaped aspect \
                  which act as ablative armor. You gain temporary hit points equal to \
                  your binder level plus your Charisma modifier. While these hit points \
                  remain, your AC equals 10 + your Dexterity modifier + your Charisma \
                  modifier. These temporary hit points remain until you finish a long \
                  rest."
              ] )
          ; ( "Shadow Sanctuary"
            , 14
            , [ !"Beginning at 14th level, the Shadow whisks you away to beyond the \
                  realms, rather than subjecting you to death. As a reaction when you \
                  take damage, but are not reduced to 0 hit points or killed outright, \
                  you can use your reaction to teleport yourself to an endless \
                  demiplane. You remain in this plane as long as you wish, and can use \
                  your action to return to the space you left or in the nearest \
                  unoccupied space if that space is occupied."
              ] )
          ]
    ;;

    let subclass = Class.Subclass.create ~name ~short_name ~class_ ~features ()
  end

  let class_ =
    Class.create
      ~name
      ~subclass_name
      ~hit_die
      ~proficiency
      ~starting_proficiencies
      ~starting_equipment
      ~multiclassing
      ~class_table
      ~features
      ()
  ;;

  let subclasses =
    [ Avatarist.subclass
    ; Ascetic.subclass
    ; Storyteller.subclass
    ; Envy's_faithful.subclass
    ; Legion's_lodge.subclass
    ; Flexile.subclass
    ; Shadowbinder.subclass
    ]
  ;;

  let subclass_features =
    List.concat
      [ Avatarist.features
      ; Ascetic.features
      ; Storyteller.features
      ; Envy's_faithful.features
      ; Legion's_lodge.features
      ; Flexile.features
      ; Shadowbinder.features
      ]
  ;;
end

module Hedge_mage = struct
  let name = "Hedge Mage"
  let subclass_name = "House Mage"
  let hit_die = hit_die 8
  let proficiency = Ability_name.[ constitution; intelligence ]

  let starting_proficiencies =
    Proficiencies.(
      create
        ~armor:Armor.[ light ]
        ~weapons:Weapon.[ simple ]
        ~tools:Tool.[ artisans_tools 1; musical_instrument ]
        ~skills:
          Skill.
            [ choose
                ( 2
                , [ acrobatics
                  ; animal_handling
                  ; arcana
                  ; athletics
                  ; history
                  ; investigation
                  ; medicine
                  ; perception
                  ; survival
                  ] )
            ])
  ;;

  let starting_equipment =
    Equipment.of_string_list
      [ "Leather armor, a dagger, and any simple weapon"
      ; "(a) a component pouch or (b) an arcane focus"
      ; "An explorer's pack and (a) a scholar's pack or (b) one tool kit you're \
         proficient in."
      ]
  ;;

  let multiclassing =
    Class.Multiclass.create
      ~requirements:(Ability.create ~int_:13 ())
      ~proficiencies_gained:
        Proficiencies.(create ~armor:Armor.[ light ] ~weapons:[] ~tools:[] ~skills:[])
  ;;

  let class_table =
    let cantrips_known = function
      | 1 | 2 -> 4
      | 3 | 4 -> 5
      | 5 | 6 | 7 | 8 -> 6
      | 9 | 10 | 11 | 12 -> 7
      | 13 | 14 | 15 | 16 -> 8
      | 17 | 18 | 19 -> 9
      | _ -> 10
    in
    let cantrip_bonus_dice = function
      | 1 | 2 | 3 | 4 -> 0
      | 5 | 6 | 7 | 8 | 9 | 10 -> 1
      | 11 | 12 | 13 | 14 | 15 | 16 -> 2
      | _ -> 3
    in
    let tricks_known = function
      | 1 -> 0
      | 2 -> 2
      | 3 | 4 -> 3
      | 5 | 6 -> 4
      | 7 | 8 -> 5
      | 9 | 10 -> 6
      | 11 | 12 -> 7
      | 13 | 14 -> 8
      | 15 | 16 -> 9
      | _ -> 10
    in
    Class.Table.create
      ~header:[ "Cantrips Known"; "Cantrip Bonus Dice"; "Tricks Known" ]
      ~rows:
        (List.init 20 ~f:(fun idx ->
             let idx = idx + 1 in
             List.map
               ~f:int_to_string
               [ cantrips_known idx; cantrip_bonus_dice idx; tricks_known idx ]))
      ()
  ;;

  let features =
    let open Feature in
    let c n l b e = create ~name:n ~level:l ~class_:name ~gain_subclass:b ~entries:e () in
    List.map
      ~f:(fun (n, l, e) -> c n l false e)
      Entry.
        [ ( "Hedge Magic"
          , 1
          , [ !"While other spellcasters aspire to grander and more complex spells, \
                hedge mages refine and master the most fundamental magic: cantrips. \
                Hedge mages wield their magic in the same way that a warrior uses a \
                sword, bow or axe: as weapons to be perfected and mastered, not as an \
                unknowable force to be feared. In contrast to sorcerers and wizards, \
                their magic is a trainable skill -- one that can be mastered by almost \
                anyone with discipline and aptitude."
            ] )
        ; ( "Spellcasting"
          , 1
          , [ !"At first level, you begin to learn the simple yet potent brand of \
                spellcasting for which hedge mages are known."
            ; "Cantrips"
              <-> [ "You learn four cantrips of your choice from the hedge mage spell \
                     list. You learn additional hedge mage cantrips of your choice at \
                     higher levels, as shown in the Cantrips Known column of the Hedge \
                     mage table."
                  ; "Additionally, when you gain a level in this class, you can choose \
                     one of the hedge mage cantrips you know and replace it with another \
                     hedge mage cantrip."
                  ]
            ; ne
                "Spellcasting Ability"
                [ !"Intelligence is your spellcasting ability for your hedge mage \
                    spells, since you learn your spells through practice and mental \
                    discipline. You can use your Intelligence whenever a spell refers to \
                    your spellcasting ability. In addition, you use your Intelligence \
                    modifier when setting the saving throw DC for a hedge mage spell you \
                    cast and when making an attack roll with one."
                ; sd "Spell" Ability_name.intelligence
                ; sa "Spell" Ability_name.intelligence
                ]
            ; "Spellcasting Focus"
              --> "You can use an arcane focus as a spellcasting focus for your hedge \
                   mage spells."
            ] )
        ; ( "Arcane Initiation"
          , 1
          , [ !"Hedge mages come from all backgrounds and walks of life. At 1st level, \
                choose where you first learned the basics of magic. The cantrips offered \
                by your initiation don't count against your total number of hedge mage \
                cantrips known."
            ; "Adventurer"
              --> "You picked up your magic informally by travelling with a dozen \
                   different mages over the years. You learn the {@spell mage hand} and \
                   {@spell ray of frost} cantrips."
            ; "Circus Performer"
              --> "You learned a few simple tricks to participate in a slideshow or \
                   circus act. You learn the {@spell dancing lights} and {@spell minor \
                   illusion} cantrips."
            ; "Mercenary"
              --> "You mastered the fundamentals of hedge magic to engage in battle with \
                   similarly-armed arcanists. You learn the {@spell arc blade|ALT} and \
                   {@spell true strike} cantrips."
            ; "Shadowtouched"
              --> "An influx of magic from Lacuna's Shadow left an imprint on you. You \
                   learn the {@spell chill touch} and {@spell message} cantrips."
            ; "Temple"
              --> "A monastery or temple educated you in the ways of gentle healing \
                   magic. You learn the {@spell sacred flame} and {@spell spare the \
                   dying} cantrips."
            ; "Tower Apprentice"
              --> "You apprenticed under a spellcaster for some time, who taught you the \
                   fundamentals of arcana. You learn the {@spell prestidigitation} and \
                   {@spell shocking grasp} cantrips."
            ; "Self-Taught"
              --> "You taught yourself all the fundamentals of magic from a dusty old \
                   tome or abandoned scroll. You learn the {@spell fire bolt} and \
                   {@spell shocking grasp} cantrips."
            ; "Survival"
              --> "To survive in the wilderness, you taught yourself to cast simple \
                   spells. You learn the {@spell druidcraft} and {@spell shillelagh} \
                   cantrips."
            ] )
        ; ( "Arcane Fighting Style"
          , 1
          , [ !"Hedge mages learn that magic is the purest of weapons, and can be \
                wielded as easily as any other. As 1st level, select a {@filter Hedge \
                Mage Fighting Style|optionalfeatures|feature type=hedge mage fighting \
                style}. You can't take the same Fighting Style option more than once, \
                even if you get to choose again."
            ; op
                [ "Blaster|ALT"
                ; "Deflector|ALT"
                ; "Resistive|ALT"
                ; "Sniper|ALT"
                ; "Striker|ALT"
                ]
            ] )
        ; ( "Empowered Magic"
          , 2
          , [ !"Starting at 2nd level, once on each of your turns when you deal damage \
                with a hedge mage cantrip, you can improve one damage roll of the spell, \
                adding your Intelligence modifier to the roll."
            ] )
        ; ( "Improved Empowered Magic"
          , 5
          , [ !"Starting at 5th level you may add additional dice of cantrip damage, as \
                shown in the Cantrip Bonus Dice column of the hedge mage table. This \
                stacks with the bonus from the original Empowered Magic feature."
            ] )
        ; ( "Hedge Mage Trick"
          , 2
          , [ !"Beginning at 2nd level, you learn a {@filter Hedge Mage \
                Trick|optionalfeatures|feature type=hedge mage trick}, a special \
                technique that alters the way you fight, move, and cast your spells. You \
                learn 2 tricks at 2nd level, and an additional trick as shown on the \
                Tricks Known column of the hedge mage table."
            ; !"Additionally, when you gain a level in this class, you can replace a \
                trick that you know with a another trick for which you meet the \
                prerequisites."
            ] )
        ; ( "Arcane Surge"
          , 5
          , [ !"Starting at 5th level, you learn to -- for a moment -- tap into a vast \
                reservoir of magical power and unleash it upon your foes. On your turn, \
                when you deal damage with a hedge mage cantrip, you can deal twice the \
                number of damage dice dealt by the spell. You can't use this ability on \
                a spell that has scored a critical hit."
            ; !"Once you use this ability, you can't use it again until you finish a \
                short or long rest."
            ] )
        ; ( "Improved Arcane Surge"
          , 11
          , [ !"You can use your Arcane Surge feature twice before resting, instead of \
                once."
            ] )
        ; ( "Tactical Insight"
          , 6
          , [ !"At 6th level, you learn how to use ambient magical power to defend \
                yourself from your foes' magical attacks. You can add your Intelligence \
                modifier to saving throws you make against spells and magical effects \
                that deal damage."
            ] )
        ; ( "Strategic Deflection"
          , 14
          , [ !"Starting at 14th level, as a reaction when you mage a successful saving \
                throw against a spell, you can redirect the spell's energy to a new \
                target. WHen you do, choose another creature (including the spellcaster) \
                you can see within the spell's range or 30 feet, whichever is closer. \
                The spell targets the chosen creature instead of you, as if you cast the \
                spell. The target creature makes its own save against your spell save \
                DC."
            ; !"Once you use this ability, you can't use it again until you finish a \
                short or long rest."
            ] )
        ; ( "Hedge Magic Mastery"
          , 20
          , [ !"At 20th level, you reach the pinnacle of your hedge magic. If you cast a \
                cantrip that deals 4 dice of damage to a target, it instead deals 5 dice \
                of damage. If you cast a cantrip which makes 4 attacks, it instead makes \
                5 attack."
            ] )
        ]
    @ List.map [ 4; 8; 12; 16; 19 ] ~f:(fun level ->
          let entries = ability_score level in
          c "Ability Score Improvement" level false entries)
    @
    let c n l e = c n l true e in
    Entry.
      [ c
          "Hedge Mage House"
          3
          [ !"Hedge mages collect into distinct Houses, teaching different skills, \
              abilities, and techniques. Upon reaching 3rd level, you can select a \
              House, which offers you features at 3rd level, and additional features at \
              7th, 10th, 15th, and 18th level."
          ]
      ; c "House Feature" 7 [ !"At 7th level, you gain a feature granted by your House." ]
      ; c
          "House Feature"
          10
          [ !"At 10th level, you gain a feature granted by your House." ]
      ; c
          "House Feature"
          15
          [ !"At 15th level, you gain a feature granted by your House." ]
      ; c
          "House Feature"
          18
          [ !"At 18th level, you gain a feature granted by your House." ]
      ]
  ;;

  let class_spells =
    [ `Spell "Acid Splash"
    ; `Spell "Blade Ward"
    ; `Spell "Chill Touch"
    ; `Spell "Dancing Lights"
    ; `Spell "Fire Bolt"
    ; `Spell "Friends"
    ; `Spell "Light"
    ; `Spell "Mage Hand"
    ; `Spell "Mending"
    ; `Spell "Message"
    ; `Spell "Minor Illusion"
    ; `Spell "Prestidigitation"
    ; `Spell "Poison Spray"
    ; `Spell "Produce Flame"
    ; `Spell "Ray of Frost"
    ; `Spell "Shocking Grasp"
    ; `Spell "True Strike"
    ; `Spell_source ("Arc Blade", "ALT")
    ; `Spell_source ("Burning Blade", "ALT")
    ; `Spell_source ("Card Trick", "ALT")
    ; `Spell_source ("Caustic Blade", "ALT")
    ; `Spell_source ("Cheat", "ALT")
    ; `Spell_source ("Cryptogram", "ALT")
    ; `Spell_source ("Force Buckler", "ALT")
    ; `Spell_source ("Force Dart", "ALT")
    ; `Spell_source ("Force Weapon", "ALT")
    ; `Spell_source ("Frigid Blade", "ALT")
    ; `Spell_source ("Lightning Surge", "ALT")
    ; `Spell_source ("Magic Daggers", "ALT")
    ; `Spell_source ("Phantom Grapnel", "ALT")
    ; `Spell_source ("Quickstep", "ALT")
    ; `Spell_source ("Sonic Pulse", "ALT")
    ; `Spell_source ("Springheel", "ALT")
    ; `Spell_source ("Thunderous Distortion", "ALT")
    ; `Spell_source ("Booming Blade", "TCE")
    ; `Spell_source ("Green-Flame Blade", "TCE")
    ; `Spell_source ("Lightning Lure", "TCE")
    ; `Spell_source ("Mind Sliver", "TCE")
    ; `Spell_source ("Sword Burst", "TCE")
    ; `Spell_source ("Control Flames", "XGE")
    ; `Spell_source ("Create Bonfire", "XGE")
    ; `Spell_source ("Frostbite", "XGE")
    ; `Spell_source ("Gust", "XGE")
    ; `Spell_source ("Infestation", "XGE")
    ; `Spell_source ("Mold Earth", "XGE")
    ; `Spell_source ("Shape Water", "XGE")
    ; `Spell_source ("Thunderclap", "XGE")
    ; `Spell_source ("Toll the Dead", "XGE")
    ]
  ;;

  module Bishop = struct
    let class_ = name
    let name = "House of Bishops"
    let short_name = name

    let subclass_tables =
      [ Class.Table.create
          ~header:[ "Spells Known" ]
          ~rows:
            (let spells_known = function
               | 1 | 2 -> 0
               | 3 -> 3
               | 4 | 5 | 6 -> 4
               | 7 -> 5
               | 8 | 9 -> 6
               | 10 | 11 -> 7
               | 12 -> 8
               | 13 -> 9
               | 14 | 15 -> 10
               | 16 | 17 | 18 -> 11
               | 19 -> 12
               | _ -> 13
             in
             List.init 20 ~f:(fun idx -> [ int_to_string (spells_known (idx + 1)) ]))
          ()
      ; Class.Table.create
          ~title:"Bishop Spellcasting"
          ~header:[ "1st"; "2nd"; "3rd"; "4th" ]
          ~rows:
            (let fst = function
               | 1 | 2 -> 0
               | 3 -> 2
               | 4 | 5 | 6 -> 3
               | _ -> 4
             in
             let snd = function
               | 1 | 2 | 3 | 4 | 5 | 6 -> 0
               | 7 | 8 | 9 -> 2
               | _ -> 3
             in
             let trd = function
               | 13 | 14 | 15 -> 2
               | 16 | 17 | 18 | 19 | 20 -> 3
               | _ -> 0
             in
             let frt = function
               | 19 | 20 -> 1
               | _ -> 0
             in
             List.init 20 ~f:(fun level ->
                 [ fst; snd; trd; frt ]
                 |> List.map ~f:(fun f -> f (level + 1))
                 |> List.map ~f:int_to_string))
          ()
      ]
    ;;

    let features =
      let open Subclass_feature in
      let c n l e = create ~name:n ~class_ ~subclass:short_name ~level:l ~entries:e () in
      List.map
        ~f:(fun (n, l, e) -> c n l e)
        Entry.
          [ ( "House of Bishops"
            , 3
            , [ !"The House of Bishops adopts hedge mages with true arcane potential, \
                  expanding on essential magic principles with true spellcasting. Such \
                  hedge mages might be easily confused for wizards or sorcerers, for \
                  they wear the same garb and wield many of the same spells, but the \
                  hedge mage focus on perfection still holds precedence over garnering a \
                  vast array of spells."
              ] )
          ; ( "Spellcasting"
            , 3
            , [ !"When you choose this house at 3rd level, you gain the ability to cast \
                  spells more potent than cantrips."
              ; "Spell Slots"
                --> "The Bishop Spellcasting table shows how many spell slots you have \
                     to cast your spells of 1st level and higher. To cast one of these \
                     spells, you must expend a slot of the spell’s level or higher. You \
                     regain all expended spell slots when you finish a long rest."
              ; "Spells known of 1st-level or higher"
                <-> [ "You know three 1st-level wizard spells of your choice, two of \
                       which you must choose from the conjuration and evocation spells \
                       on the wizard spell list."
                    ; "The Spells Known column of the Bishop Spellcasting table shows \
                       when you learn more wizard spells of 1st level or higher. Each of \
                       these spells must be a conjuration or evocation spell of your \
                       choice, except for the spells learned at 8th, and 14th level, and \
                       must be of a level for which you have spell slots."
                    ; "Whenever you gain a level in this class, you can replace one of \
                       the wizard spells you know with another spell of your choice from \
                       the wizard spell list. The new spell must be of a level for which \
                       you have spell slots, and it must be a conjuration or evocation \
                       spell, unless you’re replacing the spell you gained at 8th or \
                       14th level."
                    ]
              ; "Empowered Magic"
                --> "You can add additional damage with your hedge mage spells of 1st \
                     level or higher using your Empowered Magic feature, as if they were \
                     cantrips."
              ] )
          ; ( "Arcane Study"
            , 3
            , [ !"At 3rd level, you become proficient in two of the following skills: \
                  {@skill Arcana}, {@skill History}, {@skill Medicine}, {@skill \
                  Investigation}, or {@skill Religion}."
              ] )
          ; ( "Mystical Companion"
            , 7
            , [ !"At 7th level, you learn the find familiar spell and can cast it as a \
                  ritual. The spell doesn’t count against your number of spells known."
              ; !"When you cast the spell, you can choose one of the normal forms for \
                  your familiar or one of the following special forms: imp, \
                  pseudodragon, quasit, or sprite."
              ] )
          ; ( "Siege Casting"
            , 10
            , [ !"At 10th level, damage you deal to objects with a hedge mage spell is \
                  doubled. Additionally, when you cast a hedge mage spell which requires \
                  a spell attack roll, you can cast it at up to double its normal range. \
                  If its target is beyond its normal range, the spell attack has \
                  disadvantage."
              ] )
          ; ( "Arcane Sculpting"
            , 15
            , [ !"Starting at 15th level, when you cast a hedge mage spell that affects \
                  other creatures that you can see, you can choose a number of them \
                  equal to your Intelligence modifier. The chosen creatures \
                  automatically succeed on their saving throws against the spell, and \
                  they take no damage if they would normally take half damage on a \
                  successful save."
              ] )
          ; ( "Arcane Dominance"
            , 18
            , [ !"At 18th level, you learn a powerful magical trick. As a bonus action \
                  on your turn, you can expend a number of spell slots with a combined \
                  level of 6 or more to regain an expended use of your Arcane Surge."
              ] )
          ]
    ;;

    let subclass =
      Class.Subclass.create ~name ~short_name ~class_ ~subclass_tables ~features ()
    ;;
  end

  module Dice = struct
    let class_ = name
    let name = "House of Dice"
    let short_name = name

    let features =
      let open Subclass_feature in
      let c n l e = create ~name:n ~class_ ~subclass:short_name ~level:l ~entries:e () in
      List.map
        ~f:(fun (n, l, e) -> c n l e)
        Entry.
          [ ( "House of Dice"
            , 3
            , [ !"The House of Dice throws out the hedge mage convention of meticulous, \
                  deliberate spellcasting in favor of wild, chaotic magic that presses \
                  the odds. Everything, from their magical bursts to their enchanted \
                  sets of dice can fail horribly, so it’s auspicious that they often \
                  keep their fingers on the scales of fate. These hedge mages have \
                  mastered not only a repertoire of cantrips, but also techniques to \
                  manipulate fortune itself, allowing them to risk everything, and more \
                  than often come out on top."
              ] )
          ; ( "Bonus Proficiencies"
            , 3
            , [ !"Starting when you choose this house at 3rd level, your light fingers \
                  and unscrupulous tactics help you win the day. You gain proficiency in \
                  the {@skill Sleight of Hand} skill and with gaming sets (dice set), if \
                  you didn’t have it already."
              ] )
          ; ( "Dice of Fate"
            , 3
            , [ !"At 3rd level, you gain four Dice of Fate, which are d6s. Whenever you \
                  make an ability check, attack roll, saving throw, or damage roll, you \
                  can expend a Die of Fate and add it to the roll. Once you expend a Die \
                  of Fate, it goes to the DM, who can use it to add it to a roll made by \
                  an NPC or monster. Once the DM has used a die, it passes back to you, \
                  and so on. When you finish a long rest, you regain all of your \
                  expended Dice of Fate, whether or not the DM has used them."
              ] )
          ; ( "Dice of Fate (2)"
            , 7
            , [ !"Beginning at 7th level, you have an additional 2 Dice of Fate, and you \
                  can add two Dice of Fate to your damage rolls with hedge mage spells."
              ] )
          ; ( "Chaos Roll"
            , 3
            , [ !"Also at 3rd level, you can expend two of your Dice of Fate as an \
                  action, rolling them on the table below to create a chaotic surge of \
                  energy."
              ; tbl
                  ~title:"Chaos Roll"
                  ~labels:[ "2d6"; "Effect" ]
                  ~style:[ ""; "" ]
                  ~rows:
                    (List.map
                       ~f:(fun (roll, effect) -> [ Int.to_string roll; effect ])
                       [ 2, "You cast {@spell fireball}, centered on yourself."
                       ; 3, "Your AC is reduced by 2 until the start of your next turn."
                       ; 4, "You fall prone"
                       ; ( 5
                         , "Each creature other than yourself within 60 feet of you can \
                            speak only in a babbling nonsense language for the next \
                            minute, and can’t perform the verbal components of spells." )
                       ; ( 6
                         , "A 5-foot radius sphere of butterflies, insects, or doves \
                            fills a location you can choose within range, heavily \
                            obscuring the area until the start of your next turn." )
                       ; ( 7
                         , "You gain 7 temporary hit points, and keep the Dice of Fate \
                            instead of giving them to the DM." )
                       ; ( 8
                         , "You become invisible until the end of your next turn, as per \
                            the spell {@spell invisibility}." )
                       ; ( 9
                         , "A random object explodes nearby, dealing no damage to you or \
                            your allies, and dealing {@dice 3d6} fire damage to one \
                            creature caught in the blast chosen by the DM." )
                       ; ( 10
                         , "You teleport up to 60 feet to an unoccupied location you can \
                            see. Each creature within 5 feet of the destination must \
                            make a Dexterity saving throw against your spell save DC or \
                            take {@dice 2d6} force damage." )
                       ; ( 11
                         , "Choose a creature you can see within 60 feet. That creature \
                            takes {@dice 4d6} necrotic damage, and you regain hit points \
                            equal to the necrotic damage dealt." )
                       ; ( 12
                         , "You cast {@spell lightning bolt} and can add the Dice of \
                            Fate to the damage roll." )
                       ])
              ] )
          ; ( "Loaded Dice"
            , 7
            , [ !"By 7th level, you can subtly cheat your dice. Once on each of your \
                  turns when you roll a d6, you can flip the die upside down. Note that \
                  on a balanced d6, the top and bottom numbers add up to 7, so you can \
                  determine the bottom number by subtracting the top from 7."
              ] )
          ; ( "Twisted Fate"
            , 10
            , [ !"Starting at 10th level, the winds of chance follow your die rolls, \
                  rather than vice-versa. When you make an attack roll or ability check \
                  with disadvantage on your turn, you can attempt to invert fate as a \
                  bonus action. Expend a Die of Fate and roll it; on a 5, you ignore \
                  disadvantage on the roll, on a 6, you instead have advantage on the \
                  attack roll or ability check."
              ] )
          ; ( "Roll the Bones"
            , 15
            , [ !"Beginning at 15th level, you can channel the chaotic energy of your \
                  dice in an instant. As a reaction when you take damage from a creature \
                  you can see, you can expend two Dice of Fate to make a Chaos Roll."
              ] )
          ; ( "Steal Luck"
            , 18
            , [ !"Starting at 18th level, when you roll initiative, roll a d6. You steal \
                  that many Dice of Fate back from the DM."
              ] )
          ]
    ;;

    let subclass = Class.Subclass.create ~name ~short_name ~class_ ~features ()
  end

  module King = struct
    let class_ = name
    let name = "House of Kings"
    let short_name = name

    let subclass_tables =
      [ Class.Table.create
          ~header:[ "Battle Dice" ]
          ~rows:
            (let battle_dice = function
               | 1 | 2 -> int_to_string 0
               | 3 | 4 | 5 | 6 -> "2d8"
               | 7 | 8 | 9 | 10 | 11 | 12 -> "3d8"
               | 13 | 14 | 15 | 16 | 17 | 18 -> "3d10"
               | _ -> "4d10"
             in
             List.init 20 ~f:(fun idx -> [ battle_dice (idx + 1) ]))
          ()
      ]
    ;;

    let features =
      let open Subclass_feature in
      let c n l e = create ~name:n ~class_ ~subclass:short_name ~level:l ~entries:e () in
      List.map
        ~f:(fun (n, l, e) -> c n l e)
        Entry.
          [ ( "House of Kings"
            , 3
            , [ !"Hedge mages who train in the House of Kings specialize in tactics and \
                  strategy, learning age-old maneuvers to give them the edge in combat. \
                  Natural-born leaders, Kings work best with a cadre of supporters to \
                  perform their tactics and overwhelm the enemy. Moreso than all other \
                  hedge mages, those in the House of Kings treat life and death as \
                  little more than a game of strategy to be understood and conquered. "
              ] )
          ; ( "Bonus Proficiencies"
            , 3
            , [ !"When you choose this house at 3rd level, you gain proficiency with \
                  medium armor, {@item battleaxe|phb|battleaxes}, {@item \
                  longsword|phb|longswords}, {@item trident|phb|tridents}, {@item \
                  lance|phb|lances}, and {@item warhammer|phb|warhammers}."
              ] )
          ; ( "Battle Tactics"
            , 3
            , [ !"At 3rd level, you learn maneuvers that are fueled by special dice \
                  called Battle dice."
              ; "Battle Dice"
                <-> [ "You have two battle dice, which are d8s. A battle die is expended \
                       when you use it. You regain all of your battle dice when you take \
                       a short or long rest, or when you roll initiative."
                    ; "Your battle die changes and more battle dice become available \
                       when you reach certain levels in this class, as shown on the \
                       Battle Dice column of the hedge mage table."
                    ]
              ; "Using Battle Dice"
                --> "Once per turn, you can expend a battle die to perform an {@filter \
                     Arcane Manuever|optionalfeatures|feature type=arcane maneuver} of \
                     your choice."
              ; "Saving Throws"
                --> "When a maneuver calls for a saving throw to resist its effects, use \
                     your spell save DC."
              ] )
          ; ( "Lead From the Front"
            , 7
            , [ !"Starting at 7th level, you and each friendly creature within 100 feet \
                  that can see you ignores nonmagical difficult terrain. "
              ] )
          ; ( "Tactical Master"
            , 10
            , [ !"Starting at 10th level, friendly creatures within 10 feet of you add \
                  your Intelligence modifier to saving throws against spells and magical \
                  effects that deal damage."
              ] )
          ; ( "Manuever: Checkmate"
            , 15
            , [ !"Starting at 15th level, you learn the {@optfeature Checkmate|ALT} \
                  manuever."
              ; "Checkmate"
                --> "When you hit a creature with a weapon or spell attack, you can use \
                     your bonus action and expend a battle die to direct one of your \
                     companions to strike. When you do so, choose a friendly creature \
                     who can see or hear you that is within reach of the creature you \
                     hit. That creature can immediately use its reaction to make one \
                     weapon attack or cast a cantrip requiring an attack roll, adding \
                     the battle die to the attack’s damage roll."
              ] )
          ; ( "Grandmaster"
            , 18
            , [ !"Beginning at 18th level, when you roll initiative, choose a number of \
                  friendly creatures equal to your Intelligence modifier that can hear \
                  or see you to gain a battle die. Once within the next 10 minutes, the \
                  creature can roll the die and add the number rolled to one ability \
                  check, attack roll, damage roll, or saving throw it makes. The \
                  creature can wait until after it makes the roll before deciding to add \
                  the battle die, but must decide before the DM declares the roll’s \
                  outcome. Once the battle die is rolled, it is lost. A creature can \
                  have only one battle die from this feature at a time."
              ] )
          ]
    ;;

    let subclass =
      Class.Subclass.create ~name ~short_name ~class_ ~subclass_tables ~features ()
    ;;
  end

  module Lancer = struct
    let class_ = name
    let name = "House of Lancers"
    let short_name = name

    let features =
      let open Subclass_feature in
      let c n l e = create ~name:n ~class_ ~subclass:short_name ~level:l ~entries:e () in
      List.map
        ~f:(fun (n, l, e) -> c n l e)
        Entry.
          [ ( "House of Lancers"
            , 3
            , [ !"Hedge mages who join the House of Lancers are trained in the fluid \
                  martial arts and meditative asceticism of monks, and have learned to \
                  meld these practices with their potent spellcasting. As such, they are \
                  unarmed elemental warriors, self-reliant and capable of feats no monk \
                  or hedge mage could hope to match. However, Lancers are notoriously \
                  neutral in almost all matters, and stand apart from the politics of \
                  the other Houses."
              ] )
          ; ( "Intercept Technique"
            , 3
            , [ !"Starting when you choose this house at 3rd level, you adopt the \
                  monastic principle of offensive protection. While you are unarmored or \
                  are under the effects of {@spell mage armor}, you can add your \
                  Intelligence, rather than your Dexterity, to your Armor Class."
              ] )
          ; ( "Hand-to-Hand Arcana"
            , 3
            , [ !"Also at 3rd level, you learn the Lancer’s secret unarmed combat \
                  technique, granting you the following benefits:"
              ; lh
                  [ !!"You can use Intelligence instead of Strength for the attack and \
                       damage rolls of your unarmed strikes."
                  ; !!"Your unarmed strikes count as melee weapons for the purposes of \
                       hedge mage spells and cantrips."
                  ; !!"Your unarmed strikes deal 1d6 bludgeoning damage"
                  ]
              ] )
          ; ( "Shock Trooper"
            , 3
            , [ !"Starting at 3rd level, can rapidly close the distance between you and \
                  your foes. Whenever you make a melee attack on your turn against a \
                  creature you can see, you can lunge up to 15 feet toward your target \
                  before making the attack. This movement doesn’t provoke opportunity \
                  attacks. You can perform this movement even if it causes you to travel \
                  through the air, though you fall after making the attack if you do not \
                  land on solid ground."
              ] )
          ; ( "Mystical Physicality"
            , 7
            , [ !"Starting at 7th level, whenever you make a Strength, Dexterity, or \
                  Constitution check, you can use your Intelligence modifier instead of \
                  the normal modifier."
              ; !"In addition, moving through rough terrain no longer costs you \
                  additional movement."
              ] )
          ; ( "Deflect Energy"
            , 10
            , [ !"By 10th level, you can deflect bolts of energy with your bare hands. \
                  As a reaction when you are hit by a ranged spell attack or a ranged \
                  weapon attack which deals cold, fire, force, lightning, necrotic, or \
                  radiant damage, you can use your reaction to deflect the bolt. The \
                  damage you take from the attack is reduced by 1d10 + your Intelligence \
                  modifier + half your hedge mage level (rounded down)."
              ] )
          ; ( "Improved Shock Trooper"
            , 15
            , [ !"Starting at 15th level, you can lunge up to 30 feet using your Shock \
                  Trooper ability. This movement causes you to teleport through \
                  creatures and objects, blinking to the target in an instant. You can’t \
                  end your movement in an occupied space."
              ] )
          ; ( "Flurry of Spells"
            , 18
            , [ !"Starting at 18th level, you can cast spells with superhuman speed. As \
                  an action, you can expend a use of your Arcane Surge ability to cast \
                  three different cantrips which have a casting time of 1 action or 1 \
                  bonus action. You can’t use your Arcane Surge feature on any of these \
                  cantrips."
              ; !"Once you use this ability, you can’t use it again until you finish a \
                  long rest."
              ] )
          ]
    ;;

    let subclass = Class.Subclass.create ~name ~short_name ~class_ ~features ()
  end

  module Knight = struct
    let class_ = name
    let name = "House of Knights"
    let short_name = name

    let features =
      let open Subclass_feature in
      let c n l e = create ~name:n ~class_ ~subclass:short_name ~level:l ~entries:e () in
      List.map
        ~f:(fun (n, l, e) -> c n l e)
        Entry.
          [ ( "House of Knights"
            , 3
            , [ !"The House of Knights have a long and storied tradition of melding \
                  melee combat and swordplay with the hedge mage's simple spellcasting. \
                  Unlike other warriors, however, Knights carry neither swords or \
                  shields into combat, for they can forge both out of magical force at a \
                  moment’s notice."
              ] )
          ; ( "Bonus Proficiencies"
            , 3
            , [ !"When you choose this house at 3rd level, you gain proficiency with \
                  medium armor and martial weapons."
              ] )
          ; ( "Force Breastplate"
            , 3
            , [ !"At 3rd level, plates of magical force reinforce your armor. While you \
                  are wearing light or medium armor or are under the effects of {@spell \
                  mage armor}, you can add your Intelligence, rather than your \
                  Dexterity, to your Armor Class. "
              ] )
          ; ( "Mystical Weapon"
            , 3
            , [ !"Also at 3rd level, you learn the {@spell force weapon} cantrip, which \
                  does not count against your total number of cantrips known. \
                  Additionally, on your turn when you would draw a weapon, you can \
                  summon a simple or martial weapon, made entirely of magical force, to \
                  your empty hand. This weapon counts as magical for the purposes of \
                  overcoming resistance and immunity to nonmagical attacks and damage. \
                  This weapon vanishes if it leaves your hand."
              ] )
          ; ( "Dancing Blades"
            , 7
            , [ !"Beginning at 7th level, when you cast a cantrip which allows you to \
                  make multiple spell attacks, such as {@spell force weapon} or {@spell \
                  magic daggers}, you can use your bonus action to make one additional \
                  spell attack."
              ] )
          ; ( "Knight's Ward"
            , 10
            , [ !"Starting 10th level, you learn to forge a hardened magical barrier \
                  between you and your foes. As a bonus action on your turn, you can \
                  gain a number of temporary hit points equal to twice your hedge mage \
                  level, which last for 1 minute."
              ; !"Once you use this feature, you can’t use it again until you finish a \
                  short or long rest."
              ] )
          ; ( "Tactical Maneuver"
            , 15
            , [ !"At 15th level, you can spend your entire movement to teleport up to \
                  half your movement speed to a location you can see."
              ] )
          ; ( "Cleaving Blades"
            , 18
            , [ !"Starting at 18th level, you can summon a whirlwind of mystical \
                  weapons, striking at a legion of foes in a single swipe. As an action \
                  on your turn, you can choose up to 5 creatures you can see within 30 \
                  feet. Make a single melee spell attack against each target. On a hit, \
                  a target takes {@dice 2d10} + your Intelligence modifier force \
                  damage. "
              ] )
          ]
    ;;

    let subclass = Class.Subclass.create ~name ~short_name ~class_ ~features ()
  end

  module Pawn = struct
    let class_ = name
    let name = "House of Pawns"
    let short_name = name

    let features =
      let open Subclass_feature in
      let c n l e = create ~name:n ~class_ ~subclass:short_name ~level:l ~entries:e () in
      List.map
        ~f:(fun (n, l, e) -> c n l e)
        Entry.
          [ ( "House of Pawns"
            , 3
            , [ !"One of the most prominent hedge mage Houses is that of the Pawns. \
                  Hedge mages which join the House of Pawns learn to stretch their skill \
                  with cantrips to its limits. Pawns can master any cantrip or trick \
                  known to the other hedge mage houses, for they embody the adaptability \
                  all hedge mages aspire to. "
              ] )
          ; ( "Promotion"
            , 3
            , [ !"When you choose this house at 3rd level, you gain the adaptability of \
                  the quintessential hedge mage. You learn one {@filter Hedge Mage \
                  Trick|optionalfeatures|feature type=hedge mage trick} of your choice, \
                  which doesn’t count against your total number of hedge mage tricks \
                  known. Additionally, whenever you learn a hedge mage trick, you can \
                  learn tricks which have the House of Bishops, House of Kings, House of \
                  Knights, or House of Rooks as a prerequisite, so long as you meet all \
                  of the trick's other prerequisites."
              ] )
          ; ( "Adaptive Arcanist"
            , 3
            , [ !"Also at 3rd level, you learn to emulate the spellcasting prowess of \
                  other hedge mages. When you finish a short or long rest, choose one \
                  hedge mage cantrip. You learn this cantrip, which doesn’t count \
                  against the total number of hedge mage cantrips you can learn, until \
                  you choose a different one with this feature."
              ] )
          ; ( "Rapid Withdrawal"
            , 7
            , [ !"Beginning at 7th level, when you cast a cantrip targeting a creature, \
                  you don’t provoke opportunity attacks from that creature for the rest \
                  of the turn, whether you deal damage using the cantrip or not."
              ] )
          ; ( "Additional Arcane Fighting Style"
            , 10
            , [ !"At 10th level, you can choose a second option from the Arcane Fighting \
                  Style class feature."
              ] )
          ; ( "Opening Move"
            , 15
            , [ !"Starting at 15th level, you can add your Intelligence modifier to your \
                  initiative rolls. Additionally, when you roll initiative and you are \
                  not surprised, you can move up to your movement speed."
              ] )
          ; ( "Fundamental Mastery"
            , 18
            , [ !"Starting at 18th level, your magic always finds its way to your foes \
                  most vulnerable spots. Once on each turn when you roll damage for a \
                  hedge mage cantrip, you can choose to replace one damage die roll with \
                  the maximum possible result. "
              ] )
          ]
    ;;

    let subclass = Class.Subclass.create ~name ~short_name ~class_ ~features ()
  end

  module Rook = struct
    let class_ = name
    let name = "House of Rooks"
    let short_name = name

    let features =
      let open Subclass_feature in
      let c n l e = create ~name:n ~class_ ~subclass:short_name ~level:l ~entries:e () in
      List.map
        ~f:(fun (n, l, e) -> c n l e)
        Entry.
          [ ( "House of Rooks"
            , 3
            , [ !"The clandestine members of the House of Rooks make adept spies, \
                  assassins, and scouts, though they might just as easily pick up jobs \
                  run by rogues and brigands. In addition to their suite of cantrips, \
                  Rooks are light on their feet and know a few tricks to magically open \
                  doors in utter silence."
              ] )
          ; ( "Rook Strike"
            , 3
            , [ !"Starting you choose this house at 3rd level, as a bonus action, you \
                  can gain advantage on the next spell attack roll you make on your \
                  turn. Alternatively, you can impose disadvantage on a saving throw a \
                  creature makes against a hedge mage spell you cast before the end of \
                  your turn. Once you use this ability, you can’t use it again until you \
                  finish a short or long rest, or you use a cantrip to reduce a creature \
                  to 0 hit points. "
              ] )
          ; ( "Rasp"
            , 3
            , [ !"Starting at 3rd level, you can cast the {@spell knock} spell at will \
                  without using a spell slot or spell components. When you cast the \
                  spell using this ability, the casting time is increased to 1 minute \
                  and the spell is completely silent. "
              ] )
          ; ( "Arcane Acrobat"
            , 7
            , [ !"Beginning at 7th level, you can add your Intelligence modifier to all \
                  Dexterity checks you make. Additionally, while you are conscious, you \
                  ignore falling damage from falling any distance shorter than 60 feet. \
                  Subtract 60 feet from the distance fallen when calculating falling \
                  damage from further drops."
              ] )
          ; ( "Fleeting Decoy"
            , 10
            , [ !"At 10th level, as a reaction when you take damage from a creature you \
                  can see, you raise a defensive illusion to protect you from further \
                  harm. Attacks made against you have disadvantage until the beginning \
                  of your next turn. "
              ] )
          ; ( "Elusive Step"
            , 15
            , [ !"By 15th level, you’re extremely difficult to pin down. If you move \
                  more than 15 feet on your turn, any additional movement you make does \
                  not provoke opportunity attacks. "
              ] )
          ; ( "Glamerweave"
            , 18
            , [ !"By 15th level, you’re extremely difficult to pin down. If you move \
                  more than 15 feet on your turn, any additional movement you make does \
                  not provoke opportunity attacks."
              ; !"Once you use this feature, you can’t use it again until you finish a \
                  short or long rest."
              ] )
          ]
    ;;

    let subclass = Class.Subclass.create ~name ~short_name ~class_ ~features ()
  end

  let class_ =
    Class.create
      ~name
      ~subclass_name
      ~hit_die
      ~proficiency
      ~starting_proficiencies
      ~starting_equipment
      ~multiclassing
      ~class_table
      ~features
      ~class_spells
      ()
  ;;

  let subclasses =
    [ Bishop.subclass
    ; Dice.subclass
    ; King.subclass
    ; Lancer.subclass
    ; Knight.subclass
    ; Pawn.subclass
    ; Rook.subclass
    ]
  ;;

  let subclass_features =
    List.concat
      [ Bishop.features
      ; Dice.features
      ; King.features
      ; Lancer.features
      ; Knight.features
      ; Pawn.features
      ; Rook.features
      ]
  ;;
end

module Gunslinger = struct
  let name = "Gunslinger"
  let subclass_name = "Gunslinger"
  let hit_die = hit_die 8
  let proficiency = Ability_name.[ dexterity; charisma ]

  let starting_proficiencies =
    Proficiencies.(
      create
        ~armor:Armor.[ light ]
        ~weapons:
          Weapon.[ simple; martial; custom "simple firearms"; custom "martial firearms" ]
        ~tools:Tool.[ tinkers_tools; gaming_set ]
        ~skills:
          Skill.
            [ choose
                ( 2
                , [ animal_handling
                  ; athletics
                  ; acrobatics
                  ; insight
                  ; intimidation
                  ; deception
                  ; perception
                  ; persuasion
                  ; sleight_of_hand
                  ] )
            ])
  ;;

  let starting_equipment =
    Equipment.of_string_list
      [ "Leather armor and a dagger"
      ; "Any two-handed firearm that isn’t heavy and 30 bullets"
      ; "(a) a handgun and 20 bullets or (b) a revolver and 10 bullets"
      ; "(a) an explorer’s pack or (b) one kit you're proficient with "
      ]
  ;;

  let multiclassing =
    Class.Multiclass.create
      ~requirements:(Ability.create ~dex:13 ())
      ~proficiencies_gained:
        Proficiencies.(
          create
            ~armor:Armor.[ light ]
            ~weapons:
              Weapon.[ simple; custom "simple firearms"; custom "martial firearms" ]
            ~tools:[]
            ~skills:[])
  ;;

  let class_table =
    let risk_die = function
      | 1 -> int_to_string 0
      | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 -> "d8"
      | 10 | 11 | 12 | 13 | 14 | 15 | 16 -> "d10"
      | _ -> "d12"
    in
    let risk_die_qty = function
      | 1 -> 0
      | 2 | 3 | 4 | 5 -> 4
      | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 13 -> 5
      | _ -> 6
    in
    Class.Table.create
      ~header:[ "Risk Dice"; "Risk Die" ]
      ~rows:
        (List.init 20 ~f:(fun idx ->
             let idx = idx + 1 in
             [ risk_die_qty idx |> int_to_string; risk_die idx ]))
      ()
  ;;

  let features =
    let open Feature in
    let c n l b e = create ~name:n ~level:l ~class_:name ~gain_subclass:b ~entries:e () in
    List.map
      ~f:(fun (n, l, e) -> c n l false e)
      Entry.
        [ ( "Gunslinger"
          , 1
          , [ !"Gunslingers are those who have mastered a new and terrifying technology \
                -- firearms. Risk is in a gunslinger’s blood. They are bold renegades, \
                bucking tradition and forging a new path with the dangerous and \
                inelegant firearms. Gunslingers are infamous for surviving by their \
                wits, relying on split-second timing and a considerable amount of luck \
                to survive."
            ; !"A gunslinger’s explosive lifestyle lends well to wandering and \
                adventuring. Gunslingers will often shoot first and ask questions later, \
                an attitude which earns them few friends and bountiful enemies. In their \
                travels, most gunslingers are secretive and take lengths go unnoticed, \
                lest they be spotted by old foes with scores to settle."
            ] )
        ; ( "Fighting Style"
          , 1
          , [ !"You adopt a particular style of gunfighting as your specialty. Choose \
                one of the {@filter Gunslinger Fighting Styles|optionalfeatures|feature \
                type=gunslinger fighting style}. You can’t take a Fighting Style option \
                more than once, even if you later get to choose again"
            ; op [ "Akimbo|ALT"; "Bullseye|ALT"; "Precise|ALT"; "Scattershot|ALT" ]
            ] )
        ; ( "Quick Draw"
          , 1
          , [ !"Gunslingers have twitch reflexes and can pull a gun in the blink of an \
                eye. You have advantage on initiative rolls. Additionally, you can draw \
                or stow up to two weapons when you roll initiative and whenever you take \
                an action on your turn."
            ] )
        ; ( "Critical Shot"
          , 2
          , [ !"At 2nd level, your ranged firearm attacks score a critical hit on a roll \
                of 19 or 20."
            ] )
        ; ( "Critical Shot Improvement"
          , 9
          , [ !"At 9th level, your ranged firearm attacks score a critical hit on a roll \
                of 18 to 20."
            ] )
        ; ( "Critical Shot Mastery"
          , 17
          , [ !"At 17th level, your ranged firearm attacks score a critical hit on a \
                roll of 17 to 20."
            ] )
        ; ( "Poker Face"
          , 2
          , [ !"Starting at 2nd level, you have advantage on ability checks and saving \
                throws made to prevent others from sensing your motives, perceiving your \
                emotions, or reading your thoughts."
            ] )
        ; ( "Risk"
          , 2
          , [ !"By 2nd level, you can perform incredible feats of daring that are fueled \
                by special dice called risk dice."
            ; "Risk Dice"
              --> " You have four risk dice, which are d8s. You gain additional risk \
                   dice, and your risk dice change as you gain levels in this class, as \
                   shown in the Risk Dice and Risk Die columns of the Gunslinger table. \
                   You regain all expended risk dice when you take a long rest."
            ; "Using Risk Dice"
              --> "Once on each of your turns, you can expend a risk die to perform a \
                   {@filter Deed|optionalfeatures|feature type=deed} of your choice."
            ; "Saving Throws"
              --> "Some of your deeds require your target to make a saving throw to \
                   resist the deed’s effects. The saving throw DC is calculated as \
                   follows:"
            ; sd "Deed" Ability_name.dexterity
            ] )
        ; ( "Extra Attack"
          , 5
          , [ !"Beginning at 5th level, you can attack twice, instead of once, whenever \
                you take the Attack action on your turn."
            ] )
        ; ( "Gut Shot"
          , 6
          , [ !"Starting at 6th level, whenever you score a critical hit on a Large or \
                smaller creature using a firearm, the target of that attack is \
                incapacitated until the beginning of your next turn. Elementals, oozes, \
                and undead are immune to this effect. "
            ] )
        ; ( "Evasion"
          , 7
          , [ !"Beginning at 7th level, you can nimbly dodge out of the way of certain \
                area effects. When you are subjected to an effect that allows you to \
                make a Dexterity saving throw to take only half damage, you instead take \
                no damage if you succeed on the saving throw, and only half damage if \
                you fail. "
            ] )
        ; ( "Mankiller"
          , 11
          , [ !"At 11th level, when you take the Attack action on your turn, you can add \
                your ability score modifier to firearm damage rolls."
            ] )
        ; ( "Dire Gambit"
          , 13
          , [ !"Starting at 13th level, whenever you score a critical hit, you regain \
                one expended risk die."
            ] )
        ; ( "Cheat Death"
          , 15
          , [ !"By 15th level, you have a knack for escaping the reaper. When you would \
                be reduced to 0 hit points, you can use your reaction and expend one \
                risk die to avoid being incapacitated and instead be reduced to either \
                your gunslinger level or your hit points before the attack, whichever is \
                lower."
            ] )
        ; ( "Maverick"
          , 18
          , [ !"By 18th level, you are unshakable. You have advantage on Constitution \
                checks and saving throws."
            ] )
        ; ( "Headshot"
          , 20
          , [ !"At 20th level, when you score a critical hit against a creature using a \
                firearm, you can choose for this shot to be a head shot. If the creature \
                has less than 100 hit points, it dies. Otherwise, it takes {@dice 10d10} \
                additional damage. Elementals, oozes, undead, and creatures which lack \
                nervous systems or vital organs take no additional damage from this \
                ability"
            ; !"Once you use this ability, you must finish a short or long rest before \
                using it again."
            ] )
        ]
    @ List.map [ 4; 8; 12; 16; 19 ] ~f:(fun level ->
          let entries = ability_score level in
          c "Ability Score Improvement" level false entries)
    @
    let c n l e = c n l true e in
    Entry.
      [ c
          "Gunslinger's Creed"
          3
          [ !"By the time gunslingers reach 3rd level, they embrace a way of living, \
              known as their creed, which guides their judgments and their unique brand \
              of gunslinging. Your choice grants you features at 3rd level and again at \
              7th, 10th, and 14th level."
          ]
      ; c "Creed Feature" 7 [ !"At 7th level, you gain a feature granted by your Creed." ]
      ; c
          "Creed Feature"
          10
          [ !"At 10th level, you gain a feature granted by your Creed." ]
      ; c
          "Creed Feature"
          14
          [ !"At 14th level, you gain a feature granted by your Creed." ]
      ]
  ;;

  module Tank = struct
    let class_ = name
    let name = "Tank"
    let short_name = name

    let features =
      let open Subclass_feature in
      let c n l e = create ~name:n ~class_ ~subclass:short_name ~level:l ~entries:e () in
      List.map
        ~f:(fun (n, l, e) -> c n l e)
        Entry.
          [ ( "Creed: Tank"
            , 3
            , [ !"{@i Prerequisite: Strength score 15 or higher}"
              ; !"Be bigger, be badder, and be tougher, and no man will stand in your \
                  way. You are a living siege engine, a titan of muscle, brandishing \
                  weapons most men are incapable of lifting. Armed with these \
                  devastating firearms, you wade into the field of battle, bombarding \
                  and destroying those foolish enough to approach you"
              ] )
          ; ( "Heavy Gunner"
            , 3
            , [ !"When you select this creed at 3rd level, you can also carry heavy \
                  firearms regardless of their weight. Additionally, you can use \
                  Strength, rather than Dexterity, for attack and damage rolls using \
                  heavy firearms, and you can add your Strength instead of your \
                  Dexterity to your Deed Save DC."
              ] )
          ; ( "Tough as Nails"
            , 3
            , [ !"Starting at 3rd level, your hit point maximum increases by 1 and \
                  increases by 1 again whenever you gain a level in this class."
              ; !"You also gain proficiency with medium and heavy armor."
              ] )
          ; ( "Thick-Headed"
            , 7
            , [ !"At 7th level, you have advantage on saving throws you make against \
                  being charmed."
              ] )
          ; ( "Strong as an Ox"
            , 10
            , [ !"Starting at 10th level, your lifting and carrying capacities are \
                  doubled and you have advantage on Strength checks and Strength saving \
                  throws. Additionally, you can ignore the Two-Handed property on \
                  firearms with which you are proficient. "
              ] )
          ; ( "Gun Rage"
            , 14
            , [ !"At 14th level, as a bonus action, you can explode into a fury to \
                  destroy those who stand in your way. For the next minute, you have \
                  resistance to bludgeoning, piercing, and slashing damage and can make \
                  one additional attack when you take the Attack action on your turn. \
                  However, for the duration, your movement speed is halved and you have \
                  disadvantage on Dexterity checks and saving throws."
              ; !"Your gun rage ends if you use your bonus action to end it early, take \
                  cover, or finish your turn without attacking. Once you use this \
                  ability, you must finish a short or long rest before using it again."
              ] )
          ]
    ;;

    let subclass = Class.Subclass.create ~name ~short_name ~class_ ~features ()
  end

  module Bullet_ballet = struct
    let class_ = name
    let name = "Bullet Ballet"
    let short_name = name

    let features =
      let open Subclass_feature in
      let c n l e = create ~name:n ~class_ ~subclass:short_name ~level:l ~entries:e () in
      List.map
        ~f:(fun (n, l, e) -> c n l e)
        Entry.
          [ ( "Creed: Bullet Ballet"
            , 3
            , [ !"The ancient art of bullet ballet is passed through generations of \
                  gunslingers who study the gun as a perfect weapon and meditate on its \
                  intricacies. This path is not for the impatient or the faint of heart, \
                  but those who practice it diligently make their guns an extension of \
                  themselves, learning to strike with the speed of a rifle and catch \
                  bullets out of the air."
              ] )
          ; ( "Point Blank Shot"
            , 3
            , [ !"At 3rd level, you don't suffer disadvantage from making a ranged \
                  attack with a firearm while you are within 5 feet of a hostile \
                  creature."
              ] )
          ; ( "Bullet Ballet"
            , 3
            , [ !"Beginning at 3rd level, you learn the immortal art of bullet ballet, \
                  which sees the gun as a total weapon, as devastating used as a melee \
                  weapon in close-quarters as it is fired at range. You can treat \
                  firearms as melee weapons with the Finesse property that deal {@dice \
                  1d6} bludgeoning damage on a hit, or {@dice 1d8} damage if the firearm \
                  has the Two-Handed property"
              ; !"Additionally, when you make a ranged weapon attack using a firearm \
                  against a creature within 5 feet of you, you can make a melee weapon \
                  attack using that firearm as a bonus action."
              ] )
          ; ( "Lightning Disarm"
            , 7
            , [ !"Starting at 7th level, if a creature within 5 feet of you is holding a \
                  firearm, you can use your bonus action and expend a risk die to \
                  attempt to disarm them. The target must make a Dexterity saving throw \
                  against your Deed save DC. On a failed save, you take the firearm from \
                  the creature's hands after a series of rapid movements. You must have \
                  at least one empty hand to use this ability."
              ] )
          ; ( "Deflect Projectiles"
            , 7
            , [ !"Also at 7th level, you can use your reaction to deflect or catch a \
                  projectile when you are hit by a ranged weapon attack. When you do so, \
                  the damage you take from the attack is reduced by {@dice 1d10} + your \
                  Dexterity modifier + proficiency bonus."
              ] )
          ; ( "Predictive Dodge"
            , 10
            , [ !"At 10th level, your reflexes are so honed that you can dodge incoming \
                  bullets. You can use your bonus action to choose one creature that you \
                  can see within 30 feet of you. You gain the benefits of the Dodge \
                  action against the target's ranged attacks and effects until the \
                  beginning of your next turn. You lose this benefit if you take damage \
                  from the target."
              ] )
          ; ( "Gatling Strikes"
            , 14
            , [ !"By 14th level, you have mastered the most advanced techniques of \
                  bullet ballet. You can use your bonus action to make two melee weapon \
                  attacks and one ranged weapon attack using a firearm against targets \
                  within 15 feet of you. Once you use this ability, you can't use it \
                  again until you finish a short or long rest."
              ] )
          ]
    ;;

    let subclass = Class.Subclass.create ~name ~short_name ~class_ ~features ()
  end

  module Fortune's_favorite = struct
    let class_ = name
    let name = "Fortune's Favorite"
    let short_name = name

    let features =
      let open Subclass_feature in
      let c n l e = create ~name:n ~class_ ~subclass:short_name ~level:l ~entries:e () in
      List.map
        ~f:(fun (n, l, e) -> c n l e)
        Entry.
          [ ( "Creed: Fortune's Favorite"
            , 3
            , [ !"Sometimes, it's better to be lucky than good; in fact, some \
                  gunslingers find this is their defining characteristic. Lucky \
                  gunslingers engage in some spectacularly dangerous behavior, running \
                  in unawares, will shooting from the hip, and mouthing insults whenever \
                  they can, but somehow always come out unscathed. Fortune favors the \
                  bold, after all."
              ] )
          ; ( "Lady Luck's Favor"
            , 3
            , [ !"Starting when you choose this archetype at 3rd level, you can reroll \
                  on ability check, attack roll or saving throw. You choose to reroll \
                  after you roll the die, but before the outcome is determined. You gain \
                  the ability to use this ability an additional time at 7th level (2 \
                  times), 10th level (3 times), and 14th level (4 times) and regain all \
                  uses of this ability when you finish a long rest."
              ] )
          ; ( "Gambling Shot"
            , 3
            , [ !"At 3rd level, once per turn when you make a ranged attack roll with a \
                  firearm, you can chance a risky shot to perhaps deal a decisive blow. \
                  If you hit, roll for damage as normal and record the result, but do \
                  not deal this damage to the target. Guess if your damage will be \
                  higher or lower than the recorded number, then roll for damage again. \
                  If you guessed correctly, you can add your gunslinger level to the \
                  second damage roll, which is dealt to the target. If you guess \
                  incorrectly, you subtract your level from the damage roll (to a \
                  minimum of 1 damage). If you roll the same damage on the second damage \
                  roll, you add nothing to the damage roll."
              ] )
          ; ( "Stylish Reload"
            , 7
            , [ !"Starting at 7th level, whenever you spend a risk die, you can reload \
                  one firearm you're holding without using an action or bonus action. "
              ] )
          ; ( "Half Cocked"
            , 10
            , [ !"Starting at 10th level, you have advantage on attack rolls against any \
                  creature that hasn't taken a turn in the combat yet."
              ] )
          ; ( "One-in-a-Million Shot"
            , 14
            , [ !"By 14th level, you seem to have a knack for succeeding even when the \
                  odds are at their worst. When you hit with a ranged attack that has \
                  disadvantage, you can turn the attack into a critical hit. You can \
                  choose to give yourself disadvantage at any time."
              ; !"Once you use this ability, you can't use it again until you finish a \
                  short or long rest."
              ] )
          ]
    ;;

    let subclass = Class.Subclass.create ~name ~short_name ~class_ ~features ()
  end

  module Bulletstorm = struct
    let class_ = name
    let name = "Bulletstorm"
    let short_name = name

    let features =
      let open Subclass_feature in
      let c n l e = create ~name:n ~class_ ~subclass:short_name ~level:l ~entries:e () in
      List.map
        ~f:(fun (n, l, e) -> c n l e)
        Entry.
          [ ( "Creed: Bulletstorm"
            , 3
            , [ !"Bullets are power, and you have long believed that more bullets equal \
                  more power. Your expertise is in delivering a hail of deadly fire to \
                  pulverize your enemies. Not every bullet needs to be accurate to make \
                  a difference."
              ] )
          ; ( "Point Blank Shot"
            , 3
            , [ !"Starting at 3rd level, you don't suffer disadvantage from making a \
                  ranged attack with a firearm while you are within 5 feet of a hostile \
                  creature."
              ] )
          ; ( "Fan the Hammer"
            , 3
            , [ !"At 3rd level, when you take the Attack action on your turn to attack \
                  with a firearm, you can use your bonus action to make two additional \
                  firearm attacks with disadvantage. These additional attacks always \
                  have disadvantage, regardless of circumstance. This weapon can’t have \
                  the Two-Handed property."
              ] )
          ; ( "More Bullets"
            , 10
            , [ !"At 10th level, the number of bonus attacks you can make with the Fan \
                  the Hammer feature increases to 3 additional attacks instead of 2."
              ] )
          ; ( "Even More Bullets"
            , 14
            , [ !"At 14th level, the number of bonus attacks you can make with the Fan \
                  the Hammer feature increases to 4 additional attacks instead of 3."
              ] )
          ; ( "Speed Loader"
            , 7
            , [ !"Starting at 7th level, you have mastered the art of quickly reloading \
                  your weapon. On your turn, you can reload a single one-handed firearm \
                  without using an action or bonus action. "
              ] )
          ; ( "Bullet Time"
            , 10
            , [ !"At 10th level, when you make a firearm attack with a one-handed \
                  firearm on your turn, you can use your bonus action to gain advantage \
                  on the attack roll. Once you use this ability, you can’t use it again \
                  until you finish a short or long rest."
              ] )
          ; ( "Swift Vengeance"
            , 14
            , [ !"At 14th level, when you take damage from a creature within 15 feet of \
                  you, you can use your reaction to make a firearm attack targeting that \
                  creature. "
              ] )
          ]
    ;;

    let subclass = Class.Subclass.create ~name ~short_name ~class_ ~features ()
  end

  module Sharpshooter = struct
    let class_ = name
    let name = "Sharpshooter"
    let short_name = name

    let features =
      let open Subclass_feature in
      let c n l e = create ~name:n ~class_ ~subclass:short_name ~level:l ~entries:e () in
      List.map
        ~f:(fun (n, l, e) -> c n l e)
        Entry.
          [ ( "Creed: Sharpshooter"
            , 3
            , [ !"A well-placed bullet is more powerful than a sword, arrow, or spell. \
                  Indeed, you believe that every violent conflict should sound like a \
                  single loud crack followed by a long silence. Such shots need to be \
                  delivered perfectly, even at range, for when they are done right, they \
                  are as deadly for the target as they are stupendous for the audience"
              ] )
          ; ( "Aim"
            , 3
            , [ !"Starting at 3rd level, as a bonus action on your turn, you can take \
                  the Aim action. When you take this action, your next firearm attack \
                  suffers no penalty for long range, and ignores half and three-quarters \
                  cover."
              ] )
          ; ( "Eagle Eye"
            , 3
            , [ !"At 3rd level, you can see better from far away. You gain proficiency \
                  in in the {@skill Perception} skill, if you did not have it already, \
                  and you can add twice your proficiency modifier to Wisdom (Perception) \
                  checks you make that rely on sight."
              ] )
          ; ( "Camouflage"
            , 7
            , [ !"By 7th level, you’ve learned to expertly conceal yourself with your \
                  surroundings. You can spend one minute to prepare camouflage for \
                  yourself. Until you move, you have advantage on Dexterity ({@skill \
                  Stealth}) checks you make to hide."
              ] )
          ; ( "Utility Shot"
            , 10
            , [ !"At 10th level, you can quickly and precisely shoot objects. As an \
                  action, you can make a firearm attack targeting a Tiny object within \
                  the firearm’s range which isn’t being worn or carried, such as a rope, \
                  chain, belt, wooden rod, candle, bottle, or lock. This object is \
                  considered to have an AC of 10. On a hit, a nonmagical object is \
                  destroyed instantly and a magical object is flung 15 feet away."
              ] )
          ; ( "Focus"
            , 14
            , [ !"Starting at 14th level, whenever you take the Aim action on your turn \
                  followed by the Attack action, you can choose to make only one firearm \
                  attack. This attack is made with advantage and deals critical damage \
                  on a hit."
              ] )
          ]
    ;;

    let subclass = Class.Subclass.create ~name ~short_name ~class_ ~features ()
  end

  module Trickshot = struct
    let class_ = name
    let name = "Trickshot"
    let short_name = name

    let features =
      let open Subclass_feature in
      let c n l e = create ~name:n ~class_ ~subclass:short_name ~level:l ~entries:e () in
      List.map
        ~f:(fun (n, l, e) -> c n l e)
        Entry.
          [ ( "Creed: Trickshot"
            , 3
            , [ !"Accuracy means different things to different people. For you, true \
                  accuracy isn’t necessarily in hitting a target on the first shot, but \
                  might include hitting the mark after the bullet bounces around a dozen \
                  times. Your attacks are just as dangerous if they miss, or even after \
                  hitting their mark, as others are while they’re still in the air."
              ] )
          ; ( "Creative Trajectory"
            , 3
            , [ !"Starting at 3rd level you begin to find ways to make your bullets \
                  travel in unexpected ways. Your firearm attacks ignore half cover."
              ] )
          ; ( "Ricochet"
            , 3
            , [ !"At 3rd level when you miss with an attack using a firearm and you do \
                  not have disadvantage on the roll, you can use your bonus action to \
                  reroll the attack roll and you must use the new roll. If this causes \
                  the attack to hit, the attack only deals only half damage."
              ] )
          ; ( "Lightfooted"
            , 7
            , [ !"By 7th level, you're just as good at escaping from trouble as you are \
                  getting in to it. You ignore difficult terrain."
              ] )
          ; ( "Deft Deflectionist"
            , 10
            , [ !"Starting at 10th level, as a reaction when an attacker you can see \
                  makes a ranged attack roll against you, you can fire a firearm you are \
                  holding to deflect the strike. Make an attack roll. If your attack \
                  roll is higher than the attacker's, the attack targeting you has \
                  disadvantage. You must use this feature before you know the outcome of \
                  the roll."
              ] )
          ; ( "Pinball Shot"
            , 14
            , [ !"Starting at 14th level, when you hit a creature with a ranged firearm \
                  attack, once per turn you can have the bullet ricochet to hit an \
                  additional target. The second target must be within half the firearm's \
                  range of the first target. Make a separate attack roll for the second \
                  target. You can use this ability a number of times equal to your \
                  Dexterity modifier, and regain all expended uses when you finish a \
                  long rest."
              ] )
          ]
    ;;

    let subclass = Class.Subclass.create ~name ~short_name ~class_ ~features ()
  end

  module Twice_damned = struct
    let class_ = name
    let name = "Twice-Damned"
    let short_name = name

    let features =
      let open Subclass_feature in
      let c n l e = create ~name:n ~class_ ~subclass:short_name ~level:l ~entries:e () in
      List.map
        ~f:(fun (n, l, e) -> c n l e)
        Entry.
          [ ( "Creed: Twice-Damned"
            , 3
            , [ !"Someone wronged you, and you refuse to die before you've shot them \
                  dead. You believe that the gun is the ultimate tool of vengeance, the \
                  only means to redress the balance of blood. Maybe you were robbed, \
                  humiliated, or left for dead; maybe you were even killed, but were \
                  later returned to life with hate overflowing your heart. Regardless of \
                  how you were first damned, you'll be damned twice before you let them \
                  get away with it. "
              ] )
          ; ( "Black Bullet"
            , 3
            , [ !"Starting at 3rd level, whenever you deal damage to a creature using a \
                  ranged firearm attack, the creature's hit point maximum is reduced for \
                  1 hour by an amount equal to the damage dealt. Any effect that removes \
                  a disease allows a creature's hit point maximum to return to normal \
                  before that time passes."
              ; !"Additionally, when you deal damage with a firearm to a creature that \
                  has less than half its maximum hit points, you can use your bonus \
                  action to deal an additional {@dice 1d6} necrotic damage."
              ] )
          ; ( "Vengeful Black Bullet"
            , 10
            , [ !"The hit point reduction of your Black Bullet lasts until the target \
                  takes a long rest, instead of one hour. In addition, the extra damage \
                  done to a creature with half health increases to {@dice 2d6}."
              ] )
          ; ( "Hateful Vengeance"
            , 3
            , [ !"When you adopt this creed at 3rd level, choose a specific creature \
                  that has wronged you that you wish to seek revenge against. You must \
                  know this creature's name or must otherwise have enough information to \
                  identify it. Your attack rolls against this creature have advantage \
                  and deal maximum damage."
              ; !"Once you have chosen a creature to take revenge against, you can only \
                  change this selection if you learn that the creature you have chosen \
                  was innocent of any wrongdoing against you and that another creature \
                  was instead responsible."
              ] )
          ; ( "Grim Determination"
            , 7
            , [ !"By 7th level, nothing shakes your resolve. You have advantage on \
                  saving throws against being frightened"
              ] )
          ; ( "Forceful Interrogation"
            , 10
            , [ !"Starting at 10th level, with a cold glare and the barrel of a gun in \
                  someone's face, you can always get the answers you want to hear. You \
                  have advantage on Charisma ({@skill Intimidation}) checks to make \
                  while holding a firearm, and you can treat a roll of 10 or lower as a \
                  10."
              ] )
          ; ( "Marked for Death"
            , 14
            , [ !"By 14th level, you feel an adrenaline burst whenever you exact \
                  vengeance. When you roll initiative, choose one creature you can see \
                  within 60 feet of you. If you reduce that creature to 0 hit points on \
                  your turn within the next minute, you gain one additional action, \
                  which can be used to take the Attack, Dash, Dodge, Disengage, or Hide \
                  action."
              ] )
          ]
    ;;

    let subclass = Class.Subclass.create ~name ~short_name ~class_ ~features ()
  end

  let class_ =
    Class.create
      ~name
      ~subclass_name
      ~hit_die
      ~proficiency
      ~starting_proficiencies
      ~starting_equipment
      ~multiclassing
      ~class_table
      ~features
      ()
  ;;

  let subclasses =
    [ Tank.subclass
    ; Bullet_ballet.subclass
    ; Fortune's_favorite.subclass
    ; Bulletstorm.subclass
    ; Sharpshooter.subclass
    ; Trickshot.subclass
    ; Twice_damned.subclass
    ]
  ;;

  let subclass_features =
    List.concat
      [ Tank.features
      ; Bullet_ballet.features
      ; Fortune's_favorite.features
      ; Bulletstorm.features
      ; Sharpshooter.features
      ; Trickshot.features
      ; Twice_damned.features
      ]
  ;;
end

module Martyr = struct
  let name = "Martyr"
  let subclass_name = "Mortal Burden"
  let hit_die = hit_die 12
  let proficiency = Ability_name.[ strength; wisdom ]

  let starting_proficiencies =
    Proficiencies.(
      create
        ~armor:Armor.[ light; medium; shield ]
        ~weapons:Weapon.[ simple; martial ]
        ~tools:[]
        ~skills:
          Skill.
            [ choose
                ( 2
                , [ athletics
                  ; history
                  ; insight
                  ; intimidation
                  ; medicine
                  ; persuasion
                  ; religion
                  ] )
            ])
  ;;

  let starting_equipment =
    Equipment.of_string_list
      [ "(a) a martial weapon and a shield or (b) two martial weapons"
      ; "(a) scale mail or (b) chain mail (if proficient)"
      ; "(a) a light crossbow and 20 bolts, or (b) any simple weapon"
      ; "(a) a priest’s pack or (b) an explorer’s pack"
      ; "A holy symbol"
      ]
  ;;

  let multiclassing =
    Class.Multiclass.create
      ~requirements:(Ability.create ~con:13 ~wis:13 ())
      ~proficiencies_gained:
        Proficiencies.(
          create
            ~armor:Armor.[ light; medium; shield ]
            ~weapons:Weapon.[ simple; martial ]
            ~tools:[]
            ~skills:[])
  ;;

  let class_table =
    let spell_level = function
      | 1 -> int_to_string 0
      | 2 | 3 | 4 -> "1st"
      | 5 | 6 | 7 | 8 -> "2nd"
      | 9 | 10 | 11 | 12 -> "3rd"
      | 13 | 14 | 15 | 16 -> "4th"
      | _ -> "5th"
    in
    let spell_uses = function
      | 1 -> 0
      | 2 -> 2
      | 3 | 4 -> 3
      | 5 | 6 -> 6
      | 7 | 8 -> 7
      | 9 | 10 -> 9
      | 11 | 12 -> 10
      | 13 | 14 -> 11
      | 15 | 16 -> 12
      | 17 | 18 -> 14
      | _ -> 15
    in
    Class.Table.create
      ~header:[ "Max Spell Level"; "Spell Uses" ]
      ~rows:
        (List.init 20 ~f:(fun idx ->
             let idx = idx + 1 in
             [ spell_level idx; spell_uses idx |> int_to_string ]))
      ()
  ;;

  let features =
    let open Feature in
    let c n l b e = create ~name:n ~level:l ~class_:name ~gain_subclass:b ~entries:e () in
    List.map
      ~f:(fun (n, l, e) -> c n l false e)
      Entry.
        [ ( "Martyr"
          , 1
          , [ !"Martyrs are chosen by a divine force, predestined to speed forth to a \
                final destination: they are ordained to die for a great cause. Martyrs \
                are prophets and oracles, great priests and liberators. No matter how \
                much good they might accomplish in their lives, the end must always be \
                the same: a glorious death, to be remembered in legend."
            ; !"As martyrs are ordained by prophesy to die, their suffering is holy \
                sacrament. Martyrs brought close to death grow more powerful, not less, \
                and those that spill their own blood evoke waves of divine energy. In \
                the view of the divine, such hardship only heightens martyrdom and \
                hastens the martyr to their destiny."
            ] )
        ; ( "Ordained Death"
          , 1
          , [ !"As a martyr, you have been predestined by something greater to perish \
                for a great cause; there is no greater glory in death, and no greater \
                joy in the afterlife. However, it is not yet your time. Starting at 1st \
                level, when you fall to 0 hit points and begin to make death saving \
                throws, you must fail 5 saving throws to die. Additionally, if a spell \
                has the sole effect of restoring you to life (but not undeath), the \
                caster doesn’t need material components to cast the spell on you.."
            ] )
        ; ( "Mark of the Herald"
          , 2
          , [ !"At 2nd level, your divine cause manifests itself upon you, forming a \
                special mark or stigmata for all to see. You have advantage on checks \
                you make to convince others that you are an ordained martyr in service \
                of a holy cause."
            ] )
        ; ( "Spellcasting"
          , 2
          , [ !"Starting at 2nd level, you can leverage the power of your suffering to \
                cast divine magic."
            ; ne
                "Casting Spells"
                [ !"To cast one of your martyr spells of 1st level or higher, you must \
                    spend your own hit points to provide the power to manifest a spell. \
                    The number of hit points is listed on the Hit Points Spellcasting \
                    table and can’t be reduced or avoided. You don’t make Constitution \
                    saving throws to maintain concentration on spells as a result of \
                    losing these hit points."
                ; !"The Martyr table shows the maximum level of spell slot you can \
                    create."
                ; !"The Spell Uses column of the Martyr table shows how many spells you \
                    can cast. When you expend hit points to cast a spell, you expend one \
                    of these uses. You regain all expended uses when you finish a long \
                    rest."
                ; tbl
                    ~title:"Hit Points Spellcasting"
                    ~labels:[ "Spell Level"; "Damage" ]
                    ~style:[ ""; "" ]
                    ~rows:
                      [ [ "1st"; "5" ]
                      ; [ "2nd"; "10" ]
                      ; [ "3rd"; "20" ]
                      ; [ "4th"; "30" ]
                      ; [ "5th"; "45" ]
                      ]
                ]
            ; "Preparing Spells"
              --> "You prepare the list of martyr spells that are available for you to \
                   cast, choosing from the martyr spell list. When you do so, choose a \
                   number of martyr spells equal to your Wisdom modifier + half your \
                   martyr level, rounded down (minimum of one spell). Spells you choose \
                   must be of a level no higher than what’s shown on the Max Spell Level \
                   column for your level. You can change your list of prepared spells \
                   when you finish a long rest."
            ; "Healing Magic"
              --> "Because your power is derived from mortal suffering, you can’t regain \
                   hit points from any spell you cast."
            ; ne
                "Spellcasting Ability"
                [ !"Wisdom is your spellcasting modifier for martyr spells, since your \
                    power originates in the devotion used overcome your trials and \
                    tribulations. You use your Wisdom whenever a spell refers to your \
                    spellcasting ability. In addition, you use your Wisdom modifier when \
                    setting the saving throw DC for a martyr spell you cast and when \
                    making an attack roll with one."
                ; sd "Spell" Ability_name.wisdom
                ; sa "Spell" Ability_name.wisdom
                ]
            ; "Spellcasting Focus"
              --> "You can use a holy symbol as a spellcasting focus for your martyr \
                   spells."
            ] )
        ; ( "Sainted Reprisal"
          , 2
          , [ !"Also at 2nd level, you can reprimand those who draw your blood. When a \
                creature you can see within 5 feet of you hits you with a melee attack, \
                you can use your reaction to deal that creature 1d6 necrotic or radiant \
                damage (your choice)."
            ] )
        ; ( "Great Sainted Reprisal"
          , 5
          , [ !"At 5th level your Sainted Reprisal deals 2d6 necrotic or radiant damage \
                instead of 1d6."
            ] )
        ; ( "Greater Sainted Reprisal"
          , 11
          , [ !"At 11th level your Sainted Reprisal deals 3d6 necrotic or radiant damage \
                instead of 2d6."
            ] )
        ; ( "Greatest Sainted Reprisal"
          , 17
          , [ !"At 17th level your Sainted Reprisal deals 4d6 necrotic or radiant damage \
                instead of 3d6."
            ] )
        ; ( "Divine Healing"
          , 3
          , [ !"Starting at 3rd level, you can utter a prayer to the gods for mercy. As \
                an action, you can spend Hit Dice and regain hit points as if you had \
                just finished a short rest. When you use this ability, you can spend a \
                number of Hit Dice up to your proficiency bonus."
            ] )
        ; ( "Torment"
          , 3
          , [ !"By 3rd level, you have learned to curry the gods’ favor through anguish \
                and mortal trials. Once on each of your turns when you hit a creature \
                with a melee weapon attack, you can lose 5 hit points, which can’t be \
                reduced or avoided, to deal a bonus +10 bonus necrotic or radiant damage \
                (your choice) to the target. You don’t make Constitution saving throws \
                to maintain concentration on spells as a result of losing these hit \
                points."
            ] )
        ; ( "Anguish"
          , 11
          , [ !"At 11th level you may choose to lose 10 hit points instead of 5 for your \
                Torment feature to deal an additional +20 bonus damage instead of +10."
            ] )
        ; ( "Extra Attack"
          , 5
          , [ !"Beginning at 5th level, you can attack twice, instead of once, whenever \
                you take the Attack action on your turn."
            ] )
        ; ( "Respite"
          , 7
          , [ !"Beginning at 7th level, you regain all spent Hit Dice when you finish a \
                long rest, instead of only half of them."
            ] )
        ; ( "Undying Conviction"
          , 10
          , [ !"Beginning at 10th level, when you are reduced to 0 hit points and are \
                not killed outright, you can choose to drop to 1 hit point instead. Once \
                you use this ability, you can’t use it again until you finish a long \
                rest."
            ] )
        ; ( "March Unto Destiny"
          , 15
          , [ !"At 15th level, your inevitable end draws nearer, and nothing can hold \
                you from it. You do not need to eat or drink, and cannot be paralyzed, \
                petrified, or stunned."
            ] )
        ; ( "Final Martyrdom"
          , 20
          , [ !"At 20th level, you have at last reached your predestination: you will \
                die in eternal glory. You can use your action to become immune to all \
                damage for 10 minutes. During this duration, you can’t be blinded, \
                charmed, deafened, exhausted, frightened, incapacitated, poisoned, \
                restrained, or rendered unconscious. You have advantage on all ability \
                checks, attack rolls, and saving throws."
            ; !"Additionally, during this duration, you can cast the {@spell wish} spell \
                once, without spending a spell slot or hit points. If you use the spell \
                produce any effect other than duplicating another spell, the stress of \
                casting it doesn’t reduce your Strength or cause you to take necrotic \
                damage."
            ; !"At the end of this duration, you die. No force, short of divine \
                intervention can prevent your death, and you can’t be returned to life \
                by any means."
            ] )
        ]
    @ List.map [ 4; 8; 12; 16; 19 ] ~f:(fun level ->
          let entries = ability_score level in
          c "Ability Score Improvement" level false entries)
    @
    let c n l e = c n l true e in
    Entry.
      [ c
          "Martyr's Mortal Burden"
          1
          [ !"Beginning at 1st level, you are fated to perish in the name of a great \
              ideal, cementing your name alongside others who have done the same. Choose \
              a Mortal Burden from one of the buttons above."
          ; !"Your choice grants you features at 1st level, and again at 6th, 14th, and \
              18th level."
          ; "Burden Spells"
            <-> [ "Each burden has a list of associated spells that you gain at the \
                   martyr levels specified in the burden description. Once you gain \
                   access to a burden spell, you always have it prepared, and it doesn’t \
                   count against the number of spells you can prepare each day."
                ; "If you gain a burden spell that doesn’t appear on the martyr spell \
                   list, the spell is nonetheless a martyr spell for you."
                ]
          ]
      ; c
          "Mortal Burden Feature"
          6
          [ !"At 6th level, you gain a feature granted by your Burden." ]
      ; c
          "Mortal Burden Feature"
          14
          [ !"At 14th level, you gain a feature granted by your Burden." ]
      ; c
          "Mortal Burden Feature"
          18
          [ !"At 18th level, you gain a feature granted by your Burden." ]
      ]
  ;;

  module Atonement = struct
    let class_ = name
    let name = "Atonement"
    let short_name = name

    let features =
      let open Subclass_feature in
      let c n l e = create ~name:n ~class_ ~subclass:short_name ~level:l ~entries:e () in
      List.map
        ~f:(fun (n, l, e) -> c n l e)
        Entry.
          [ ( "Creed: Atonement"
            , 1
            , [ !"For all the wrong you have done in your life, the gods have graced you \
                  with an undeserved second chance. Your burden is to atone, to undo all \
                  the evils you have done in your life, and to seek redemption before \
                  the gods. How you accomplish this is your choice, so long as you \
                  remove any dark marks you leave behind. Ultimately, your final \
                  martyrdom might be a grand act, averting a great disaster or banishing \
                  a force of evil, or it might be as humble as redirecting the life of \
                  an evil person and causing them to seek redemption, passing the torch \
                  to a new martyr of atonement."
              ] )
          ; ( "Burden Spells"
            , 3
            , [ !"Starting at 3rd level, whenever you deal damage to a creature using a \
                  ranged firearm attack, the creature's hit point maximum is reduced for \
                  1 hour by an amount equal to the damage dealt. Any effect that removes \
                  a disease allows a creature's hit point maximum to return to normal \
                  before that time passes."
              ; tbl
                  ~title:"Burden Spells"
                  ~labels:[ "Martyr Level"; "Spells" ]
                  ~style:[ ""; "" ]
                  ~rows:
                    (List.map
                       [ 3, [ "cure wounds"; "sanctuary" ]
                       ; 5, [ "calm emotions"; "lesser restoration" ]
                       ; 9, [ "remove curse"; "speak with dead" ]
                       ; 13, [ "death ward"; "fire shield" ]
                       ; 17, [ "greater restoration"; "mass cure wounds" ]
                       ]
                       ~f:(fun (level, spells) ->
                         Int.to_string level
                         :: List.map spells ~f:(fun spell ->
                                [%string "{@spell %{spell}}"])))
              ] )
          ; ( "Bonus Proficiencies"
            , 1
            , [ !"Starting at 1st level, you gain proficiency in heavy armor. " ] )
          ; ( "Self-Sacrifice"
            , 1
            , [ !"Also at 1st level, you can dive in the way of an attack. When a \
                  creature you can see attacks a target other than you that is within 5 \
                  feet of you, you can use your reaction to change the target of the \
                  attack to yourself instead."
              ; !"Once you use this ability, you can’t use it again until you finish a \
                  short or long rest."
              ] )
          ; ( "Blooded Reprieve"
            , 6
            , [ !"Starting at 6th level, whenever you use your Torment feature against a \
                  hostile creature and you reduce the target to 0 hit points, you lose \
                  no hit points from using the ability."
              ] )
          ; ( "Draw Aggression"
            , 14
            , [ !"Beginning at 14th level, as a bonus action on your turn, you can \
                  invoke the ire of your foes to protect your allies. Until the \
                  beginning of your next turn, each hostile creature you choose within 5 \
                  feet of you that can see you has disadvantage on attack rolls against \
                  creatures other than you, and advantage on attack rolls against you."
              ] )
          ; ( "Sin Eater"
            , 18
            , [ !"Starting at 18th level, you can use your action to transfer negative \
                  effects from any number of willing creatures within 60 feet to \
                  yourself. You can transfer one of the following effects from each \
                  creature:"
              ; lh
                  [ !!"One level of exhaustion"
                  ; !!"One disease or poison"
                  ; !!"One curse, including the target’s attunement to a cursed magic \
                       item"
                  ; !!"Any reduction to one of the target’s ability scores"
                  ; !!"One effect reducing the target’s hit point maximum"
                  ; !!"The blinded, charmed, deafened, paralyzed, petrified, or poisoned \
                       condition"
                  ]
              ; !"The effect ends for the creature and targets you as if you were the \
                  original target, with the effect’s original duration. You can transfer \
                  multiple negative effects of the same type to yourself, affecting you \
                  only once each time you use this ability."
              ] )
          ]
    ;;

    let subclass = Class.Subclass.create ~name ~short_name ~class_ ~features ()
  end

  let class_ =
    Class.create
      ~name
      ~subclass_name
      ~hit_die
      ~proficiency
      ~starting_proficiencies
      ~starting_equipment
      ~multiclassing
      ~class_table
      ~features
      ()
  ;;

  let subclasses = [ Atonement.subclass ]
  let subclass_features = List.concat [ Atonement.features ]
end

module Warlock = struct
  let name = "Warlock"

  module Seeker = struct
    let class_ = name
    let name = "Seeker"
    let short_name = name

    let features =
      let open Subclass_feature in
      let c n l e =
        create
          ~name:n
          ~class_
          ~class_source:"PHB"
          ~subclass:short_name
          ~level:l
          ~entries:e
          ()
      in
      List.map
        ~f:(fun (n, l, e) -> c n l e)
        Entry.
          [ ( "The Seeker"
            , 1
            , [ !"Your patron is an inscrutable being who travels the Realms and the \
                  World Beyond in search of knowledge and secrets. In return for your \
                  patron's gifts, you wander the world seeking lore, information, \
                  relics, artifacts, and other items or knowledge that you can share \
                  with the Seeker."
              ] )
          ; ( "Expanded Spell List"
            , 1
            , [ !"The Seeker lets you choose from an expanded list of spells when you \
                  learn a warlock spell. The following spells are added to the warlock \
                  spell list for you."
              ; tbl
                  ~title:"Seeker Expanded Spells"
                  ~labels:[ "Spell Level"; "Spells" ]
                  ~style:[ ""; "" ]
                  ~rows:
                    (List.map
                       [ 1, [ "feather fall"; "jump" ]
                       ; 2, [ "levitate"; "locate object" ]
                       ; 3, [ "clairvoyance"; "sending" ]
                       ; 4, [ "arcane eye"; "locate creature" ]
                       ; 5, [ "legend lore"; "passwall" ]
                       ]
                       ~f:(fun (level, spells) ->
                         Int.to_string level
                         :: List.map spells ~f:(fun spell ->
                                [%string "{@spell %{spell}}"])))
              ] )
          ; ( "Student of Lore"
            , 1
            , [ !"When you make your pact with the Seeker at 1st level, they impart to \
                  you a mote of their immense knowledge. Your spellcasting ability for \
                  warlock spells and class features becomes Intelligence, and anytime \
                  you gain a warlock feature that is Charisma-based, it becomes \
                  Intelligence-based for you."
              ; !"In addition, you gain proficiency in the Arcana skill, and you learn \
                  one cantrip of your choice from the wizard spell list. It counts as a \
                  warlock cantrip for you, but doesn't count against your total number \
                  of Cantrips Known."
              ] )
          ; ( "Astral Slip"
            , 6
            , [ !"You can draw on your patron's knowledge of the realms to save you from \
                  harm. Beginning at 6th level, when you are hit by an attack, you can \
                  use your reaction to instantly slip between the realms, causing the \
                  triggering attack to miss. You then immediately reappear in an \
                  unoccupied space of your choice within 30 feet of where you \
                  disappeared."
              ; !"Once you use this ability, you can’t use it again until you finish a \
                  short or long rest."
              ] )
          ; ( "Arcane Savant"
            , 6
            , [ !"Your patron grants you enhanced insight into arcane magic. Also at 6th \
                  level, each time you finish a long rest you can replace one warlock \
                  spell you know of 1st-level or higher with another warlock spell of \
                  your choice."
              ] )
          ; ( "Spell Ward"
            , 10
            , [ !"Your knowledge of the aether allows you to resist arcane effects. At \
                  10th level, you gain resistance to force damage, and when you are \
                  forced to make a saving throw to resist the effects of a spell, you \
                  can use your reaction to make an Intelligence saving throw in place of \
                  the normal saving throw."
              ; !"You can use this reaction a number of times equal to your Intelligence \
                  modifier (minimum of once), and you regain all expended uses when you \
                  finish a long rest."
              ] )
          ; ( "Shunt"
            , 14
            , [ !"Starting at 14th level, when you hit a creature with an attack, you \
                  can force them to make a Charisma saving throw in place of dealing \
                  damage. On a failed save, the target is instantly transported to a \
                  great library in another realm for up to 1 minute, or until you lose \
                  concentration on this ability."
              ; !"When this feature ends, the banished creature returns to the space \
                  that it was previously occupying (or the nearest unoccupied space) and \
                  must make an additional Charisma saving throw, taking 5d10 force \
                  damage on a failed save."
              ; !"Once you use this feature you must finish a short or long rest before \
                  you can use it again."
              ] )
          ]
    ;;

    let subclass =
      Class.Subclass.create ~name ~short_name ~class_ ~class_source:"PHB" ~features ()
    ;;
  end

  let subclasses = [ Seeker.subclass ]
  let subclass_features = List.concat [ Seeker.features ]
end

let class_json =
  `List
    ([ Binder.class_; Hedge_mage.class_; Gunslinger.class_; Martyr.class_ ]
    |> List.sort ~compare:[%compare: Class.t]
    |> List.map ~f:Class.to_json)
;;

let class_feature_json =
  `List
    ([ Binder.features; Hedge_mage.features; Gunslinger.features; Martyr.features ]
    |> List.concat
    |> List.sort ~compare:[%compare: Feature.t]
    |> List.map ~f:Feature.to_json)
;;

let subclass_json =
  `List
    ([ Binder.subclasses
     ; Hedge_mage.subclasses
     ; Gunslinger.subclasses
     ; Martyr.subclasses
     ; Warlock.subclasses
     ]
    |> List.concat
    |> List.sort ~compare:[%compare: Class.Subclass.t]
    |> List.map ~f:Class.Subclass.to_json)
;;

let subclass_feature_json =
  `List
    ([ Binder.subclass_features
     ; Hedge_mage.subclass_features
     ; Gunslinger.subclass_features
     ; Martyr.subclass_features
     ; Warlock.subclass_features
     ]
    |> List.concat
    |> List.sort ~compare:[%compare: Subclass_feature.t]
    |> List.map ~f:Subclass_feature.to_json)
;;