open! Core
open! Async
open! Gen_lib

module Deva = struct
  let create ~name ~ability ~entries =
    let name = [%string "Deva (%{name})"] in
    let size = Size.medium in
    let speed = Speed.create 30 () in
    let creature_type = Creature_type.celestial in
    let height_and_weight = Height_and_weight.create ~h:70 ~mh:"1d6" ~w:110 ~mw:"2d4" in
    let languages = Language.[ common; celestial; any 1 ] in
    let info =
      Entry.(
        info
          ~name:"Deva"
          ~title:"Immortal Spirits in Mortal Bodies"
          ~intro:
            "Deep in the recesses of memory, deva recall what they once were: immortal \
             servitors of the realms, spirits who chose to bind themselves to the world \
             in mortal flesh. For millennia, their souls have been reborn to wage an \
             endless war in the name of their Aspect."
          ~physical_qualities:
            [ !"In appearance, deva are very similar to humans, but with an unearthly \
                beauty and an uncanny stillness about them."
            ; !"Deva's coloration distinguishes them most strongly from humans. All deva \
                have patterns of light and dark colors on their skin. The light portions \
                are chalk white or pale gray, and the dark areas range from blue or \
                purple to dark gray or black.In any individual deva, either light or \
                dark can be dominant, with the opposite shade appearing in simple, \
                elegant patterns on the face, chest, and shoulders. A deva's hair is \
                typically the same color as these skin patterns."
            ; !"When sitting or standing at rest, deva remain almost perfectly still, \
                except for the occasional blink of the eyes. They don't fidget or \
                twitch, and their eyes move only when they are actively examining \
                something."
            ; !"Deva do not have children. When a deva dies, their spirit is \
                reincarnated in a new, adult body that appears in some sacred place. The \
                new deva can recall enough memory of past lives to function in society, \
                but only with some effort."
            ]
          ~playstyle:
            [ !"Deva are refined and polite. They follow strict moral standards, but \
                they are not afraid of violence. Because they remember, at least dimly, \
                a life on their original realm, most devas are devout worshipers of \
                their respective temple. Deva seek to achieve a personal connection with \
                their realm rather than approach them through priests. Deva adventurers \
                are commonly clerics, paladins, and warlocks, who savor the experience \
                of divine power flowing through them as a conduit."
            ; !"Deva do not have cities or societies of their own, and their numbers are \
                so small that a deva can spend entire lifetimes without ever meeting \
                another of their kind. They live among other races and, at least to some \
                extent, adopt their ways. However, all devas remember of the life they \
                had before their incarnation in flesh and the beginning of their cycle \
                of rebirth, and they share some common cultural elements of dress and \
                habits. Deva favor flowing clothes of fine silks, polished metal armor \
                with winglike shoulder ornaments, and headdresses or helmets that \
                suggest crowns or halos."
            ])
    in
    let traits =
      Description.(
        standard
          ~age:
            "Deva are Manifested fully formed as an adults, but only maintain their \
             corporeal form for up to 20 years."
          ~creature_type:Creature_type.celestial
          ~size:"Deva have the same range of height and weight as humans."
        @ [ "Darkvision"
            --> "Blessed with a radiant soul, your vision can easily cut through \
                 darkness. You can see in dim light within 60 feet of you as if it were \
                 bright light, and in darkness as if it were dim light. You can't \
                 discern color in darkness, only shades of gray."
          ; "Celestial Resistance"
            --> "You have resistance to necrotic damage and radiant damage."
          ; "Memory of a Thousand Lifetimes"
            <-> [ "The dreamlike memories of your previous lives lend insight to aid \
                   you. When you make an attack roll, saving throw, or ability check and \
                   dislike the result, you may add 1d6 to the roll before you learn the \
                   result."
                ; "You can use this feature a number of times equal to your Wisdom \
                   modifier (a minimum of once). You regain any expended uses when you \
                   finish a long rest."
                ]
          ; "Light Bearer"
            --> "You know the {@spell light} cantrip. Charisma is your spellcasting \
                 ability for it."
          ]
        @ entries)
    in
    Race.create
      ~name
      ~size
      ~speed
      ~ability
      ~creature_type
      ~height_and_weight
      ~languages
      ~info
      ~traits
  ;;

  let beacon =
    create
      ~name:"Beacon"
      ~ability:(Ability.create ~wis:2 ~con:1 ())
      ~entries:
        Description.
          [ "Ancient Power"
            <-> [ "Starting at 3rd level, you can use your action to unleash the divine \
                   energy within yourself, drawing upon the vast stores of energy in \
                   your realm."
                ; "For one minute or until you choose to end the effect as a bonus \
                   action, you gain a number of temporary hit points equal to half your \
                   level at the start of each of your turns."
                ; "In addition, once on each of your turns, when you deal damage to a \
                   target with an attack or a spell, you can deal extra radiant damage \
                   to the target equal to half your level."
                ; "Once you use this trait, you can't use it again until you finish a \
                   long rest."
                ]
          ]
  ;;

  let tinder =
    create
      ~name:"Tinder"
      ~ability:(Ability.create ~wis:2 ~int_:1 ())
      ~entries:
        Description.
          [ "Radiant Consumption"
            <-> [ "Starting at 3rd level, you can use your action to unleash the divine \
                   energy within yourself, causing your eyes to burn like coals and your \
                   veins to glow with red energy."
                ; "Your transformation lasts for 1 minute or until you end it as a bonus \
                   action. During it, you shed bright light in a 10-foot radius and dim \
                   light for an additional 10 feet, and you add your Intelligence \
                   modifier to any fire damage you deal with spells you cast. Also, once \
                   before your transformation ends, you may cast the {@spell \
                   Pyrotechnics|xge} spell without expending a spell slot."
                ; "Once you use this trait, you can't use it again until you finish a \
                   long rest."
                ]
          ]
  ;;

  let commander =
    create
      ~name:"Commander"
      ~ability:(Ability.create ~wis:2 ~cha:1 ())
      ~entries:
        Description.
          [ "Overriding Force"
            <-> [ "Starting at 3rd level, you can use your action to unleash the divine \
                   energy within yourself, granting you an authoritative and oppressive \
                   aura."
                ; "Your transformation lasts for 1 minute or until you end it as a bonus \
                   action. The instant you transform, other creatures within 60 feet of \
                   you that can see you or hear you must succeed on a Wisdom saving \
                   throw (DC 8 + your proficiency bonus + your Wisdom modifier) or \
                   become charmed by you until the end of your transformation. You can \
                   target a maximum number of creatures equal to your Wisdom modifier. \
                   If you or your allies attack the creatures charmed in this way, they \
                   can repeat the saving throw, ending the effect on a success."
                ; "Once you use this trait, you can't use it again until you finish a \
                   long rest."
                ]
          ]
  ;;

  let race = [ beacon; tinder; commander ]
end

module Shade = struct
  let create ~name ~speed ~ability ~entries =
    let name = [%string "Shade (%{name})"] in
    let size = Size.medium in
    let creature_type = Creature_type.aberration in
    let height_and_weight = Height_and_weight.create ~h:56 ~mh:"2d10" ~w:110 ~mw:"2d4" in
    let languages = Language.[ common; abyssal; any 1 ] in
    let info =
      Entry.(
        info
          ~name:"Shade"
          ~title:"Pieces of Gods Gone By"
          ~intro:
            "Shades begin as normal humans, but at some point in their adolescence a \
             piece of Lacuna's Shadow merged with their body. This connection tethers \
             shades to an immense, ancient power that simultaneously grants them great \
             boons while also eating away at their very being."
          ~physical_qualities:
            [ !"Shades have similar dimensions to humans, generally speaking, as they \
                were human earlier in their life."
            ; !"A shade's tether to the Shadow gives away their \"otherness\" compared \
                to regular humans. The tether can appear in different ways, but common \
                variations include all-black pupils, skin that appears to fade into \
                smoke, nails and hair made of pieces of obsidian, and when they speak \
                the words appear to come from somewhere other than their mouth."
            ; !"As a shade ages and their tether takes over their body, the otherworldly \
                aspects of their appearance become more defined. A particularly old \
                shade might have whole limbs of smoke or speak without opening their \
                mouth."
            ]
          ~playstyle:
            [ !"In many parts of Althane shades are shunned or considered with mistrust. \
                However, some religious and criminal organizations worship the Shadow, \
                and many see the shades as divine heralds of their God's will. While \
                shades have no predisposition towards any alignment or personality, the \
                company they're forced to keep skews their views."
            ; !"Due to their natural proclivity to blend into the shadows some shade \
                adventurers becomes rangers and rogues, although many shades seek to \
                better understand their condition and follow the arcane path of the \
                wizard."
            ])
    in
    let traits =
      Description.(
        standard
          ~age:
            "Shades have unnaturally extended lifetimes when compared to untethered \
             humans. Shades mature at the same rate but can live upwards of 200 years."
          ~creature_type:Creature_type.aberration
          ~size:"Shades have the same range of height and weight as humans."
        @ [ "Innate Understanding"
            --> "You gain proficiency in two skills of your choice."
          ; "Otherworldly Sustenance"
            --> "You sustain yourself on your connection to the Shadow. You do not need \
                 to breathe."
          ; "Darkvision"
            --> "With a greater connection to the shadows of the world, your eyes can \
                 easily see through darkness. You can see in dim light within 60 feet of \
                 you as if it were bright light, and in darkness as if it were dim \
                 light. You can't discern color in darkness, only shades of gray."
          ]
        @ entries)
    in
    Race.create
      ~name
      ~size
      ~speed
      ~ability
      ~creature_type
      ~height_and_weight
      ~languages
      ~info
      ~traits
  ;;

  let farseer =
    create
      ~name:"Farseer"
      ~speed:(Speed.create 30 ())
      ~ability:(Ability.create ~int_:2 ~wis:1 ())
      ~entries:
        Description.
          [ ne
              "Imbued Token"
              Entry.
                [ !"As a bonus action, you can imbue a small trinket or token with your \
                    magic until you finish a long rest. While the token is imbued in \
                    this way, you can take these actions:"
                ; ln
                    [ "Telepathic Message."
                      -!> "As an action, you can send a telepathic message to the \
                           creature holding or carrying the token, as long as you are \
                           within 10 miles of it. The message can contain up to \
                           twenty-five words and the recipient can respond in a like \
                           manner. In addition, the holder of the token initiate the \
                           telepathic conversation."
                    ; "Remote Viewing."
                      -!> "If you are within 10 miles of the token, you can enter a \
                           trance as an action. The trance lasts for 1 minute, but it \
                           ends early if you dismiss it (no action required) or are \
                           {@condition incapacitated}. During this trance, you can see \
                           and hear from the token as if you were located where it is. \
                           While you are using your senses at the token's location, you \
                           are {@condition blinded} and {@condition deafened} in regard \
                           to your own surroundings. When the trance ends, the token is \
                           harmlessly destroyed."
                    ]
                ; !"You can only have a single imbued token at a time. When you attempt \
                    to imbue a trinket any existing imbued trinkets lose their \
                    connection to you."
                ]
          ; "Hex Magic"
            --> "You can cast the {@spell disguise self} and {@spell hex} spells. Once \
                 you cast either of these spells with this trait, you can't cast that \
                 spell with it again until you finish a long rest. You may also cast \
                 these spells using any spell slots you have. Intelligence is your \
                 spellcasting ability for these spells."
          ]
  ;;

  let bloodwell =
    create
      ~name:"Bloodwell"
      ~speed:(Speed.create 35 ~climb:35 ())
      ~ability:(Ability.create ~con:2 ~dex:1 ())
      ~entries:
        Description.
          [ "Spider Climb"
            --> "You have a climbing speed equal to your walking speed. In addition, at \
                 3rd level, you can move up, down, and across vertical surfaces and \
                 upside down along ceilings, while leaving your hands free."
          ; ne
              "Vampiric Bite"
              Entry.
                [ !"Your fanged bite is a natural weapon, which counts as a simple melee \
                    weapon with which you are proficient. You add your Constitution \
                    modifier, instead of your Strength modifier, to the attack and \
                    damage rolls when you attack with this bite. It deals {@dice 1d4} \
                    piercing damage on a hit. While you are missing half or more of your \
                    hit points, you have advantage on attack rolls you make with this \
                    bite."
                ; !"When you attack with this bite and hit a creature that isn't a \
                    Construct or an Undead, you can empower yourself in one of the \
                    following ways of your choice:"
                ; lh
                    [ !"You regain hit points equal to the piercing damage dealt by the \
                        bite."
                    ; !"You gain a bonus to the next ability check or attack roll you \
                        make; the bonus equals the piercing damage dealt by the bite"
                    ]
                ; !"You can empower yourself with this bite a number of times equal to \
                    your proficiency bonus, and you regain all expended uses when you \
                    finish a long rest."
                ]
          ]
  ;;

  let deathless =
    create
      ~name:"Deathless"
      ~speed:(Speed.create 30 ())
      ~ability:(Ability.create ~dex:2 ~con:1 ())
      ~entries:
        Description.
          [ ne
              "Deathless Nature"
              Entry.
                [ !"You have escaped death, a fact represented by the following benefits:"
                ; lh
                    [ !"You have advantage on saving throws against disease and being \
                        {@condition poisoned}, and you have resistance to poison damage."
                    ; !"You have advantage on death saving throws."
                    ; !"You don't need to eat, drink, or breathe."
                    ; !"You don't need to sleep, and magic can't put you to sleep. You \
                        can finish a long rest in 4 hours if you spend those hours in an \
                        inactive, motionless state, during which you retain \
                        consciousness."
                    ]
                ]
          ; "Knowledge from Beyond"
            --> "You temporarily remember glimpses of the past, drawing from the \
                 knowledge of the Shadow. When you make an ability check that uses a \
                 skill, you can roll a {@dice d6} immediately after seeing the number on \
                 the {@dice d20} and add the number on the {@dice d6} to the check. You \
                 can use this feature a number of times equal to your proficiency bonus, \
                 and you regain all expended uses when you finish a long rest."
          ]
  ;;

  let race = [ farseer; bloodwell; deathless ]
end

module Goblin = struct
  let create ~name ~size:(size, size_flavor) ~ability ~height_and_weight ~entries =
    let name = [%string "Goblin (%{name})"] in
    let speed = Speed.create 30 () in
    let creature_type = Creature_type.humanoid in
    let languages = Language.[ common; goblin ] in
    let info =
      Entry.(
        info
          ~name:"Goblin"
          ~title:"An Explosion of Energy"
          ~intro:
            "The goblin races are often considered suspiciously by the other races when \
             they venture into large population areas -- often rightfully so, as \
             wherever one sees a goblin a trail of explosions and untied shoelaces is \
             likely to follow."
          ~physical_qualities:
            [ !"While the general dimensions of the different goblin races vary greatly, \
                they all have skin tones of marbled yellow and red and tend to have \
                stark red or jet black hair."
            ; !"Goblins seemingly need to stay occupied at all times. It's very rare to \
                see a goblin that fidgeting with something: cleaning gear, eating \
                snacks, rigging small bombs, or tinkering with some contraption."
            ; !"Goblin families tend to be both very large and very close knit. A single \
                family home might have six generations with ten times as many people \
                packed into it at any given time."
            ]
          ~playstyle:
            [ !"Goblins are tinkerers at heart. They tend to be all over the place, \
                moving from one fixation to the next; and they aren't above doing \
                something outrageous if they think it will be fun. While their \
                scatterbrained-ness doesn't lend itself particularly well to research, \
                many goblins find themselves right at home in workshops and smithies \
                where they can properly flex their creative muscles."
            ; !"Goblins are commonly found in crowded metropolitan areas -- as these \
                give them the largest chance to experience new and interesting things. \
                Goblin adventurers are commonly artificers or barbarians, depending on \
                how good of a handle they have on their naturally destructive \
                tendencies."
            ])
    in
    let traits =
      Description.(
        standard
          ~age:
            "Goblins mature exceptionally quickly, considered adults after just five \
             years. While a goblin can theoretically live to the ripe, old age of 50, \
             most go out in a blaze of glory before then."
          ~creature_type
          ~size:size_flavor)
      @ Description.
          [ "Maker's Gift"
            --> "You know the {@spell mending} cantrip. You can also cast the {@spell \
                 magic weapon} spell with this trait. WHen you do so, the spell lasts \
                 for one hour and doesn't require concentration. Once you cast the spell \
                 with this trait, you can't do so again until you finish a long rest. \
                 Intelligence is your spellcasting modifier for these spells."
          ; "Busy Hands" --> "You gain proficiency in your two toolkits of your choice."
          ]
      @ entries
    in
    Race.create
      ~name
      ~size
      ~speed
      ~ability
      ~creature_type
      ~height_and_weight
      ~languages
      ~info
      ~traits
  ;;

  let clever_one =
    create
      ~name:"Clever One"
      ~size:
        ( Size.small
        , "Clever Ones are small and spindly, standing only three feet tall at their \
           tallest. Their arms and legs are elongated in proportion to the their small \
           bodies, and their fingers and toes are also long and slender. Your size is \
           small." )
      ~ability:(Ability.create ~dex:2 ~con:1 ())
      ~height_and_weight:(Height_and_weight.create ~h:41 ~mh:"2d4" ~w:34 ~mw:"1d4")
      ~entries:
        Description.
          [ "Fury of the Small"
            --> "When you damage a creature with an attack or a spell and the creature's \
                 size is larger than yours, you can cause the attack or spell to deal \
                 extra damage to the creature. The extra damage equals your level. Once \
                 you use this trait, you can't use it again until you finish a short or \
                 long rest."
          ; "Nimble Escape"
            --> "You can take the {@action Disengage} or {@action Hide} action as a \
                 bonus action on each of your turns."
          ]
  ;;

  let mighty_one =
    create
      ~name:"Mighty One"
      ~size:
        ( Size.medium
        , "Mighty Ones are tall and imposing when compared to the other goblin races. \
           They naturally put on muscle as they reach adolescence, and take pride in \
           their physique. Your size is medium." )
      ~ability:(Ability.create ~con:2 ~int_:1 ())
      ~height_and_weight:(Height_and_weight.create ~h:56 ~mh:"2d10" ~w:110 ~mw:"2d4")
      ~entries:
        Description.
          [ "Well Armed"
            --> "You are proficient with two {@filter martial \
                 weapons|items|source=phb|category=basic|type=martial weapon} of your \
                 choice and with light armor."
          ; "Double-take"
            --> "Mighty Ones are good at recovering from failures, blowing them off as \
                 purposeful gags. If you miss with an attack roll or fail an ability \
                 check or a saving throw, you can gain a bonus to the roll equal to the \
                 number of allies you can see within 30 feet of you (maximum bonus of \
                 +5). Once you use this trait, you can't use it again until you finish a \
                 long rest."
          ; "Hotswap"
            --> "When a creature you can see within five feet of you is hit by an attack \
                 roll, you can use your reaction to swap places with that creature, and \
                 you are hit by the attack instead. Once you use this trait, you can't \
                 do so again until you finish a short or long rest."
          ]
  ;;

  let race = [ clever_one; mighty_one ]
end

module Faceless = struct
  let race =
    [ Race.create
        ~name:"Faceless"
        ~size:Size.medium
        ~speed:(Speed.create 30 ())
        ~ability:(Ability.create ~cha:2 ~wis:1 ())
        ~creature_type:Creature_type.humanoid
        ~height_and_weight:(Height_and_weight.create ~h:61 ~mh:"1d4" ~w:115 ~mw:"1d4")
        ~languages:Language.[ common; any 2 ]
        ~info:
          Entry.(
            info
              ~name:"Faceless"
              ~title:"Many Personas on One Body"
              ~intro:
                "The faceless can shift their forms with a thought. Many use this to \
                 gift as a form of artistic and emotional expression, but it's also an \
                 invaluable as a tool for grifters, spies, and others who wish to \
                 deceive."
              ~physical_qualities:
                [ !"In their true form, faceless are pale, with colorless eyes and \
                    silver-white hair. It's rare to see a faceless in that form, as a \
                    typical faceless changes their shape the way others might change \
                    their clothes."
                ; !"Faceless tend to adopt a set of personas that they rely on most \
                    heavily. Each persona might represent a different aspect of their \
                    personality, and depending on the situation they might flow from one \
                    to the other. A persona can be owned by multiple faceless at the \
                    same time -- some families pass personas down to their children \
                    allowing a younger faceless to take advantage of contacts \
                    established by the persona's previous owners."
                ]
              ~playstyle:
                [ !"Faceless are usually born into one of two options. A few are born \
                    into communities of faceless and flaunt their nature openly amongst \
                    their peers. The others are born into areas where their family's \
                    nature isn't known, and may go years before they even realize \
                    themselves that they're different from others."
                ; !"A faceless' persona helps pinpoint a particular skill or emotion. A \
                    faceless adventurer might have personas for many situations, \
                    including negotiation, investigation, and combat. When making a \
                    faceless, consider their relationships with the people around them. \
                    Do they conceal their true nature or embrace it?"
                ])
        ~traits:
          Description.(
            standard
              ~age:"Faceless mature at, and live to, the same age as humans do."
              ~creature_type:Creature_type.humanoid
              ~size:
                "Your size is variable depending on your current form, but it always \
                 stays with in the range of three to seven feet. Your size is Medium."
            @ [ "Shapechanger"
                <-> [ "As an action, you can change your appearance and your voice. You \
                       determine the specifics of the changes, including your \
                       coloration, hair length, and sex. You can also adjust your height \
                       and weight, but not so much that your size changes. You can make \
                       yourself appear as a member of another race, though none of your \
                       game statistics change. You can't duplicate the appearance of a \
                       creature you've never seen, and you must adopt a form that has \
                       the same basic arrangement of limbs that you have. Your clothing \
                       and equipment aren't changed by this trait."
                    ; "You stay in the new form until you use an action to revert to \
                       your true form or until you die."
                    ]
              ; "Faceless Instincts"
                --> "You gain proficiency with two of the following skills of your \
                     choice: {@skill Deception}, {@skill Insight}, {@skill \
                     Intimidation}, and {@skill Persuasion}."
              ])
    ]
  ;;
end

module Dray = struct
  let create ~name ~ability ~description ~entries =
    let name = [%string "Dray (%{name})"] in
    let size = Size.medium in
    let speed = Speed.create 30 () in
    let creature_type = Creature_type.dragon in
    let height_and_weight = Height_and_weight.create ~h:66 ~mh:"3d8" ~w:175 ~mw:"2d6" in
    let languages = Language.[ common; draconic ] in
    let info =
      Entry.(
        info
          ~name:"Dray"
          ~title:"Draconic Essence in Humanoid Form"
          ~intro:
            "Dray are the result of an ancient society of dragons attempting to distill \
             their essence into a humanoid form. They carry themselves with the same \
             regal stature that their progenitors command."
          ~physical_qualities:
            ([ !"Dray look similar to their creators, with interlocking scales of \
                 vibrant hues and long, prehensile tails. A dray's horn structure and \
                 eye color match the dragon that donated its essence to make their kind. \
                 Dray tend to carry themselves with a stoic, commanding presence -- \
                 which they use their height and booming voices to back up."
             ]
            @ description)
          ~playstyle:
            [ !"Dray value skill and excellence in all endeavors. They hate to fail, and \
                they push themselves to extreme efforts before they give up on \
                something. A dray holds mastery of that particular skill as a lifetime \
                goal."
            ; !"Dray live in clan structures and are expected to represent their clan to \
                the highest standard. Bringing dishonor to a clan can result in disgrace \
                and exile. Each dray knows their station and duties within the clan, and \
                honor demands maintaining the bounds of that position."
            ])
    in
    let traits =
      Description.(
        standard
          ~age:
            "Dray mature slowly for the mortal races, and aren't considered adults until \
             their fifties. A dray can live up to 180 years."
          ~creature_type
          ~size:
            "Dray tend to be larger than humans, ranging six to eight feet tall with \
             increased bulk to accompany it. Your size is Medium.")
      @ entries
    in
    Race.create
      ~name
      ~size
      ~speed
      ~ability
      ~creature_type
      ~height_and_weight
      ~languages
      ~info
      ~traits
  ;;

  let astral =
    create
      ~name:"Astral"
      ~ability:(Ability.create ~con:2 ~wis:1 ())
      ~description:
        Entry.
          [ "Astral Dray"
            --> "Astral dray take after the cosmic dragon that lent its essence to their \
                 people. Astral dray can have blue, yellow, and purple scales and tend \
                 to have ornate, spiralling horns. Astral dray also grow swirling \
                 protrusions of bone on their chest and shoulders, that they show of \
                 with accommodating clothing."
          ]
      ~entries:
        Description.
          [ "Domineering Presence"
            <-> [ "You can use your action to exude a mentally crushing wave of force. \
                   When you use your presence, every creature in a 15 ft. cone in front \
                   of you must make a Wisdom saving throw against a DC of 8 + your \
                   proficiency bonus + your constitution modifier. A creature takes 2d6 \
                   + half your level (rounded down) psychic damage on a failed save, or \
                   half as much damage on a successful one."
                ; "After you use your presence, you can't use it again until you finish \
                   a long or short rest."
                ]
          ; "Mental Fortitude" --> "You have resistance to psychic damage."
          ; "Cosmic Sight"
            --> "You have advantage on Intelligence (Investigation) checks made to \
                 discern illusions or to locate hidden passages or compartments."
          ; "Nebulous Magic"
            --> "You know the {@spell minor illusion} cantrip. When you reach 3rd level, \
                 you can cast {@spell fog cloud} once with this trait. You regain the \
                 ability to cast this spell with this trait when you finish a long rest. \
                 Intelligence is your spellcasting modifier for these spells."
          ]
  ;;

  let tellurian =
    create
      ~name:"Tellurian"
      ~ability:(Ability.create ~con:2 ~str:1 ())
      ~description:
        Entry.
          [ "Tellurian Dray"
            --> "Tellurian dray are shaped in the form of the titanic dragon that gave \
                 itself to make them. Tellurian dray can have large matte gray, black, \
                 and red scales that connect to make a kind of pseudo-plate armor. Their \
                 heads are more elongated than other dray forms, and instead of the \
                 standard pair of eyes they have a row of three secondary eyes on each \
                 side of their face."
          ]
      ~entries:
        Description.
          [ "Terrestrial Command"
            <-> [ "You can use your action to summon spikes of molten earth from the \
                   ground. When you use your command, every creature in a 30 ft. line in \
                   front of you must make a Dexterity saving throw against a DC of 8 + \
                   your proficiency bonus + your constitution modifier. A creature takes \
                   1d12 + half your level (rounded down) fire damage on a failed save, \
                   or half as much damage on a successful one."
                ; "After you use your command, you can't use it again until you finish a \
                   long or short rest."
                ]
          ; "Earthen Toughness"
            --> "Your hitpoint maximum increases by 1, and it increases by 1 every time \
                 you gain a level."
          ; "Tremorsense"
            --> "You have a +5 bonus to your passive Wisdom (Insight) and (Perception) \
                 scores. You also have blindsight out 10 feet."
          ; "Adamant Mind"
            --> "You have advantage on death saving throws and saving throws against \
                 being charmed or frightened."
          ]
  ;;

  let spectral =
    create
      ~name:"Spectral"
      ~ability:(Ability.create ~dex:2 ~int_:1 ())
      ~description:
        Entry.
          [ "Spectral Dray"
            --> "Spectral dray have the physical characteristics of the spiritual dragon \
                 that gave them life. Spectral dray have can have fluorescent green, \
                 yellow, and orange scales and can change the patterning on their scales \
                 at will. They also commonly have barbs or whiskers growing off of their \
                 snout."
          ]
      ~entries:
        Description.
          [ "Inherent Telepathy"
            --> "You can communicate simple ideas, emotions, and images telepathically \
                 to any creature within 30 feet of you that can understand a language."
          ; "Familiar Arcana"
            --> "You are proficient in the Arcana skill. In addition, you can cast \
                 {@spell find familiar} once with this trait, requiring no components. \
                 You regain the ability to cast this spell with this trait when you \
                 finish a long rest."
          ; "Magic Resistance"
            --> "When you fail a saving throw against a spell or other magic effect, you \
                 can choose to reroll the save, and you must use the new roll. Once you \
                 use this trait, you can't use it again until you finish a short or long \
                 rest."
          ]
  ;;

  let solar =
    create
      ~name:"Solar"
      ~ability:(Ability.create ~str:2 ~con:1 ())
      ~description:
        Entry.
          [ "Solar Dray"
            --> "Solar dray take after the radiant dragon that formed them. Solar dray \
                 can have reflective scales in metallic colors: gold, silver, and \
                 bronze. They produce sunlight within their bodies, and when they one \
                 opens its mouth a glow at the back of their throat can be seen."
          ]
      ~entries:
        Description.
          [ "Blinding Force"
            <-> [ "You can use your action to project the sunlight in your body as a \
                   searing beam. When you use your force, every creature in a 15 ft. \
                   cone in front of you must make a Constitution saving throw against a \
                   DC of 8 + your proficiency bonus + your constitution modifier. A \
                   creature takes 2d6 + half your level (rounded down) radiant damage on \
                   a failed save, or half as much damage on a successful one."
                ; "After you use your force, you can't use it again until you finish a \
                   long or short rest."
                ]
          ; "Solar Absorption" --> "You have resistance to radiant damage."
          ; "Scorching Presence"
            --> "You have proficiency in the Intimidation skill, and you automatically \
                 succeed on constitution saving throws to endure extreme climates (both \
                 hot and cold)."
          ; "Fission"
            --> "As a bonus action you can cause your body to begin pulsing with a \
                 vibrant, radiant energy. You shed bright light in a 30-foot radius and \
                 dim light for an additional 15 feet. This light lasts until you are \
                 incapacitated, you die, or you dismiss it as a free action."
          ]
  ;;

  let race = [ astral; tellurian; spectral; solar ]
end

module Beastfolk = struct
  let create
      ~name
      ~size:(size, size_flavor)
      ~speed
      ~age
      ~ability
      ~height_and_weight
      ~description
      ~playstyle
      ~entries
    =
    let creature_type = Creature_type.beast in
    let languages = Language.[ common; any 1 ] in
    let info =
      Entry.(
        info
          ~name:"Beastfolk"
          ~title:"Apex of the Animal Kingdom"
          ~intro:
            "Beastfolk is the blanket term for the wide variety of humanoid creatures on \
             Althane that have the traits of different animals across the planet. \
             Beastfolk are as varied as the organisms they source their abilities from, \
             and collectively can be found everywhere across the globe."
          ~physical_qualities:
            ([ !"Beastfolk are humanoid, but have traits similar to the animals from \
                 which they gain their namesake. For instance, ivrik tower over other \
                 creatures and have great trunks, but bes have a commanding presence and \
                 razor-sharp claws."
             ]
            @ description)
          ~playstyle:
            ([ !"Beastfolk can be found nearly anywhere in Althane, and the diversity in \
                 their kind lends to different personalities and common traits in \
                 different subraces. Some beastfolk live in small tribes in the \
                 wilderness, and others live in bustling capital cities."
             ]
            @ playstyle))
    in
    let traits = Description.(standard ~age ~creature_type ~size:size_flavor) @ entries in
    Race.create
      ~name
      ~size
      ~speed
      ~ability
      ~creature_type
      ~height_and_weight
      ~languages
      ~info
      ~traits
  ;;

  let ivrik =
    create
      ~name:"Ivrik"
      ~size:
        ( Size.large
        , "Ivrik stand between 8 and 10 feet tall. Their massive bodies weigh between \
           400 and 500 pounds. Your size is Large." )
      ~speed:(Speed.create 30 ())
      ~age:
        "Ivrik physically mature at the same rate as humans, but they live about 200 \
         years. They highly value the weight of wisdom and experience and are considered \
         young until they reach the age of 60."
      ~ability:(Ability.create ~con:2 ~wis:1 ())
      ~height_and_weight:(Height_and_weight.create ~h:79 ~mh:"3d10" ~w:395 ~mw:"2d4")
      ~description:
        Entry.
          [ "Ivrik"
            --> "Ivrik are humanoid elephants. They have the characteristic trunk and \
                 large ears of an elephant, but they stand on two legs like a human \
                 might and they have four-digited hands and feet. Their skin varies from \
                 pale blue to black, with large tusks of white ivory."
          ]
      ~playstyle:
        Entry.
          [ "Ivrik"
            --> "The ivrik are often oases of calm in the busy streets of large cities. \
                 They hum or chant in sonorous tones and move slowly or sit in perfect \
                 stillness. If provoked to action, ivrik are true terrors -- bellowing \
                 with rage, trumpeting and flapping their ears. Their serene wisdom, \
                 fierce loyalty, and unwavering conviction are tremendous assets to \
                 their allies."
          ]
      ~entries:
        Description.
          [ "Powerful Build"
            --> "You count as one size larger when determining your carrying capacity \
                 and the weight you can push, drag, or lift."
          ; "Mental Fortitude"
            --> "You have advantage on saving throws against being {@condition charmed} \
                 or {@condition frightened}."
          ; "Natural Armor"
            --> "You have thick, leathery skin. When you aren't wearing armor, your AC \
                 is 12 + your Constitution modifier. You can use your natural armor to \
                 determine your AC if the armor you wear would leave you with a lower \
                 AC. A shield's benefits apply as normal while you use your natural \
                 armor."
          ; "Trunk"
            <-> [ "You can grasp things with your trunk, and you can use it as a \
                   snorkel. It has a reach of 5 feet, and it can lift a number of pounds \
                   equal to five times your Strength score. You can use it to do the \
                   following simple tasks: lift, drop, hold, push, or pull an object or \
                   a creature; open or close a door or a container; grapple someone; or \
                   make an unarmed strike. Your DM might allow other simple tasks to be \
                   added to that list of options."
                ; "Your trunk can't wield weapons or shields or do anything that \
                   requires manual precision, such as using tools or magic items or \
                   performing the somatic components of a spell."
                ]
          ; "Keen Smell"
            --> "Thanks to your sensitive trunk, you have advantage on Wisdom ({@skill \
                 Perception}), Wisdom ({@skill Survival}), and Intelligence ({@skill \
                 Investigation}) checks that involve smell."
          ]
  ;;

  let bes =
    create
      ~name:"Bes"
      ~size:
        ( Size.medium
        , "Bes are typically over 6 feet tall, with some standing over 7 feet. Your size \
           is Medium." )
      ~speed:(Speed.create 35 ())
      ~age:
        "Bes mature and age at about the same rate as humans, capping out around 100 \
         years."
      ~ability:(Ability.create ~con:2 ~str:1 ())
      ~height_and_weight:(Height_and_weight.create ~h:66 ~mh:"2d10" ~w:180 ~mw:"2d6")
      ~description:
        Entry.
          [ "Bes"
            --> "Bes are humanoid tigers. Orange or white fur with black stripes covers \
                 bes bodies, and some grow thick manes ranging in shades from gold to \
                 black. While their hands prove as nimble as those of other humanoids, \
                 bes have retractable feline claws, which they can extend instantly."
          ]
      ~playstyle:
        Entry.
          [ "Bes"
            --> "Bes tend to be tall compared to humans and move with a boldness that \
                 suggests their physical might.  This, along with their ability to \
                 produce bone-shaking roars, gives most bes an air that readily shifts \
                 between regal and fearsome."
          ]
      ~entries:
        Description.
          [ "Claws"
            --> "Your claws are natural weapons, which you can use to make unarmed \
                 strikes. If you hit with them, you can deal slashing damage equal to \
                 {@dice 1d6} + your Strength modifier, instead of the bludgeoning damage \
                 normal for an unarmed strike."
          ; "Hunter's Instincts"
            --> "You have proficiency in one of the following skills of your choice: \
                 {@skill Athletics}, {@skill Intimidation}, {@skill Perception}, or \
                 {@skill Survival}."
          ; "Daunting Roar"
            --> "As a bonus action, you can let out an especially menacing roar. \
                 Creatures of your choice within 10 feet of you that can hear you must \
                 succeed on a Wisdom saving throw or become {@condition frightened} of \
                 you until the end of your next turn. The DC of the save equals 8 + your \
                 proficiency bonus + your Constitution modifier. Once you use this \
                 trait, you can't use it again until you finish a short or long rest."
          ]
  ;;

  let drakh =
    create
      ~name:"Drakh"
      ~size:
        ( Size.medium
        , "Drakh are a little bulkier and taller than humans, and their colorful frills \
           make them appear even larger. Your size is Medium." )
      ~speed:(Speed.create 30 ~swim:30 ())
      ~age:"Drakh reach maturity around age 14 and rarely live longer than 60 years."
      ~ability:(Ability.create ~con:2 ~str:1 ())
      ~height_and_weight:(Height_and_weight.create ~h:76 ~mh:"2d10" ~w:210 ~mw:"2d6")
      ~description:
        Entry.
          [ "Drakh"
            --> "Drakh are reptilian humanoids, closer to terrestrial lizards than \
                 dragons.  They're covered in small green and yellowish scales across \
                 their body, and they have an orange frill around their neck. While all \
                 drakh have sharp claws, many have four protruding fangs, two on each \
                 jax, used for clamping down on prey."
          ]
      ~playstyle:
        Entry.
          [ "Drakh"
            <-> [ "Lacking any internal emotional reactions, drakh behave in a distant \
                   manner. They don't mourn fallen comrades or rage against their \
                   enemies. They simply observe and react as a situation warrants."
                ; "Drakh assess everyone and everything in terms of utility. Art and \
                   beauty have little meaning for them. A sharp sword serves a useful \
                   and good purpose, while a dull sword is a dead weight without a \
                   whetstone."
                ; "Despite their alien outlook, some drakh make an effort to understand \
                   and, in their own manner, befriend people of other races. Such drakh \
                   make faithful and skilled allies."
                ]
          ]
      ~entries:
        Description.
          [ "Swim Speed" --> "You have a swimming speed of 30 feet."
          ; "Cunning Artisan"
            --> "As part of a short rest, you can harvest bone and hide from a slain \
                 beast, construct, dragon, monstrosity, or plant creature of size small \
                 or larger to create one of the following items: a {@item shield|phb}, a \
                 {@item club|phb}, a {@item javelin|phb}, or {@dice 1d4} {@item \
                 dart|phb|darts} or {@item blowgun needle|phb|blowgun needles}. To use \
                 this trait, you need a blade, such as a {@item dagger|phb}, or \
                 appropriate {@filter artisan's \
                 tools|items|source=phb|miscellaneous=mundane|type=artisan's tools}, \
                 such as {@item leatherworker's tools|phb}."
          ; "Hold Breath" --> "You can hold your breath for up to 15 minutes at a time."
          ; "Hunter's Lore"
            --> "You gain proficiency with two of the following skills of your choice: \
                 {@skill Animal Handling}, {@skill Nature}, {@skill Perception}, {@skill \
                 Stealth}, and {@skill Survival}."
          ; "Natural Armor"
            --> "You have tough, scaly skin. When you aren't wearing armor, your AC is \
                 13 + your Dexterity modifier. You can use your natural armor to \
                 determine your AC if the armor you wear would leave you with a lower \
                 AC. A shield's benefits apply as normal while you use your natural \
                 armor."
          ; "Hungry Jaws"
            --> "In battle, you can throw yourself into a vicious feeding frenzy. As a \
                 bonus action, you can make a special attack with your bite. If the \
                 attack hits, you deal piercing damage equal to {@dice 1d6} + your \
                 Strength modifier, and you gain temporary hit points equal to your \
                 Constitution modifier (minimum of 1), and you can't use this trait \
                 again until you finish a short or long rest."
          ]
  ;;

  let grung =
    create
      ~name:"Grung"
      ~size:
        ( Size.small
        , "Grung stand between 2.5 and 3.5 feet tall and average about 30 pounds. Your \
           size is Small." )
      ~speed:(Speed.create 30 ())
      ~age:
        "Grung mature to adulthood in a single year, and have been known to live up to \
         30 years."
      ~ability:(Ability.create ~dex:2 ~con:1 ())
      ~height_and_weight:(Height_and_weight.create ~h:24 ~mh:"1d12" ~w:10 ~mw:"1d4")
      ~description:
        Entry.
          [ "Grung"
            --> "Grung are frog-like amphibious humanoids. They have slick skin that \
                 comes in a variety of colors across the rainbow speckled with black \
                 patterning. Grung are carnivorous and have rows of sharp teeth instead \
                 of the standard toothless mouth that regular frogs have. As a result, \
                 they do not have a prehensile tongue."
          ]
      ~playstyle:
        Entry.
          [ "Grung"
            --> "Grung are naturally adverse to living in large cities, preferring to \
                 stick to the hot jungles of their ancestral home. Short-tempered but \
                 fast on their feet, many grung adventurers find themselves becoming \
                 fighters or rogues."
          ]
      ~entries:
        Description.
          [ "Arboreal Alertness"
            --> "You have proficiency in the {@skill Survival} skill."
          ; "Amphibious" --> "You can breathe air and water."
          ; "Poison Immunity"
            --> "You're immune to poison damage and the {@condition poisoned} condition."
          ; "Poisonous Saliva"
            --> "You can apply your poisonous saliva to any piercing weapon as part of \
                 an attack with that weapon. The target must succeed on a DC 12 \
                 Constitution saving throw or take {@dice 2d4} poison damage. Once you \
                 use this trait a number of times equal to your proficiency bonus you \
                 may not do so again before a long rest."
          ; "Standing Leap"
            --> "Your long jump is up to 25 feet and your high jump is up to 15 feet, \
                 with or without a running start."
          ]
  ;;

  let aven =
    create
      ~name:"Aven"
      ~size:
        ( Size.medium
        , "Aven are about 5 feet tall. They have thin, lightweight bodies that weigh \
           between 80 and 100 pounds. Your size is Medium." )
      ~speed:(Speed.create 25 ~fly:35 ())
      ~age:
        "Aven reach maturity by age 3. Compared to humans, aven don't usually live \
         longer than 30 years."
      ~ability:(Ability.create ~dex:2 ~wis:1 ())
      ~height_and_weight:(Height_and_weight.create ~h:45 ~mh:"1d12" ~w:72 ~mw:"1d4")
      ~description:
        Entry.
          [ "Aven"
            --> "From below, aven look much like large birds. Only when they descend to \
                 roost on a branch or walk across the ground does their humanoid \
                 appearance reveal itself. Standing upright, aven might reach 5 feet \
                 tall, and they have long, narrow legs that taper to sharp talons. Aven \
                 typically have colorful plumage, with feathers of blue, orange, yellow, \
                 or gray."
          ]
      ~playstyle:
        Entry.
          [ "Aven"
            --> "Nowhere are the aven more comfortable than in the sky. They can spend \
                 hours in the air, and some go as long as days, locking their wings in \
                 place and letting the thermals hold them aloft. In battle, they prove \
                 dynamic and acrobatic fliers, moving with remarkable speed and grace, \
                 diving to lash opponents with weapons or talons before turning and \
                 flying away."
          ]
      ~entries:
        Description.
          [ "Flight"
            --> "You have a flying speed of 35 feet. To use this speed, you can't be \
                 wearing medium or heavy armor."
          ; "Talons"
            --> "Your talons are natural weapons, which you can use to make unarmed \
                 strikes. If you hit with them, you deal slashing damage equal to \
                 {@damage 1d4} + your Strength modifier, instead of the bludgeoning \
                 damage normal for an unarmed strike."
          ]
  ;;

  let race = [ ivrik; bes; drakh; grung; aven ]
end

module Lycan = struct
  let create ~name ~ability ~description ~entries =
    let name = [%string "Lycan (%{name})"] in
    let speed = Speed.create 30 () in
    let size = Size.medium in
    let creature_type = Creature_type.humanoid in
    let height_and_weight = Height_and_weight.create ~h:54 ~mh:"2d8" ~w:90 ~mw:"2d4" in
    let languages = Language.[ common ] in
    let info =
      Entry.(
        info
          ~name:"Lycan"
          ~title:"In-built Animal Companions"
          ~intro:
            "Lycan are humans that bonded with a bestial spirit at an early age, \
             allowing them to temporarily take their companion's form. While technically \
             a human can bond with any spirit, those more animalistic in nature seem to \
             be drawn to people more readily."
          ~physical_qualities:
            [ !"Lycan look -- for the most part -- like the humans they would be without \
                their connection to the spirits. The only real tell that something might \
                be different is that Lycan take on small aspects of their companion as \
                they grow. A swiftstride lycan may have catlike eyes and delicate build, \
                while a beasthide lycan might be a massive brute built like a bear."
            ]
          ~playstyle:
            ([ !"Different lycan met their animal spirit in different ways. Some might \
                 find their spirit when wandering in the woods as a child, and they \
                 might be the only Lycan in their village. Others might be part of a \
                 family that has bonded with a great sprit, and were introduce by their \
                 parents when they were old enough. There are also villages that have \
                 made pacts with groups of these spirits, and each child born into the \
                 village receives a spirit companion at birth."
             ]
            @ description))
    in
    let traits =
      Description.(
        standard
          ~age:
            "Lycan are quick to mature both physically and emotionally, reaching young \
             adulthood at age 10. They rarely live to be more than 70 years old."
          ~creature_type
          ~size:
            "As Lycan are humans who have made a pact with a bestial spirit, they have \
             the same general size as humans. Your size is Medium."
        @ [ "Darkvision"
            --> "You have superior vision in dark and dim conditions. You can see in dim \
                 light within 60 feet of you as if it were bright light, and in darkness \
                 as if it were dim light. You can't discern color in darkness, only \
                 shades of gray."
          ; "Shifting"
            <-> [ "As a bonus action, you can assume a more bestial appearance. This \
                   transformation lasts for 1 minute, until you die, or until you revert \
                   to your normal appearance as a bonus action. When you shift, you gain \
                   temporary hit points equal to your level + your Constitution modifier \
                   (minimum of 1 temporary hit point). You also gain additional benefits \
                   that depend on your lycan subrace, described below."
                ; "Once you shift, you can't do so again until you finish a short or \
                   long rest."
                ]
          ]
        @ entries)
    in
    Race.create
      ~name
      ~size
      ~speed
      ~ability
      ~creature_type
      ~height_and_weight
      ~languages
      ~info
      ~traits
  ;;

  let beasthide =
    create
      ~name:"Beasthide"
      ~ability:(Ability.create ~con:2 ~str:1 ())
      ~description:
        Entry.
          [ "Beasthide"
            --> "Stoic and solid, a beasthide lycan draws strength and stability from \
                 the beast within. Beasthide lycan are typically tied to a bear or boar \
                 spirit, but this subrace could embody any creature known for its \
                 toughness."
          ]
      ~entries:
        Description.
          [ "Natural Athlete" --> "You have proficiency in the {@skill Athletics} skill."
          ; "Shifting Feature"
            --> "Whenever you shift, you gain {@dice 1d6} additional temporary hit \
                 points. While shifted, you have a +1 bonus to your Armor Class."
          ]
  ;;

  let longtooth =
    create
      ~name:"Longtooth"
      ~ability:(Ability.create ~str:2 ~dex:1 ())
      ~description:
        Entry.
          [ "Longtooth"
            --> "Longtooth lycan are fierce and aggressive, but they form deep bonds \
                 with their friends. Many longtooth lycan have canine traits that become \
                 more pronounced as they shift, but they might instead draw on tigers, \
                 hyenas, or other predators."
          ]
      ~entries:
        Description.
          [ "Fierce" --> "You have proficiency in the {@skill Intimidation} skill."
          ; "Shifting Feature"
            --> "While shifted, you can use your elongated fangs to make an unarmed \
                 strike as a bonus action. If you hit with your fangs, you can deal \
                 piercing damage equal to {@dice 1d6} + your Strength modifier, instead \
                 of the bludgeoning damage normal for an unarmed strike."
          ]
  ;;

  let swiftstride =
    create
      ~name:"Swiftstride"
      ~ability:(Ability.create ~dex:2 ~cha:1 ())
      ~description:
        Entry.
          [ "Swiftstride"
            --> "Swiftstride lycan are graceful and quick. Typically feline in nature, \
                 swiftstride lycan are often aloof and difficult to pin down physically \
                 or socially."
          ]
      ~entries:
        Description.
          [ "Graceful" --> "You have proficiency in the {@skill Acrobatics} skill."
          ; "Shifting Feature"
            --> "While shifted, your walking speed increases by 10 feet. Additionally, \
                 you can move up to 10 feet as a reaction when a creature ends its turn \
                 within 5 feet of you. This reactive movement doesn't provoke \
                 opportunity attacks."
          ]
  ;;

  let wildhunt =
    create
      ~name:"Wildhunt"
      ~ability:(Ability.create ~wis:2 ~dex:1 ())
      ~description:
        Entry.
          [ "Wildhunt"
            --> "Wildhunt lycan are sharp and insightful. Many are constantly alert, \
                 ever wary for possible threats. Others focus on their intuition, \
                 searching within. Wildhunt lycan are excellent hunters, and they also \
                 tend to become the spiritual leaders of lycan communities."
          ]
      ~entries:
        Description.
          [ "Natural Tracker" --> "You have proficiency in the {@skill Survival} skill."
          ; "Shifting Feature"
            --> "While shifted, you have advantage on Wisdom checks, and no creature \
                 within 30 feet of you can make an attack roll with advantage against \
                 you, unless you're {@condition incapacitated}."
          ]
  ;;

  let race = [ beasthide; longtooth; swiftstride; wildhunt ]
end

module Praelin = struct
  let create
      ~name
      ~size:(size, size_flavor)
      ~speed
      ~ability
      ~height_and_weight
      ~physical_qualities
      ~playstyle
      ~entries
    =
    let creature_type = Creature_type.fey in
    let languages = Language.[ common; primordial ] in
    let info =
      Entry.(
        info
          ~name:"Praelin"
          ~title:"Ancient Invaders Well Integrated"
          ~intro:
            "The praelin come from somewhere other than Althane originally, but there \
             are no written records from their arrival and none are old enough to \
             remember it anymore. They've adapted themselves to this planet -- adopting \
             humanoid forms -- but still carry a distinct \"otherness\" that \
             distinguishes them from other creatures."
          ~physical_qualities
          ~playstyle:
            ([ !"Praelin might look human when one squints, but they came from very \
                 different stock than most of the other races on Althane. There are \
                 quirks that have been carried along from generation to generation \
                 despite not making much sense: a praelin might see a building and feel \
                 that the architecture is off, but not be able to articulate why; or \
                 they might go to throw a spear but find they put way too much force \
                 into it -- as if they had expected more resistance."
             ; !"The praelin are the epitome of individualism much in the same way that \
                 humans are. There are no broad strokes with which you can paint this \
                 race as a people, each one has the capacity to form a unique \
                 combination of billions of personality traits, and experience with one \
                 praelin doesn't prepare one for interactions with another."
             ]
            @ playstyle))
    in
    let traits =
      Description.(
        standard
          ~age:
            [%string
              "%{name} mature slightly slower than humans, and are considered adults at \
               25. They live on average until 125 years of age."]
          ~creature_type
          ~size:size_flavor)
      @ entries
    in
    Race.create
      ~name
      ~size
      ~speed
      ~ability
      ~creature_type
      ~height_and_weight
      ~languages
      ~info
      ~traits
  ;;

  let terran =
    create
      ~name:"Terran"
      ~size:
        ( Size.large
        , "Terrans bulky nature causes them to tower over most other races. They average \
           between seven and nine feet tall, and start at 500 pounds. Your size is \
           Large." )
      ~speed:(Speed.create 30 ~climb:30 ())
      ~ability:(Ability.create ~con:2 ~str:1 ())
      ~height_and_weight:(Height_and_weight.create ~h:76 ~mh:"2d6" ~w:500 ~mw:"2d4")
      ~physical_qualities:
        Entry.
          [ !"The terrans are towering, humanoid masses of rock. The color of the rock \
              varies between brown, red, and gray; and while some striations in the rock \
              are possible, they're not overly common. There are no breaks in the skin \
              around the joints of a terran. Instead, whenever a terran moves their arms \
              or legs, or opens their mouth to speak, the rock tears from the movement \
              and a red magma can be seen beneath the skin. The magma quickly hardens, \
              filling in the gap."
          ]
      ~playstyle:
        Entry.
          [ "Terrans"
            --> "Terrans are lumbering mountains in miniature, but -- perhaps in spite \
                 of their size -- they're known for expert craftsmanship and precise, \
                 fine detail. Many terran homes are covered from floor to ceiling in \
                 highly detailed ornamentation, and it's high praise to be complimented \
                 by a terran on one's work. Terrans tend to be thoughtful and careful, \
                 but it's very hard to stop one once they've started on something."
          ]
      ~entries:
        Description.
          [ "Earth Walk"
            --> "Your feet and ankles meld with the earth, and you glide instead of \
                 step. You can walk across difficult terrain made of earth or stone \
                 without expending the extra movement. You also have a climbing speed of \
                 30 feet."
          ; "Merge with Stone"
            --> "You can cast the {@spell pass without trace} spell once with this \
                 trait, requiring no material components. You regain the ability to cast \
                 it this way when you finish a long rest."
          ; "Vigilant Guardian"
            --> "When a creature you can see within 5 feet of you is hit by an attack \
                 roll, you can use your reaction to swap places with that creature, and \
                 you are hit by the attack instead. Once you use this trait, you cannot \
                 do so again until you finish a long rest."
          ]
  ;;

  let ignean =
    create
      ~name:"Ignean"
      ~size:
        ( Size.medium
        , "Igneans' whispiness gives them a smaller stature and build than many of the \
           other races. They tend to be around 5 feet tall, and weigh in the low 100 \
           area. Your size is Medium." )
      ~speed:(Speed.create 30 ())
      ~ability:(Ability.create ~dex:2 ~cha:1 ())
      ~height_and_weight:(Height_and_weight.create ~h:56 ~mh:"2d6" ~w:90 ~mw:"1d4")
      ~physical_qualities:
        Entry.
          [ !"Igneans are shorter, lithe humanoids; they have golden, nearly translucent \
              skin with swirled blue patterning and bright hair and eyes in shades of \
              red and orange. In their chest, where humans keep their heart and lungs, \
              igneans instead have a lit flame, which can be seen flickering beneath \
              their skin."
          ]
      ~playstyle:
        Entry.
          [ "Igneans"
            --> "Many igneans have both a literal and metaphorical fire burning in their \
                 heart. They tend to be passionate, rash, and opinionated. Many of \
                 Althane's most famous poets, writers, and despots have been Ignean -- \
                 the two most common forms of greeting for an ignean dignitary are a \
                 haiku and a punch to the face."
          ]
      ~entries:
        Description.
          [ "Darkvision"
            --> "You can see in dim light within 60 feet of you as if it were bright \
                 light, and in darkness as if it were dim light. The fire in your eyes \
                 tints everything red instead of gray when you use this trait."
          ; "Fire Resistance" --> "You have resistance to fire damage."
          ; "Reach to the Blaze"
            --> "You know the {@spell produce flame} cantrip. Once you reach third \
                 level, you can cast the {@spell burning hands} spell once with this \
                 trait as a 1st-level spell, and you regain the ability to cast it this \
                 way when you finish a long rest. Constitution is your spellcasting \
                 ability for these spells."
          ]
  ;;

  let vanden =
    create
      ~name:"Vanden"
      ~size:
        ( Size.medium
        , "Vanden tend to be the same general shape and size as humans. Your size is \
           Medium." )
      ~speed:(Speed.create 30 ~swim:30 ())
      ~ability:(Ability.create ~wis:2 ~int_:1 ())
      ~height_and_weight:(Height_and_weight.create ~h:64 ~mh:"2d6" ~w:175 ~mw:"1d4")
      ~physical_qualities:
        Entry.
          [ !"Vanden look like blue-skinned humans from a distance, but closer \
              inspection reveals that their skin isn't blue, but transparent. Their \
              musculature is made of pneumatic tubes filled with water, and in place of \
              organs they have colorful coral-like growths. If a vanden stays in the \
              same place for too long, the water in their body will slowly start to \
              freeze solid."
          ]
      ~playstyle:
        Entry.
          [ "Vanden"
            --> "Vanden are constantly moving, as choosing to stay in one place for too \
                 long could mean that they become permanently stuck there. While this \
                 technically only applies to their physical position, many vanden take \
                 this position with other aspects of their lives too: many vanden choose \
                 to live nomadically, and will take great steps to avoid stagnating in \
                 life. As a result vanden are less likely to be afraid of the unknown, \
                 but are more likely to never have a singular place to call home."
          ]
      ~entries:
        Description.
          [ "Acid Resistance" --> "You have resistance to acid damage."
          ; "Amphibious"
            --> "You can breathe air and water, and have a swim speed of 30 feet."
          ; "Call to the Wave"
            --> "You know the {@spell shape water} cantrip. When you reach 3-rd level, \
                 you can cast the {@spell create or destroy water} spell as a 2-nd level \
                 spell once with this trait, and you regain the ability to cast it this \
                 way when you finish a long rest."
          ]
  ;;

  let aetheren =
    create
      ~name:"Aetheren"
      ~size:
        ( Size.medium
        , "Aetheren usually fit into the same build and dimensions of humans. Your size \
           is Medium." )
      ~speed:(Speed.create 30 ())
      ~ability:(Ability.create ~int_:2 ~dex:1 ())
      ~height_and_weight:(Height_and_weight.create ~h:64 ~mh:"2d6" ~w:20 ~mw:"1d4")
      ~physical_qualities:
        Entry.
          [ !"Aetheren have a more amorphous form than many of the other praelin. While \
              their pale skin is solid to the touch, when they move only the outermost \
              layer travels immediately -- there's a few second of lag-time as the rest \
              of the body catches up. This gives the aetheren a very fluid appearance, \
              as they literally flow when they move from one place to another."
          ]
      ~playstyle:
        Entry.
          [ "Aetheren"
            --> "Aetheren know that life is fleeting and their time in a physical form \
                 is infinitesimally small compared to the enormity of time. As such, \
                 many focus on extending their legacy past their own lifespan -- \
                 communicating as many skills and as much information as they can to \
                 their children and others in their communities. Aetheren are known for \
                 being both paranoid and extremely forward-thinking, with plans not \
                 coming to fruition until generations after they were laid."
          ]
      ~entries:
        Description.
          [ "Unending Breath"
            --> "You can hold your breath indefinitely while you're not {@condition \
                 incapacitated}."
          ; "Generational Knowledge"
            --> "You gain proficiency with a set of artisan's tools of your choice."
          ; "Mingle with the Wind"
            --> "You can cast the {@spell levitate} spell once with this trait, \
                 requiring no material components, and you regain the ability to cast it \
                 this way when you finish a long rest. Constitution is your spellcasting \
                 ability for this spell."
          ]
  ;;

  let race = [ terran; ignean; vanden; aetheren ]
end

module Human = struct
  let create ~name ~speed ~ability ~entries =
    let name = [%string "Human (%{name})"] in
    let creature_type = Creature_type.humanoid in
    let size = Size.medium in
    let languages = Language.[ common; any 2 ] in
    let height_and_weight = Height_and_weight.create ~h:56 ~mh:"2d10" ~w:110 ~mw:"2d4" in
    let info =
      Entry.(
        info
          ~name:"Human"
          ~title:"Inheritors of the Spark"
          ~intro:
            "Humans hold the honor of being the first intelligent life on Althane. Since \
             they've gained awareness civilizations have risen and fallen, other races \
             have branched off, and the world has been irrevocably been altered."
          ~physical_qualities:
            [ !"With their penchant for migration and conquest, humans are more \
                physically diverse than other common races. There is no typical human. \
                An individual can stand from 5 feet to a little over 6 feet tall and \
                weigh from 125 to 250 pounds. Human skin shades range from nearly black \
                to very pale, and hair colors from black to blond (curly, kinky, or \
                straight); males might sport facial hair that is sparse or thick."
            ]
          ~playstyle:
            [ !"Humans are the most adaptable and ambitious people among the common \
                races. They have widely varying tastes, morals, and customs in the many \
                different lands where they have settled. When they settle, though, they \
                stay: they build cities to last for the ages, and great kingdoms that \
                can persist for long centuries. An individual human might have a \
                relatively short life span, but a human nation or culture preserves \
                traditions with origins far beyond the reach of any single human's \
                memory."
            ; !"They live fully in the present -- making them well suited to the \
                adventuring life -- but also plan for the future, striving to leave a \
                lasting legacy. Individually and as a group, humans are adaptable \
                opportunists, and they stay alert to changing political and social \
                dynamics."
            ])
    in
    let traits =
      Description.(
        standard
          ~age:"Humans reach adulthood in their late teens and live less than a century."
          ~creature_type
          ~size:
            "Humans vary widely in height and build, from barely 5 feet to well over 6 \
             feet tall. Regardless of your position in that range, your size is Medium.")
      @ entries
    in
    Race.create
      ~name
      ~size
      ~speed
      ~ability
      ~creature_type
      ~height_and_weight
      ~languages
      ~info
      ~traits
  ;;

  let illyan =
    create
      ~name:"Illyan" (* Illyara *)
      ~speed:(Speed.create 30 ())
      ~ability:(Ability.create ~wis:2 ~con:1 ())
      ~entries:
        Description.
          [ "Darkvision"
            --> "You can see in dim light within 60 feet of you as if it were bright \
                 light, and in darkness as if it were dim light. You can't discern color \
                 in darkness, only shades of gray."
          ; "Hunter's Intuition"
            --> "When you make a Wisdom ({@skill Perception}) or Wisdom ({@skill \
                 Survival}) check, you can roll a {@dice d4} and add the number rolled \
                 to the ability check."
          ; ne
              "Illyan Heritage"
              Entry.
                [ !"You can cast the {@spell hunter's mark} spell with this trait. \
                    Starting at 3rd level, you can also cast the {@spell locate object} \
                    spell with it. Once you cast either spell with this trait, you can't \
                    cast that spell with it again until you finish a long rest. Wisdom \
                    is your spellcasting ability for these spells."
                ; !"In addition, if you have the Spellcasting or the Pact Magic class \
                    feature, the spells on the Illyan Heritage Spells table are added to \
                    the spell list of your spellcasting class."
                ; tbl
                    ~title:"Illyan Heritage Spells"
                    ~labels:[ "Spell Level"; "Spells" ]
                    ~style:[ "col-2 text-center"; "col-10" ]
                    ~rows:
                      [ [ "1st"; "{@spell faerie fire}, {@spell longstrider}" ]
                      ; [ "2nd"
                        ; "{@spell locate animals or plants}, {@spell locate object}"
                        ]
                      ; [ "3rd"; "{@spell clairvoyance}, {@spell speak with plants}" ]
                      ; [ "4th"; "{@spell divination}, {@spell locate creature}" ]
                      ; [ "5th"; "{@spell commune with nature}" ]
                      ]
                ]
          ]
  ;;

  let oreskian =
    create
      ~name:"Oreskian" (* Oresk *)
      ~speed:(Speed.create 30 ())
      ~ability:(Ability.create ~wis:2 ~dex:1 ())
      ~entries:
        Description.
          [ "Wild Intuition"
            --> "When you make a Wisdom ({@skill Animal Handling}) or Intelligence \
                 ({@skill Nature}) check, you can roll a {@dice d4} and add the number \
                 rolled to the ability check."
          ; "The Bigger They Are"
            --> "Starting at 3rd level, you can target a beast or monstrosity when you \
                 cast {@spell animal friendship} or {@spell speak with animals}, \
                 provided the creature's Intelligence score is 3 or lower."
          ; ne
              "Oreskian Heritage"
              Entry.
                [ !"You can cast the {@spell animal friendship} and {@spell speak with \
                    animals} spells with this trait, requiring no material component. \
                    Once you cast either spell with this trait, you can't cast that \
                    spell with it again until you finish a short or long rest. Wisdom is \
                    your spellcasting ability for these spells."
                ; !"In addition, if you have the Spellcasting or the Pact Magic class \
                    feature, the spells on the Oreskian Heritage Spells table are added \
                    to the spell list of your spellcasting class."
                ; tbl
                    ~title:"Oreskian Heritage Spells"
                    ~labels:[ "Spell Level"; "Spells" ]
                    ~style:[ "col-2 text-center"; "col-10" ]
                    ~rows:
                      [ [ "1st"
                        ; "{@spell animal friendship}, {@spell speak with animals}"
                        ]
                      ; [ "2nd"; "{@spell beast sense}, {@spell calm emotions}" ]
                      ; [ "3rd"; "{@spell beacon of hope}, {@spell conjure animals}" ]
                      ; [ "4th"; "{@spell aura of life}, {@spell dominate beast}" ]
                      ; [ "5th"; "{@spell awaken}" ]
                      ]
                ]
          ]
  ;;

  let el_rasaran =
    create
      ~name:"El-Rasaran" (* El-Rasar *)
      ~speed:(Speed.create 35 ())
      ~ability:(Ability.create ~dex:2 ~int_:1 ())
      ~entries:
        Description.
          [ "Courier's Speed" --> "Your base walking speed increases to 35 feet."
          ; "Intuitive Motion"
            --> "When you make a Dexterity ({@skill Acrobatics}) check or any ability \
                 check to operate or maintain a land vehicle, you can roll a {@dice d4} \
                 and add the number rolled to the ability check."
          ; "The Bigger They Are"
            --> "Starting at 3rd level, you can target a beast or monstrosity when you \
                 cast {@spell animal friendship} or {@spell speak with animals}, \
                 provided the creature's Intelligence score is 3 or lower."
          ; ne
              "El-Rasaran Heritage"
              Entry.
                [ !"You can cast the {@spell misty step} spell once with this trait, and \
                    you regain the ability to cast it when you finish a long rest. \
                    Dexterity is your spellcasting ability for this spell."
                ; !"In addition, if you have the Spellcasting or the Pact Magic class \
                    feature, the spells on the El-Rasaran Heritage Spells table are \
                    added to the spell list of your spellcasting class."
                ; tbl
                    ~title:"El-Rasaran Heritage Spells"
                    ~labels:[ "Spell Level"; "Spells" ]
                    ~style:[ "col-2 text-center"; "col-10" ]
                    ~rows:
                      [ [ "1st"; "{@spell expeditious retreat}, {@spell jump}" ]
                      ; [ "2nd"; "{@spell misty step}, {@spell pass without trace}" ]
                      ; [ "3rd"; "{@spell blink}, {@spell phantom steed}" ]
                      ; [ "4th"; "{@spell dimension door}, {@spell freedom of movement}" ]
                      ; [ "5th"; "{@spell teleportation circle}" ]
                      ]
                ]
          ]
  ;;

  let brejvasaran =
    create
      ~name:"Brejvasaran" (* Brejvasar *)
      ~speed:(Speed.create 30 ())
      ~ability:(Ability.create ~str:2 ~int_:1 ())
      ~entries:
        Description.
          [ "Natural Talent"
            --> "When you make an Intelligence ({@skill Investigation}) check or an \
                 ability check using {@item thieves' tools|PHB}, you can roll a {@dice \
                 d4} and add the number rolled to the ability check."
          ; ne
              "Brejvasaran Heritage"
              Entry.
                [ !"You can cast the {@spell alarm} and {@spell mage armor} spells with \
                    this trait. Starting at 3rd level, you can also cast the {@spell \
                    arcane lock} spell with it. Once you cast any of these spells with \
                    this trait, you can't cast that spell with it again until you finish \
                    a long rest. Intelligence is your spellcasting ability for these \
                    spells, and you don't need material components for them when you \
                    cast them with this trait."
                ; !"In addition, if you have the Spellcasting or the Pact Magic class \
                    feature, the spells on the Brejvasaran Heritage Spells table are \
                    added to the spell list of your spellcasting class."
                ; tbl
                    ~title:"Brejvasaran Heritage Spells"
                    ~labels:[ "Spell Level"; "Spells" ]
                    ~style:[ "col-2 text-center"; "col-10" ]
                    ~rows:
                      [ [ "1st"; "{@spell alarm}, {@spell armor of Agathys}" ]
                      ; [ "2nd"; "{@spell arcane lock}, {@spell knock}" ]
                      ; [ "3rd"; "{@spell glyph of warding}, {@spell magic circle}" ]
                      ; [ "4th"
                        ; "{@spell Leomund's secret chest}, {@spell Mordenkainen's \
                           faithful hound}"
                        ]
                      ; [ "5th"; "{@spell antilife shell}" ]
                      ]
                ]
          ]
  ;;

  let saloren =
    create
      ~name:"Saloren" (* Salores *)
      ~speed:(Speed.create 30 ())
      ~ability:(Ability.create ~int_:2 ~wis:1 ())
      ~entries:
        Description.
          [ "Gifted Scribe"
            --> "When you make an Intelligence ({@skill History}) check or an ability \
                 check using {@item calligrapher's supplies|PHB}, you can roll a {@dice \
                 d4} and add the number rolled to the ability check."
          ; ne
              "Saloren Heritage"
              Entry.
                [ !"You know the {@spell message} cantrip. You can also cast {@spell \
                    comprehend languages} once with this trait, and you regain the \
                    ability to cast it when you finish a short or long rest. Starting at \
                    3rd level, you can cast the {@spell magic mouth} spell with this \
                    trait, and you regain the ability to cast it when you finish a long \
                    rest. Intelligence is your spellcasting ability for these spells."
                ; !"In addition, if you have the Spellcasting or the Pact Magic class \
                    feature, the spells on the Saloren Heritage Spells table are added \
                    to the spell list of your spellcasting class."
                ; tbl
                    ~title:"Saloren Heritage Spells"
                    ~labels:[ "Spell Level"; "Spells" ]
                    ~style:[ "col-2 text-center"; "col-10" ]
                    ~rows:
                      [ [ "1st"
                        ; "{@spell comprehend languages}, {@spell illusory script}"
                        ]
                      ; [ "2nd"; "{@spell animal messenger}, {@spell silence}" ]
                      ; [ "3rd"; "{@spell sending}, {@spell tongues}" ]
                      ; [ "4th"; "{@spell arcane eye}, {@spell confusion}" ]
                      ; [ "5th"; "{@spell dream}" ]
                      ]
                ]
          ]
  ;;

  let athrean =
    create
      ~name:"Athrean" (* Athrea *)
      ~speed:(Speed.create 30 ())
      ~ability:(Ability.create ~cha:2 ~dex:1 ())
      ~entries:
        Description.
          [ "Hospitable"
            --> "When you make a Charisma ({@skill Persuasion}) check or an ability \
                 check involving {@item brewer's supplies|PHB} or {@item cook's \
                 utensils|PHB}, you can roll a {@dice d4} and add the number rolled to \
                 the ability check."
          ; ne
              "Athrean Heritage"
              Entry.
                [ !"You know the {@spell prestidigitation} cantrip. You can also cast \
                    the {@spell purify food and drink} and {@spell unseen servant} \
                    spells with this trait. Once you cast either spell with this trait, \
                    you can't cast that spell with it again until you finish long rest. \
                    Charisma is your spellcasting ability for these spells."
                ; !"In addition, if you have the Spellcasting or the Pact Magic class \
                    feature, the spells on the Athrean Heritage Spells table are added \
                    to the spell list of your spellcasting class."
                ; tbl
                    ~title:"Athrean Heritage Spells"
                    ~labels:[ "Spell Level"; "Spells" ]
                    ~style:[ "col-2 text-center"; "col-10" ]
                    ~rows:
                      [ [ "1st"; "{@spell goodberry}, {@spell sleep}" ]
                      ; [ "2nd"; "{@spell aid}, {@spell calm emotions}" ]
                      ; [ "3rd"
                        ; "{@spell create food and water}, {@spell Leomund's tiny hut}"
                        ]
                      ; [ "4th"
                        ; "{@spell aura of purity}, {@spell Mordenkainen's private \
                           sanctum}"
                        ]
                      ; [ "5th"; "{@spell hallow}" ]
                      ]
                ]
          ]
  ;;

  let race = [ illyan; oreskian; el_rasaran; brejvasaran; saloren; athrean ]
end

module Dwarf = struct
  let create ~name ~ability ~playstyle ~entries =
    let name = [%string "Dwarf (%{name})"] in
    let creature_type = Creature_type.humanoid in
    let size = Size.small in
    let speed = Speed.create 25 () in
    let languages = Language.[ common; dwarvish ] in
    let height_and_weight = Height_and_weight.create ~h:44 ~mh:"2d4" ~w:115 ~mw:"2d6" in
    let info =
      Entry.(
        info
          ~name:"Dwarf"
          ~title:"Natives of the Deep"
          ~intro:
            "Kingdoms rich in ancient grandeur, halls carved into the roots of \
             mountains, the echoing of picks and hammers in deep mines and blazing \
             forges, and a commitment to clan and tradition -- these common threads \
             unite all dwarves."
          ~physical_qualities:
            [ !"Bold and hardy, dwarves are known as skilled warriors, miners, and \
                workers of stone and metal. Though they stand well under 5 feet tall, \
                dwarves are so broad and compact that they can weigh as much as a human \
                standing nearly two feet taller. Their courage and endurance are also \
                easily a match for any of the larger folk."
            ; !"Dwarven skin ranges from deep brown to a paler hue tinged with red, but \
                the most common shades are light brown or deep tan, like certain tones \
                of earth."
            ; !"Their hair, worn long but in simple styles, is usually black, gray, or \
                brown, though paler dwarves often have red hair. Male dwarves value \
                their beards highly and groom them carefully."
            ]
          ~playstyle:
            ([ !"Dwarves are solid and enduring like the mountains they love, weathering \
                 the passage of decades with stoic endurance and little change. They \
                 respect the traditions of their clans, tracing their ancestry back to \
                 the founding of their most ancient strongholds in the youth of the \
                 world, and don't abandon those traditions lightly. "
             ; !"Individual dwarves are determined and loyal, true to their word and \
                 decisive in action, sometimes to the point of stubbornness. Many \
                 dwarves have a strong sense of justice, and they are slow to forget \
                 wrongs they have suffered. A wrong done to one dwarf is a wrong done to \
                 the dwarf's entire clan, so what begins as one dwarf's hunt for \
                 vengeance can become a full-blown clan feud.."
             ]
            @ playstyle))
    in
    let traits =
      Description.(
        standard
          ~age:
            "Dwarves mature at the same rate as humans, but they're considered young \
             until they reach the age of 50. At most, they live about 200 years."
          ~creature_type
          ~size:
            "Dwarves stand between 4 and 5 feet tall and average about 150 pounds. Your \
             size is Small."
        @ [ "Hardy Speed" --> "Your speed is not reduced by wearing heavy armor."
          ; "Dwarven Resilience"
            --> "You have advantage on saving throws against poison, and you have \
                 resistance against poison damage."
          ; "Dwarven Combat Training"
            --> "You have proficiency with the {@item battleaxe|phb}, {@item \
                 handaxe|phb}, {@item light hammer|phb}, and {@item warhammer|phb}."
          ; "Master of Craft"
            --> "You gain proficiency with the artisan's tools of your choice: {@item \
                 smith's tools|phb}, {@item brewer's supplies|phb}, or {@item mason's \
                 tools|phb}."
          ]
        @ entries)
    in
    Race.create
      ~name
      ~size
      ~speed
      ~ability
      ~creature_type
      ~height_and_weight
      ~languages
      ~info
      ~traits
  ;;

  let deepkin =
    create
      ~name:"Deepkin"
      ~ability:(Ability.create ~str:2 ~con:1 ())
      ~playstyle:
        Entry.
          [ "Deepkin"
            --> "The deepkin are named as such due to their proximity to the Deep. The \
                 exposure to strong, corrupting elements over time have twisted the \
                 deepkin, giving them a slight resistance to magic but robbing their \
                 eyes in the process."
          ]
      ~entries:
        Description.
          [ "Deepsight"
            --> "The Deep stole your eyes at birth, but your people have learned to feel \
                 the essence of the world around them anyway. You have blindsight in a \
                 120 foot radius around you."
          ; "Inherent Immunity"
            --> "When you fail a saving throw against a spell or other magical effect, \
                 you can reroll the save. You must use the new roll. Once you use this \
                 trait, you can;t use it again until you finish and short or long rest."
          ]
  ;;

  let cogwright =
    create
      ~name:"Cogwright"
      ~ability:(Ability.create ~int_:2 ~str:1 ())
      ~playstyle:
        Entry.
          [ "Cogwright"
            --> "The cogwright live in the chasms suspended above the Deep, shielding \
                 themselves from its influence with specially crafted metal walls and \
                 travelling from stalactite to stalactite with custom ropecasters. They \
                 pride themselves on their ingenuity, and their engineers and inventors \
                 are more respected than any other occupation."
          ]
      ~entries:
        Description.
          [ "Expanded Toolkit"
            --> "You can choose either the {@item jeweler's tools|phb}, {@item thieves' \
                 tools|phb}, or the {@item tinker's tools|phb}, in place of one of the \
                 tools listed in the Master of the Craft trait."
          ; "Setup"
            --> "As a bonus action, you give yourself advantage on your next attack roll \
                 on the current turn. You can use this bonus action only if you haven't \
                 moved during this turn, and after you use the bonus action, your speed \
                 is 0 until the end of the current turn."
          ]
  ;;

  let scourger =
    create
      ~name:"Scourger"
      ~ability:(Ability.create ~int_:2 ~str:1 ())
      ~playstyle:
        Entry.
          [ "Scourger"
            --> "The Scourgers entered the Deep centuries ago to gain an edge in their \
                 war against their spellcasting brethren. When they emerged they brought \
                 with them a powerful ability that allowed them victory in their war, \
                 but cost them their last sliver of humanity in response."
          ]
      ~entries:
        Description.
          [ "Pierce the Veil"
            --> "Whenever damage you deal forces a creature to make a Constitution \
                 saving throw to maintain concentration on a spell, you can force them \
                 to subtract your proficiency bonus from their roll."
          ; "Magehunter's Magic"
            --> "You can cast {@spell detect magic} once with this trait. When you reach \
                 3rd-level you can cast {@spell enlarge/reduce} once with this trait, \
                 targeting only yourself and only selecting the Enlarge option. You \
                 regain the ability to cast these spells with this trait when you finish \
                 a long rest. Constitution is your spellcasting ability for these \
                 spells, and when you cast them with this trait they require no material \
                 components."
          ]
  ;;

  let race = [ deepkin; cogwright; scourger ]
end

module Senfir = struct
  let race =
    [ (let name = [%string "Senfir"] in
       let creature_type = Creature_type.fey in
       let ability = Ability.create ~wis:2 ~cha:1 () in
       let size = Size.large in
       let speed = Speed.create 30 () in
       let languages = Language.[ common; primordial ] in
       let height_and_weight =
         Height_and_weight.create ~h:76 ~mh:"2d6" ~w:180 ~mw:"1d4"
       in
       let info =
         Entry.(
           info
             ~name:"Senfir"
             ~title:"Humble Guardians of the Wood"
             ~intro:
               "Senfir tribes cloister in remote forest strongholds, preferring to spend \
                their days in quiet harmony with the woods. However, when provoked, \
                senfir demonstrate formidable skills with weapons and druidic magic."
             ~physical_qualities:
               [ !"Senfir were originally human and praelin offspring, and their \
                   features reflect it. Senfir tend to have the pale-to-brown skin tones \
                   that humans have, but striations of red and black pattern their skin, \
                   and their hair is usually some shade of red."
               ; !"Senfir are taller than many other races, but their builds are more \
                   lithe and graceful than the terrans that they originated from. As \
                   they moved away from the mountains into the forests at their base, \
                   senfir are at home climbing through trees and navigating dense \
                   underbrush"
               ]
             ~playstyle:[ !"" ])
       in
       let traits =
         Description.(
           standard
             ~age:
               "Senfir live short lives than praelin do, with their elders just barely \
                hitting 70."
             ~creature_type
             ~size:
               "Senfir are usually between seven and eight feel tall, and weigh around \
                240 to 300 pounds. Your size is Large."
           @ [ "Powerful Build"
               --> "You count as one size larger when determining your carrying capacity \
                    and the weight you can push, drag, or lift."
             ; "Senfir Magic"
               --> "You can cast {@spell fear} and {@spell heroism} with this trait, \
                    using Wisdom as your spellcasting ability for them. Once you cast \
                    either spell, you can't cast it again with this trait until you \
                    finish a short or long rest."
             ; "Speech of Beast and Leaf"
               --> "You have the ability to communicate in a limited manner with beasts \
                    and plants. They can understand the meaning of your words, though \
                    you have no special ability to understand them in return. You have \
                    advantage on all Charisma checks you make to influence them."
             ; "Wild Child"
               --> "You can force a burst of adrenaline when beset by enemies. As a \
                    bonus action you can enter a frenzy -- until the beginning of your \
                    next turn you have advantage on melee attack rolls and resistance to \
                    non-magical bludgeoning, piercing, and slashing damage. Once you use \
                    this ability you may not do so again until after you have completed \
                    a long rest."
             ])
       in
       Race.create
         ~name
         ~size
         ~speed
         ~ability
         ~creature_type
         ~height_and_weight
         ~languages
         ~info
         ~traits)
    ]
  ;;
end

let json =
  `List
    ([ Beastfolk.race
     ; Deva.race
     ; Dray.race
     ; Dwarf.race
     ; Faceless.race
     ; Goblin.race
     ; Human.race
     ; Lycan.race
     ; Praelin.race
     ; Senfir.race
     ; Shade.race
     ]
    |> List.concat
    |> List.sort ~compare:(fun lhs rhs -> String.compare (Race.name lhs) (Race.name rhs))
    |> List.map ~f:Race.to_json)
;;
