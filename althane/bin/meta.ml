open! Core
open! Async
open! Gen_lib

let json =
  let timestamp = Time.format (Time.now ()) ~zone:(force Time.Zone.local) "%s" in
  `Assoc
    [ ( "sources"
      , `List
          [ `Assoc
              [ "json", `String "ALT"
              ; "abbreviation", `String "ALT"
              ; "full", `String "Locality 15A-4C Compendium: Althane"
              ; "authors", `List [ `String "Josh Bassin" ]
              ; "convertedBy", `List [ `String "Josh Bassin" ]
              ; "version", `String timestamp
              ; "color", `String "A45EE5"
              ]
          ] )
    ; "dateAdded", `Int 1629943742
    ; "dateLastModified", `Int (Int.of_string timestamp)
    ]
;;
