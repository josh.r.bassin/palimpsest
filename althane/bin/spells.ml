open! Core
open! Async
open! Gen_lib

module Hedge_mage = struct
  let c = Spell.create ~level:0 ~classes:[]

  let spells =
    [ c
        ~name:"Arc Blade"
        ~school:School.evocation
        ~aspect:Aspect.expansion
        ~components:Component.(create ~v:() ~m:(`Plain "a melee weapon") ())
        ~time:Casting_time.(action)
        ~range:Range.(self)
        ~duration:Duration.(create ~concentration:false ~type_:Type.instant)
        ~entries:
          Entry.
            [ !"As part of the action used to cast this spell, you must make a melee \
                attack with a weapon against one creature within the spell’s range, \
                otherwise the spell fails. On a hit, the target suffers the weapon \
                attack’s normal effects, except that any damage dealt by the attack is \
                lightning damage instead of its normal type. Additionally, an arc of \
                lightning jumps to a creature you choose within 5 feet of the target, \
                dealing {@dice 1d6} lightning damage."
            ]
        ~entries_at_higher_level:
          Entry.
            [ !"This spell’s damage increases when you reach certain levels. At 5th \
                level, the melee attack deals an additional 1d8 lightning damage and \
                secondary damage deals an additional 1d6 lightning damage to their \
                targets. Both damage rolls increase by one die at 11th level ({@dice \
                2d8} and {@dice 3d6}), and 17th level ({@dice 3d8} and {@dice 4d6})."
            ]
        ()
    ; c
        ~name:"Burning Blade"
        ~school:School.evocation
        ~aspect:Aspect.expansion
        ~components:Component.(create ~v:() ~m:(`Plain "a melee weapon") ())
        ~time:Casting_time.(action)
        ~range:Range.(self)
        ~duration:Duration.(create ~concentration:false ~type_:Type.(round 1))
        ~entries:
          Entry.
            [ !"As part of the action used to cast this spell, you must make a melee \
                attack with a weapon against one creature within the spell’s range, \
                otherwise the spell fails. On a hit, the target suffers the weapon \
                attack’s normal effects, except that any damage dealt by the attack is \
                fire damage instead of its normal type. Additionally, embers whirl in \
                the target’s space. Until the start of your next turn, when a creature \
                enters the space for the first time or ends its turn there, you can use \
                your reaction to deal {@dice 1d6} fire damage to the creature, ending \
                the spell."
            ]
        ~entries_at_higher_level:
          Entry.
            [ !"This spell’s damage increases when you reach certain levels. At 5th \
                level, the melee attack deals an additional 1d6 fire damage to the \
                target on a hit and the secondary damage deals an additional {@dice 1d6} \
                fire damage to its target. Both damage rolls increase by one die at 11th \
                level ({@dice 2d6} and {@dice 3d6}) and 17th level ({@dice 3d6} and \
                {@dice 4d6})."
            ]
        ()
    ; c
        ~name:"Card Trick"
        ~school:School.transmutation
        ~aspect:Aspect.expansion
        ~components:
          Component.(create ~s:() ~v:() ~m:(`Plain "a deck of playing cards") ())
        ~time:Casting_time.(action)
        ~range:Range.(point 60)
        ~duration:Duration.(create ~concentration:false ~type_:Type.(instant))
        ~entries:
          Entry.
            [ !"With a flash of your hands, you fling a playing card charged with energy \
                at your opponents. Choose whether you make a spell attack roll or for \
                the target to make a Dexterity saving throw. On a hit or a failed saving \
                throw, the target takes {@dice 1d6} force damage. "
            ]
        ~entries_at_higher_level:
          Entry.
            [ !"This spell’s damage increases by 1d6 when you reach 5th level ({@dice \
                2d6}), 11th level ({@dice 3d6}), and 17th level ({@dice 4d6}). "
            ]
        ()
    ; c
        ~name:"Caustic Blade"
        ~school:School.evocation
        ~aspect:Aspect.decay
        ~components:Component.(create ~v:() ~m:(`Plain "a melee weapon") ())
        ~time:Casting_time.(action)
        ~range:Range.(self)
        ~duration:Duration.(create ~concentration:false ~type_:Type.(instant))
        ~entries:
          Entry.
            [ !"As part of the action used to cast this spell, you must make a melee \
                attack with a weapon against one creature within the spell’s range, \
                otherwise the spell fails. On a hit, the target suffers the weapon \
                attack’s normal effects, except that any damage dealt by the attack is \
                acid damage instead of its normal type. If you miss by 3 or less, acid \
                splashes on the target, and you instead deal {@dice 1d8} acid damage."
            ]
        ~entries_at_higher_level:
          Entry.
            [ !"This spell’s damage increases when you reach certain levels. At 5th \
                level, the melee attack deals an additional 1d8 acid damage to the \
                target on a hit, and the acid damage dealt on a miss increases to {@dice \
                2d8}. Both damage rolls increase by one die at 11th level ({@dice 2d8} \
                and {@dice 3d8}) and 17th level ({@dice 3d8} and {@dice 4d8})."
            ]
        ()
    ; c
        ~name:"Cheat"
        ~school:School.divination
        ~aspect:Aspect.stability
        ~components:Component.(create ~s:() ~m:(`Plain "a weighted die") ())
        ~time:Casting_time.(bonus)
        ~range:Range.(self)
        ~duration:Duration.(create ~concentration:false ~type_:Type.(minute 10))
        ~entries:
          Entry.
            [ !"You subtly twist your fingers and fate seems to follow suit. For the \
                duration, you can reroll any ability check you make to play nonmagical \
                games of skill. Therefore, this spell could influence a game of poker, \
                but not the result of a deck of many things."
            ]
        ()
    ; c
        ~name:"Cryptogram"
        ~school:School.conjuration
        ~aspect:Aspect.wisdom
        ~components:
          Component.(create ~v:() ~s:() ~m:(`Plain "a small written message") ())
        ~time:Casting_time.(action)
        ~range:Range.(unlimited)
        ~duration:Duration.(create ~concentration:false ~type_:Type.(instant))
        ~entries:
          Entry.
            [ !"You send a small scroll with a short message to a creature of your \
                choice. The recipient must be a creature known to you and also be on the \
                same plane of existence as you. This scroll will hover in front of the \
                recipient, drop into their pocket, or appear sitting on something \
                nearby. The scroll’s message can be up to 8 characters long (spaces \
                count as characters). You can send only one scroll to a single target \
                each day."
            ]
        ()
    ; c
        ~name:"Force Buckler"
        ~school:School.abjuration
        ~aspect:Aspect.stability
        ~components:
          Component.(
            create ~v:() ~s:() ~m:(`Cost ("a steel gauntlet worth at least 5gp", 5)) ())
        ~time:Casting_time.(action)
        ~range:Range.(self)
        ~duration:Duration.(create ~concentration:false ~type_:Type.(round 1))
        ~entries:
          Entry.
            [ !"You summon a translucent, yet visible, field of force which springs \
                forth from the prepared gauntlet. Until the beginning of your next turn, \
                this shield grants you a +2 bonus to your Armor Class, as if you were \
                wielding a shield. This spell ends early if you are hit by an attack."
            ]
        ()
    ; c
        ~name:"Force Dart"
        ~school:School.evocation
        ~aspect:Aspect.creation
        ~components:
          Component.(
            create ~v:() ~s:() ~m:(`Cost ("a steel gauntlet worth at least 5gp", 5)) ())
        ~time:Casting_time.(action)
        ~range:Range.(self)
        ~duration:Duration.(create ~concentration:false ~type_:Type.(instant))
        ~entries:
          Entry.
            [ !"You fling a dart of magical force at a creature or object within range. \
                Make a ranged spell attack against the target. On a hit, the target \
                takes {@dice 1d10} force damage."
            ]
        ~entries_at_higher_level:
          Entry.
            [ !"This spell’s damage increases by 1d10 when you reach 5th level ({@dice \
                2d10}), 11th level ({@dice 3d10}), and 17th level ({@dice 4d10})."
            ]
        ()
    ; c
        ~name:"Force Weapon"
        ~school:School.evocation
        ~aspect:Aspect.creation
        ~components:
          Component.(
            create ~v:() ~m:(`Cost ("a steel gauntlet worth at least 5gp", 5)) ())
        ~time:Casting_time.(action)
        ~range:Range.(radius 5)
        ~duration:Duration.(create ~concentration:false ~type_:Type.(round 1))
        ~entries:
          Entry.
            [ !"You conjure a blade of magical force in the air, which lashes out at \
                your foes. Make a melee spell attack. On a hit, you deal {@dice 1d10} \
                force damage. The blade remains in existence for a short time; until the \
                beginning of your next turn, you can make a single strike with your \
                mystical blade as an opportunity attack."
            ]
        ~entries_at_higher_level:
          Entry.
            [ !"You can make 1 additional attack on your turn at 5th level (2 attacks), \
                at 11th level (3 attacks), and at 17th level (4 attacks)."
            ]
        ()
    ; c
        ~name:"Frigid Blade"
        ~school:School.evocation
        ~aspect:Aspect.decay
        ~components:Component.(create ~v:() ~m:(`Plain "a melee weapon") ())
        ~time:Casting_time.(action)
        ~range:Range.(self)
        ~duration:Duration.(create ~concentration:false ~type_:Type.(round 1))
        ~entries:
          Entry.
            [ !"As part of the action used to cast this spell, you must make a melee \
                attack with a weapon against one creature within the spell’s range, \
                otherwise the spell fails. On a hit, the attack does damage as normal, \
                except that the entire attack deals cold damage instead of its normal \
                type. Additionally, the target is covered in a brittle frost until the \
                start of your next turn. If the target willingly moves before then, you \
                can use your reaction to deal {@dice 1d8} cold damage to the target, \
                ending the spell."
            ]
        ~entries_at_higher_level:
          Entry.
            [ !"At 5th level, the melee attack and secondary damage each deal an \
                additional {@dice 1d8} cold damage. Both damage rolls increase by 1d8 at \
                11th level ({@dice 2d8}/{@dice 3d8}), and 17th level ({@dice 3d8}/{@dice \
                4d8})."
            ]
        ()
    ; c
        ~name:"Lightning Surge"
        ~school:School.evocation
        ~aspect:Aspect.expansion
        ~components:Component.(create ~v:() ~s:() ~m:(`Plain "two wires of copper") ())
        ~time:Casting_time.(action)
        ~range:Range.(radius 5)
        ~duration:Duration.(create ~concentration:false ~type_:Type.(instant))
        ~entries:
          Entry.
            [ !"You emit a dazzling array of short lightning bolts in all directions. \
                All other creatures within 5 feet of you must succeed on a Dexterity \
                saving throw or take {@dice 1d6} lightning damage."
            ]
        ~entries_at_higher_level:
          Entry.
            [ !"This spell’s damage increases by 1d6 when you reach 5th level ({@dice \
                2d6}), 11th level ({@dice 3d6}), and 17th level ({@dice 4d6})."
            ]
        ()
    ; c
        ~name:"Magic Daggers"
        ~school:School.conjuration
        ~aspect:Aspect.creation
        ~components:Component.(create ~v:() ~s:() ())
        ~time:Casting_time.(action)
        ~range:Range.(point 60)
        ~duration:Duration.(create ~concentration:false ~type_:Type.(instant))
        ~entries:
          Entry.
            [ !"With a flourish, you conjure a throwing dagger of magical force out of \
                thin air and flick it from your wrist at a target you can see. Make a \
                ranged spell attack roll against a creature within range. On a hit, the \
                target takes {@dice 1d6} magical piercing damage. The dagger vanishes \
                after the attack."
            ]
        ~entries_at_higher_level:
          Entry.
            [ !"At higher levels, you conjure more daggers out of force and make \
                additional attacks: two daggers at 5th level, three daggers at 11th \
                level, and four daggers at 17th level. You can use the daggers to attack \
                the same target or different ones. Make a separate attack roll for each \
                dagger."
            ]
        ()
    ; c
        ~name:"Phantom Grapnel"
        ~school:School.conjuration
        ~aspect:Aspect.individualism
        ~components:Component.(create ~v:() ~s:() ())
        ~time:Casting_time.(action)
        ~range:Range.(point 30)
        ~duration:Duration.(create ~concentration:false ~type_:Type.(instant))
        ~entries:
          Entry.
            [ !"You conjure a chain and hook made of magical force, which you propel at \
                a creature or unoccupied space you can see within range. When you target \
                a space or a creature of Huge size or larger, your grapnel pulls you to \
                that target in a straight line. You provoke opportunity attacks for this \
                movement as normal. When you target a creature of Large size or smaller, \
                you pull the target up to 10 feet towards you. A creature can make a \
                Strength saving throw to resist this movement."
            ]
        ()
    ; c
        ~name:"Quickstep"
        ~school:School.transmutation
        ~aspect:Aspect.individualism
        ~components:Component.(create ~v:() ())
        ~time:Casting_time.(bonus)
        ~range:Range.(self)
        ~duration:Duration.(create ~concentration:false ~type_:Type.(round 1))
        ~entries:
          Entry.
            [ !"You call upon your inner reserves to give you a brief flash of speed. \
                When you cast this spell, your base movement speed increases by 10 feet \
                until the beginning of your next turn."
            ]
        ()
    ; c
        ~name:"Sonic Pulse"
        ~school:School.evocation
        ~aspect:Aspect.expansion
        ~components:Component.(create ~v:() ~s:() ())
        ~time:Casting_time.(action)
        ~range:Range.(point 60)
        ~duration:Duration.(create ~concentration:false ~type_:Type.(round 1))
        ~entries:
          Entry.
            [ !"You compress a thunderous boom into an invisible ball and project it at \
                a creature you can see within range. The target must succeed on a \
                Constitution saving throw, or it takes {@dice 1d8} thunder damage and is \
                deafened until the start of your next turn."
            ; !"If the spell’s target is within 10 feet of you, this spell’s damage \
                becomes d10s, instead of d8s."
            ]
        ~entries_at_higher_level:
          Entry.
            [ !"This spell’s damage increases by 1d8 when you reach 5th level ({@dice \
                2d8}), 11th level ({@dice 3d8}), and 17th level ({@dice 4d8})."
            ]
        ()
    ; c
        ~name:"Springheel"
        ~school:School.transmutation
        ~aspect:Aspect.individualism
        ~components:Component.(create ~v:() ())
        ~time:Casting_time.(bonus)
        ~range:Range.(self)
        ~duration:Duration.(create ~concentration:false ~type_:Type.(round 1))
        ~entries:
          Entry.
            [ !"You flood magic into your legs, allowing you to bound high into the air \
                from a standstill. When you cast this spell, your jump distance \
                increases 10 feet until the beginning of your next turn, and you can \
                make a running high jump or a running long jump without a running start."
            ]
        ()
    ; c
        ~name:"Thunderous Distortion"
        ~school:School.evocation
        ~aspect:Aspect.weight
        ~components:Component.(create ~v:() ~s:() ())
        ~time:Casting_time.(action)
        ~range:Range.(cone 10)
        ~duration:Duration.(create ~concentration:false ~type_:Type.(instant))
        ~entries:
          Entry.
            [ !"You produce a distorted wave of noise in a 10-foot cone, which can be \
                heard up to 100 feet away. Each creature in that area must succeed a \
                Constitution saving throw, or take {@dice 1d6} thunder damage."
            ; !"An echo of this noise persists until the end of your next turn. If you \
                cast this spell again before the end of your next turn, its damage \
                becomes d8s, instead of d6s. "
            ]
        ~entries_at_higher_level:
          Entry.
            [ !"This spell’s damage increases by 1d6 when you reach 5th level ({@dice \
                2d6}), 11th level ({@dice 3d6}), and 17th level ({@dice 4d6})."
            ]
        ()
    ]
  ;;
end

let json =
  `List
    ([ Hedge_mage.spells ]
    |> List.concat
    |> List.sort ~compare:[%compare: Spell.t]
    |> List.map ~f:Spell.to_json)
;;
