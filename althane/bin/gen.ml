open! Core
open! Async
open! Gen_lib

let gen_command =
  Command.async
    ~summary:"Generate homebrew"
    (let%map_open.Command () = return () in
     fun () ->
       `Assoc
         [ "_meta", Meta.json
         ; "race", Races.json
         ; "class", Classes.class_json
         ; "classFeature", Classes.class_feature_json
         ; "subclass", Classes.subclass_json
         ; "subclassFeature", Classes.subclass_feature_json
         ; "optionalfeature", Opt_features.json
         ; "feat", Feats.json
         ; "spell", Spells.json
         ; "itemType", Objects.type_json
         ; "itemProperty", Objects.property_json
         ; "item", Objects.item_json
         ]
       |> Yojson.pretty_to_string
       |> print_endline
       |> return)
;;

let vestige_command =
  Command.async
    ~summary:"Generate homebrew"
    (let%map_open.Command () = return () in
     fun () ->
       let vestiges =
         List.map Opt_features.vestiges ~f:(fun vestige ->
             let name = Opt_feature.name vestige in
             let required_level = Opt_feature.level vestige |> Option.value ~default:1 in
             let features =
               Opt_feature.entries vestige
               |> Entry.Vestige.ingest_entries
               |> List.map ~f:(fun (title, entry) ->
                      `Assoc
                        [ "title", `String title
                        ; "entries", `List (List.map entry ~f:(fun e -> `String e))
                        ])
             in
             let traits =
               Opt_feature.entries vestige
               |> Entry.Vestige.ingest_traits
               |> List.map ~f:(fun (title, entry) ->
                      `Assoc
                        [ "title", `String title
                        ; "entries", `List (List.map entry ~f:(fun e -> `String e))
                        ])
             in
             `Assoc
               [ "name", `String name
               ; "required_level", `Int required_level
               ; "features", `List features
               ; "traits", `List traits
               ])
       in
       print_endline (Yojson.pretty_to_string (`List vestiges));
       return ())
;;

let () =
  Command.run
    (Command.group
       ~summary:"generate homebrew"
       [ "gen", gen_command; "vestige", vestige_command ])
;;
