open! Core
open! Async
open! Gen_lib
open! Feat

let feats =
  Entry.
    [ create
        ~name:"Bounty Hunter"
        ~entries:
          [ !"When there's a price on someone's head, you make it your business to \
              collect. You gain the following benefits:"
          ; lh
              [ !!"Increase your Wisdom score by 1, to a maximum of 20."
              ; !!"You perfectly remember the name and face of every person you've ever \
                   met."
              ; !!"You can use your action to mark a creature you can see within 120 \
                   feet of you for 1 hour. You have advantage on any Wisdom (Perception) \
                   or Wisdom (Survival) check you make to find the marked creature. \
                   Additionally, as long as the creature is marked, you can discern \
                   whether or not the creature has passed through a location."
              ; !!"Whenever you reduce a target to 0 hit points with any attack you make \
                   or spell you cast, you can choose to knock the creature out instead."
              ]
          ]
    ]
;;

let json = `List (List.sort feats ~compare:[%compare: Feat.t] |> List.map ~f:Feat.to_json)
