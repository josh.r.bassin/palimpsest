open! Core
open! Async
open! Gen_lib

module Gunslinger = struct
  let simple_firearm ~damage =
    Item_type.create
      ~name:"Simple Firearm"
      ~type_:"Simple Firearm"
      ~metadata:
        [ "weaponCategory", `String "simple"
        ; "firearm", `Bool true
        ; "weapon", `Bool true
        ; "dmg1", `String damage
        ; "dmgType", `String "P"
        ]
      ~entries:
        Entry.
          [ !"This weapon propels projectiles at great speed by detonating a small \
              quantity of arcane powder -- called gunpowder. The sound of the gunshot is \
              audible out half a mile."
          ]
      ()
  ;;

  let martial_firearm ~damage =
    Item_type.create
      ~name:"Martial Firearm"
      ~type_:"Martial Firearm"
      ~metadata:
        [ "weaponCategory", `String "martial"
        ; "firearm", `Bool true
        ; "weapon", `Bool true
        ; "dmg1", `String damage
        ; "dmgType", `String "P"
        ]
      ~entries:
        Entry.
          [ !"This weapon propels projectiles at great speed by detonating a small \
              quantity of arcane powder -- called gunpowder. The sound of the gunshot is \
              audible out half a mile."
          ]
      ()
  ;;

  let types = [ simple_firearm ~damage:""; martial_firearm ~damage:"" ]

  let multi_fire =
    Item_property.create
      ~name:"Multi-fire"
      ~entries:
        Entry.
          [ !"When you make an attack with this weapon on your turn, you can choose to \
              make two attacks with disadvantage instead. These attacks always have \
              disadvantage, regardless of circumstance."
          ]
      ()
  ;;

  let dry =
    Item_property.create
      ~name:"Dry"
      ~entries:
        Entry.
          [ !"If this weapon is ever submerged in water or doused with a significant \
              quantity of water, it jams. A jammed firearm can't be used to make an \
              attack until a creature uses its action to clear the weapon malfunction."
          ]
      ()
  ;;

  let scatter ~damage =
    Item_property.create
      ~name:[%string "Scatter (%{damage})"]
      ~entries:
        Entry.
          [ ![%string
               "If you make an attack against a target that is within half this weapon’s \
                normal range, you deal %{damage} damage instead of the weapon’s normal \
                damage dice."]
          ]
      ()
  ;;

  let ultra_heavy =
    Item_property.create
      ~name:[%string "Ultra-heavy"]
      ~entries:
        Entry.
          [ ![%string
               "Weapons with the Ultra-heavy property have too much mass to be wielded \
                by the average person effectively. If you do not have a feature or feat, \
                such as the Gunslinger's Tank Creed, that gives you the ability to use \
                Ultra-heavy weapons, you make all attacks at disadvantage."]
          ]
      ()
  ;;

  let concealable =
    Item_property.create
      ~name:[%string "Concealable"]
      ~entries:
        Entry.
          [ ![%string
               "You have advantage on Dexterity (Sleight of Hand) checks made to hide \
                this weapon."]
          ]
      ()
  ;;

  let foregrip =
    Item_property.create
      ~name:[%string "Foregrip"]
      ~entries:
        Entry.
          [ ![%string
               "This weapon can be used with one or two hands. If used in two hands, its \
                normal and long ranges double."]
          ]
      ()
  ;;

  let sighted =
    Item_property.create
      ~name:[%string "Sighted"]
      ~entries:
        Entry.
          [ ![%string
               "This weapon has disadvantage on attack rolls made against targets within \
                20 feet."]
          ]
      ()
  ;;

  let explosive =
    Item_property.create
      ~name:[%string "Explosive"]
      ~entries:
        Entry.
          [ ![%string
               "When this weapon’s projectile hits a target, it explodes in a 5-foot \
                radius. The projectile can be fired at an unoccupied space within its \
                range. Each creature other than the target within the blast radius must \
                succeed on a DC 14 Dexterity saving throw, taking half the damage rolled \
                on a failed save or no damage on a successful one."]
          ]
      ()
  ;;

  let slow_reload =
    Item_property.create
      ~name:[%string "Slow Reload"]
      ~entries:
        Entry.
          [ ![%string
               "This weapon takes a considerable amount of effort to reload. Once you \
                make an attack with this weapon, it can't be used again to make an \
                attack until the end of your next turn."]
          ]
      ()
  ;;

  let scatter_2d8 = scatter ~damage:"2d8"

  let properties =
    [ dry
    ; scatter_2d8
    ; multi_fire
    ; ultra_heavy
    ; concealable
    ; foregrip
    ; sighted
    ; explosive
    ; slow_reload
    ]
  ;;

  let scattergun =
    Item.create
      ~name:"Hammerhead® .72 Scattergun"
      ~type_:(simple_firearm ~damage:"2d6")
      ~rarity:Rarity.uncommon
      ~properties:Item_property.[ scatter_2d8; reload 2; two_handed; range (40, 60) ]
      ~entries:
        Entry.
          [ !"A classic design, which loads two slugs or shells into separate barrels, \
              the scattergun trades ammo capacity and range for reliability and sheer \
              firepower."
          ]
  ;;

  let pepperbox =
    Item.create
      ~name:"Ashwright Armory® .38 Pepperbox"
      ~type_:(martial_firearm ~damage:"2d10")
      ~rarity:Rarity.very_rare
      ~properties:
        Item_property.[ reload 3; multi_fire; two_handed; ultra_heavy; range (80, 240) ]
      ~entries:
        Entry.
          [ !"A weapon infamous for its rate of fire, the pepperbox rotates its many \
              barrels, which fire in sequence, in order to manage the tremendous heat \
              from sustained automatic fire. This weapon is cumbersome, easily \
              recognizable, and utterly terrifying all at the same time."
          ]
  ;;

  let quill =
    Item.create
      ~name:"Hester & Sons® .22 Quill"
      ~type_:(simple_firearm ~damage:"2d4")
      ~rarity:Rarity.uncommon
      ~properties:Item_property.[ reload 10; light; range (40, 120) ]
      ~entries:
        Entry.
          [ !"Portable, reliable, and a rotating cylinder packed with 10 bullets, the \
              quill is an excellent firearm for self defense. Though it might struggle \
              to contend with a scattergun or albatross, the humble quill is more than \
              sufficient for city guard, and can be fired by a civilian in danger in a \
              pinch."
          ]
  ;;

  let trueshot =
    Item.create
      ~name:"Eagle-eye® .32 Trueshot"
      ~type_:(simple_firearm ~damage:"2d6")
      ~rarity:Rarity.uncommon
      ~properties:Item_property.[ reload 5; two_handed; range (80, 240) ]
      ~entries:
        Entry.
          [ !"Designed for hunting big game, these rifles are consistent and accurate, \
              but new rounds must be manually loaded with a bolt on the top of the gun, \
              greatly slowing their firing rate."
          ]
  ;;

  let albatross =
    Item.create
      ~name:"Hester & Sons® .44 Albatross"
      ~type_:(martial_firearm ~damage:"2d8")
      ~rarity:Rarity.very_rare
      ~properties:Item_property.[ reload 6; heavy; range (40, 120) ]
      ~entries:
        Entry.
          [ !"Nothing is more commanding than an Albatross. This revolver is chambered \
              for large-caliber bullets, and firing it feels like directing an explosion \
              at a target while a mule attempts to kick the gun from your hand. It’s \
              weight and heft stand testament to the fact that it is the most powerful \
              handgun money can buy."
          ]
  ;;

  let hornet =
    Item.create
      ~name:"Hester & Sons® .17 Hornet"
      ~type_:(simple_firearm ~damage:"2d4")
      ~rarity:Rarity.uncommon
      ~properties:Item_property.[ reload 1; light; concealable; range (20, 60) ]
      ~entries:
        Entry.
          [ !"The hornet is a very small, easily concealable firearm that can be \
              produced at a moment’s notice. It loads only one (and in some variants \
              two) bullets at a time, which might cripple it in a lengthy firefight, but \
              seldom matters when coupled with the element of surprise."
          ]
  ;;

  let homewrecker =
    Item.create
      ~name:"Hammerhead® .78 Homewrecker"
      ~type_:(martial_firearm ~damage:"2d6")
      ~rarity:Rarity.very_rare
      ~properties:Item_property.[ reload 8; scatter_2d8; two_handed; range (60, 80) ]
      ~entries:
        Entry.
          [ !"The ever-reliable homewrecker scattergun features a distinctive sliding \
              grip on the front of the barrel which can be ‘pumped’ to chamber a new \
              round. The simplicity of this design both improves reliability and reduces \
              cost, while allowing the scattergun to accept different ammunition, such \
              as nonlethal rounds."
          ]
  ;;

  let sparrow =
    Item.create
      ~name:"Hester & Sons® .38 Sparrow"
      ~type_:(simple_firearm ~damage:"2d6")
      ~rarity:Rarity.uncommon
      ~properties:Item_property.[ reload 6; range (40, 120) ]
      ~entries:
        Entry.
          [ !"An iconic handgun, the sparrow stores six bullets in a rotating cylinder, \
              which can be fired in rapid succession. This weapon is favored by \
              gunslingers for its reliability and stopping-power."
          ]
  ;;

  let typhoon =
    Item.create
      ~name:"Hammerhead® .78 Typhoon"
      ~type_:(simple_firearm ~damage:"2d6")
      ~rarity:Rarity.uncommon
      ~properties:Item_property.[ reload 2; scatter_2d8; two_handed; range (20, 60) ]
      ~entries:
        Entry.
          [ !"This variant of a scattergun has dramatically shortened barrels, \
              increasing the weapon’s spread to make it deadlier in close-combat. The \
              reduced range ruins this weapon for hunting but transforms it into an \
              ad-hoc trench gun, suitable for urban warfare."
          ]
  ;;

  let longshot =
    Item.create
      ~name:"Eagle-eye® .50 Longshot"
      ~type_:(martial_firearm ~damage:"2d8")
      ~rarity:Rarity.very_rare
      ~properties:Item_property.[ reload 4; sighted; two_handed; heavy; range (160, 480) ]
      ~entries:
        Entry.
          [ !"Designed for hunting big game, these rifles are consistent and accurate, \
              but new rounds must be manually loaded with a bolt on the top of the gun, \
              greatly slowing their firing rate."
          ]
  ;;

  let handcannon =
    Item.create
      ~name:"Ashwright Armory® 2.3 Handcannon"
      ~type_:(martial_firearm ~damage:"2d12")
      ~rarity:Rarity.very_rare
      ~properties:
        Item_property.
          [ reload 1; ultra_heavy; slow_reload; two_handed; ultra_heavy; range (80, 240) ]
      ~entries:
        Entry.
          [ !"A weapon infamous for its rate of fire, the pepperbox rotates its many \
              barrels, which fire in sequence, in order to manage the tremendous heat \
              from sustained automatic fire. This weapon is cumbersome, easily \
              recognizable, and utterly terrifying all at the same time."
          ]
  ;;

  let items =
    [ quill
    ; pepperbox
    ; scattergun
    ; albatross
    ; trueshot
    ; hornet
    ; homewrecker
    ; sparrow
    ; typhoon
    ; longshot
    ; handcannon
    ]
  ;;
end

let item_json =
  `List
    ([ Gunslinger.items ]
    |> List.concat
    |> List.sort ~compare:[%compare: Item.t]
    |> List.map ~f:Item.to_json)
;;

let property_json =
  `List
    ([ Gunslinger.properties ]
    |> List.concat
    |> List.sort ~compare:[%compare: Item_property.t]
    |> List.map ~f:Item_property.to_json)
;;

let type_json =
  `List
    ([ Gunslinger.types ]
    |> List.concat
    |> List.sort ~compare:[%compare: Item_type.t]
    |> List.map ~f:Item_type.to_json)
;;
